<?php

namespace sisVentas;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class PosDetail extends Model
{
    protected $table = 'oe_order_lines_all';

    protected $primaryKey = 'line_id';
    
    protected $fillable = ['inv_item_id', 'price', 'quantity', 'tax_rate', 'tax_id', 'pos_header_id', 'site_id', 'created_by', 'last_updated_by']; 

    public function getItemDisccountAttribute()
    {
        $site_id = intval(Session::get('site_id'));

        $disccount = DisccountDetail::where('item_id', $this->inv_item_id)
                        ->where('site_id', $site_id)
                        ->where('start_date', '<=', date_format(Carbon::now(),"Y-m-d"))
                        ->where('end_data', '>=', date_format(Carbon::now(),"Y-m-d"))
                        ->first();

        return isset($disccount) ? $disccount->rate_disccount : 0;   
    }    
}
