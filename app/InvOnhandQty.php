<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class InvOnhandQty extends Model
{
    protected $table = 'inv_onhand_quantities_detail';

    protected $primaryKey = 'onhand_quantities_id';

    protected $fillable = [
        'item_id', 'organization_id','site_id' , 'date_received', 'last_updated_by', 'created_by',
        'primary_transaction_quantity', 'subinventory_id', 'locator_id', 'create_transaction_id',
        'orig_date_received', 'transaction_uom_code', 'transaction_quantity', 'is_consigned'
    ];
}
