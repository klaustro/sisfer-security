<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class DisccountHeader extends Model
{
    protected $table = 'pos_disccount_header';
    protected $primaryKey = 'dis_header_id';
}
