<?php
 
namespace sisVentas;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use sisVentas\ItemSite;

class Item extends Model
{
    protected $table = "inv_item";
    
    protected $primaryKey = 'inv_item_id';

    protected $fillable = ['codigo','nombre','descripcion'
    						,'inventory_item_flag',
                            'stock_enabled_flag',
                            'mtl_transactions_enabled_flag',
                            'idcategoria',
                            'locator_id',
                            'primary_uom_code',
                            'last_updated_by',
                            'created_by',
                            'purchasing_item_flag',
                            'allow_item_desc_update_flag',
                            'list_price_per_unit',
                            'price_sell',
                            'inventory_item_status_code',
                            'inventory_planning_code',
                            'min_minmax_quantity',
                            'max_minmax_quantity',
                            'service_item_flag',
                            ];

   
 
    public function categoria()
    {
    //return $this->belongsTo('sisVentas\Categoria', 'idcategoria', 'idcategoria');
        return $this->belongsTo(Categoria::class, 'idcategoria');
    }

    public function ubicacion()
    {        
        return $this->belongsTo(Locator::class, 'locator_id');
    }    

    public function site()
    {        
        return $this->hasMany(ItemSite::class);
    }    



    //YASER - ITEM-MMA
    public function mma()
    {
        return $this->hasMany(ItemMma::class, 'item_id');
    }

}
