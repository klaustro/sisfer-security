<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class TipoCambio extends Model
{
    protected $table = 'gl_daily_rate';

    protected $primaryKey = 'rowid';
}
