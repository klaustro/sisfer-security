<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    protected $table = 'po_headers_all';

    protected $primaryKey = 'po_header_id';

    protected $fillable = [  'segment1',   'vendor_id',   'ship_to_location_id',   'bill_to_location_id',   'type_lookup_code',   'contact',   'currency_code',   'agent_id', ];
}
