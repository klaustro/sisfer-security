<?php

namespace sisVentas\Listeners;

use Carbon\Carbon;
use Illuminate\Auth\Events\Logout;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;
use sisVentas\LoginActivity;

class LogSuccessfulLogout
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Logout  $event
     * @return void
     */
    public function handle(Logout $event)
    {
        $log = LoginActivity::where('IDUSUARIO', Auth::user()->id)->get();
        $currentLog = $log[$log->count()-1];
        $currentLog->HRAFIN = Carbon::now();
        $currentLog->save();
    }
}
