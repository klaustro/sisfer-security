<?php

namespace sisVentas\Listeners;

use Carbon\Carbon;
use Illuminate\Auth\Events\Login;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Config;
use sisVentas\LoginActivity;

class LogSuccessfulLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $log = LoginActivity::where('IDUSUARIO',$event->user->id)->get();
        if ($log->count() > 0) {
            $lastLog = $log[$log->count()-1];

            
            if (!isset($lastLog->HRAFIN)) {
                $lastLog->HRAFIN = Carbon::parse($lastLog->HRAINICIO)->addMinutes(Config::get('session.lifetime'));
                $lastLog->save();
            }
        }

        LoginActivity::create([
            'IDUSUARIO' => $event->user->id,
            'HRAINICIO' => Carbon::now(),
            'created_by' => $event->user->id,
            'IDTERMINAL' => \Illuminate\Support\Facades\Request::ip(),
        ]);
    }
}
