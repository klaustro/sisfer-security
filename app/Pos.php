<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class Pos extends Model
{
    protected $table = 'oe_order_headers_all';

    protected $primaryKey = 'header_id';

    protected $fillable = ['site_id', 'client_id', 'type_lookup_code', 'pos_op_amt_id', 'change', 'created_by', 'last_updated_by',];

    public function posDetail()
    {
    	return $this->hasMany(PosDetail::class, 'pos_header_id');
    }

    public function posPay()
    {
    	return $this->hasMany(PosPay::class, 'pos_header_id');
    } 

}
