<?php

namespace sisVentas;
use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
    protected $table = "po_agents";
    
    protected $fillable = ['person_id', 'location_id', 'category_id', 'authorization_limit', 'enabled', 'created_by', 'last_updated_by' ];

    protected $primaryKey = 'agent_id';

//CODIGO PARA RELACIONAR CON LAS TABLAS
 public function personal()
    {
       // return $this->hasOne('sisVentas\Personal');
        return $this->belongsTo(Personal::class, 'person_id', 'PERSON_ID');
       
    }
 

    public function categoria()
    {
        return $this->hasOne('App\Categoria');
    }

    public function getBuyerNameAttribute()
    {
        return $this->personal->FULL_NAME;
    }
}
