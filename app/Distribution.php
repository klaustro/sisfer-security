<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class Distribution extends Model
{
    protected $table = 'po_distribution_all';

    protected $primaryKey = 'po_distribution_id';

    protected $fillable = ['distribution_num', 'po_header_id', 'po_line_id', 'quantity_ordered', 'po_distribution_id', 'recoverable_tax'];
}
