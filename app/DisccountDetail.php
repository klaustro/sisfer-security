<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class DisccountDetail extends Model
{
    protected $table = 'pos_detail_disccount';
    protected $primaryKey = 'pos_detail_disccount';
}
