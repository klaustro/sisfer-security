<?php

namespace sisVentas;
use Illuminate\Database\Eloquent\Model;

class SalesPerson extends Model
{
    protected $table = 'cc_salesrep';


    protected $fillable = ['sales_credit_type_id', 'name', 'status', 'start_date_active', 'end_date_active',
    'salesrep_number', 'org_id', 'email_address','telephone', 'person_id', 
        'created_by', 'last_updated_by','updated_at','created_at'];

    protected $primaryKey = 'salesrep_id';
 
 }