<?php

//namespace App;/

namespace sisVentas;
use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    protected $table = "po_vendor";
 
    public $timestamps= true;

 protected $fillable = ['vendor_name', 'segment1',  'end_date_active','no_atender','motivo_no_atencion','vendor_type_lookup_code', 'invoice_amount_limit', 'match_option', 'hold_all_payments_flag', 'hold_unmatched_invoices_flag', 'hold_invoice_no_validate', 'hold_reason', 'purchasing_hold_reason', 'hold_by', 'terms_id', 'payment_group', 'payment_method', 'invoice_currency', 'pay_currency', 'last_updated_by', 'created_by', 'telef1', 'telef2', 'email','address','contacto'];

    protected $primaryKey = 'vendor_id';
 
     


public function getEFFECTIVEENDDATEAttribute($date)
{
   return $date = \Carbon\Carbon::parse($date)->format('d-m-Y');
}
 

public function setEFFECTIVEENDDATEAttribute($date)
{
   $this->attributes['end_date_active'] = \Carbon\Carbon::parse($date)->format('Y-m-d');
}

	public function tax()
	{
	      return $this->hasOne(Impuesto::class, 'tax_id', 'tax_id');
	}    


}
