<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class InvMaterialTrx extends Model
{
    protected $table = 'inv_material_transactions';

    protected $primaryKey = 'transaction_id';

    protected $fillable = [
        'transaction_set_id', 'item_id', 'site_id','subinventory_id', 'locator_id',
        'transaction_type_id', 'transaction_quantity', 'primary_uom_code', 'transaction_date','pos_line_id',
        'type_doc_id', 'num_doc', 'ref_doc', 'obs_doc', 'last_updated_by', 'created_by'
    ];

    public function item()
    {
        return $this->belongsTo(Item::class, 'item_id');
    }

    public function subinv()
    {
        return $this->belongsTo(SubInventario::class, 'subinventory_id');
    }

    public function location()
    {
        return $this->belongsTo(Locator::class, 'locator_id');
    }

    public function uom()
    {
        return $this->belongsTo(Uom::class, 'transaction_uom_id');
    }

    public function type()
    {
        return $this->belongsTo(TipoTrx::class, 'transaction_type_id');
    }
}
