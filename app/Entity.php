<?php

namespace sisVentas;
use Illuminate\Database\Eloquent\Model;

class Entity extends Model
{
    protected $table = "ad_company";
    
    protected $fillable = [
        'code','name','description','currency_code','logo','server_email',
        'from_email',
        'type_convertion','code_tax','percentage'
        ,'symbol_currency','created_by','last_updated_by' ];

    protected $primaryKey = 'rowid';

    public $timestamps=true;

    public function tax()
    {
    	return $this->hasOne('\sisVentas\Impuesto', 'tax_id', 'code_tax');
    }

    public function rate()
    {
        return $this->hasOne('\sisVentas\TipoCambio', 'from_currency', 'currency_code');
    }    

    public function getTaxRateAttribute()
    {
        return $this->tax->tax_rate;
    }

}
