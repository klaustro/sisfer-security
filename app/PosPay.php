<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class PosPay extends Model
{
    protected $table = 'pos_pay';
    
    protected $fillable = ['type_lookup_code','amount', 'reference','pos_header_id'];
}
