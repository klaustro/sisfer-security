<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;


class Direccion extends Model
{
    protected $table='hr_location';

    protected $primaryKey='location_id';

    public $timestamps=true;


    protected $fillable =[
        'location_code',
        'adress_line_1',
        'adress_line_2',
        'adress_line_3',
        'country',
        'description',
        'ship_to_site_flag',        
        'bill_to_site_flag',
        'receiving_site_flag',
        'inactive_date',
        'Last_updated_by',
        'created_by'
    ];
 
 public function getinactive_dateAttribute($date)
{
    if($date != '0000-00-00'){
    return $date = \Carbon\Carbon::parse($date)->format('d-m-Y');
    }
    return '';
}

 public function setinactive_dateAttribute($date)
    {
        if($date == ''){
            $this->attributes['inactive_date'] = \Carbon\Carbon::parse('0000-00-00')->format('Y-m-d');
        }else{
            $this->attributes['inactive_date'] = \Carbon\Carbon::parse($date)->format('Y-m-d');
        }
    }
 

}
