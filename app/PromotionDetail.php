<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class PromotionDetail extends Model
{
    protected $table = 'pos_promotions_line';

    protected $primaryKey = 'detail_id';
}
