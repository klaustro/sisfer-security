<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $table = 'pos_promotions_header';

    protected $primaryKey = 'header_id';
}
