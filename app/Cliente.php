<?php

namespace sisVentas;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = 'cliente';

    public $timestamps= true;

    protected $fillable = ['idcliente', 'first_name', 'second_name', 'first_last_name', 'second_last_name',
    'full_name', 'date_of_birth', 'effective_end_date','sex', 'tipo_documento', 'num_documento',
        'email_address', 'telef1', 'telef2',  'disccount', 'country', 'address',
        'created_by', 'last_updated_by','updated_at','created_at',
        'contacto','no_atender','motivo_no_atencion'];

    protected $primaryKey = 'idcliente';

  public function vehiculos()
    {
        return $this->hasMany('sisVentas\Vehiculo');
    }
    
//CODIGO PARA RELACIONAR CON LAS TABLAS

    public function tipo_doc()
    {
        return $this->hasOne('App\Tipo_docs');
    }
 
 
public function getDATEOFBIRTHAttribute($date)
{
    if($date != '0000-00-00'){
    return $date = \Carbon\Carbon::parse($date)->format('d-m-Y');
    }
    return '';
}

public function setDATEOFBIRTHAttribute($date)
    {
        if($date == ''){
            $this->attributes['date_of_birth'] = \Carbon\Carbon::parse('0000-00-00')->format('Y-m-d');
        }else{
            $this->attributes['date_of_birth'] = \Carbon\Carbon::parse($date)->format('Y-m-d');
        }
    }
        


public function getEFFECTIVEENDDATEAttribute($date)
{
   return $date = \Carbon\Carbon::parse($date)->format('d-m-Y');
}


public function setEFFECTIVEENDDATEAttribute($date)
{
   $this->attributes['effective_end_date'] = \Carbon\Carbon::parse($date)->format('Y-m-d');
}

public function getDniAttribute()
{
    return $this->num_documento;
}
}
