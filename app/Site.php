<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $table = 'inv_sites';
}
