<?php

namespace sisVentas;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use sisVentas\DisccountDetail;
use sisVentas\InvOnhandQty;
use sisVentas\Item;
use sisVentas\PromotionDetail;

class ItemSite extends Model
{
    protected $table = 'inv_item_site';

    protected $primaryKey = 'iditemsite';

    protected $fillable = ['inv_item_id', 'site_id', 'sku', 'min_minmax_quantity','max_minmax_quantity','list_price_per_unit','mtl_transactions_enabled_flag','inventory_item_status_code',];

    public function item()
    {
    	return $this->belongsTo(Item::class, 'inv_item_id', 'inv_item_id');
    }

    public function disccount()
    {
        return $this->hasMany(DisccountDetail::class, 'item_id', 'inv_item_id');
    }

    public function stock()
    {
        return $this->hasOne(InvOnhandQty::class, 'item_id', 'inv_item_id');
    }   

    public function promotion()
    {
        return $this->hasOne(PromotionDetail::class, 'item_id', 'inv_item_id')
            ->where('start_date', '<=', date_format(Carbon::now(), 'Y-m-d'))
            ->where('end_data', '>=', date_format(Carbon::now(), 'Y-m-d'));
    }    


    public function getItemDisccountAttribute()
    {
        $site_id = intval(Session::get('site_id'));

        $disccount = DisccountDetail::where('item_id', $this->inv_item_id)
                        ->where('site_id', $site_id)
                        ->where('start_date', '<=', date_format(Carbon::now(),"Y-m-d"))
                        ->where('end_data', '>=', date_format(Carbon::now(),"Y-m-d"))
                        ->first();

        return isset($disccount) ? $disccount->rate_disccount : 0;   
    }  
}
