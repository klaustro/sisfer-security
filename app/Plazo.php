<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class Plazo extends Model
{
      protected $table = 'cc_plazos';
      
      protected $fillable=[
            'customer_trx_id',
            'term',
            'status',
            'due_date',
            'amount',
            'receipt_id',
            'pay_date',
            'comment',
            'last_updated_by',
            'created_by',
      ];

      //Relationship
      public function receipts()
      {
            return $this->hasMany(ReciboDetalle::class, 'term_id', 'id');
      }

      //Attributes
      public function getReceiptsAppliedAttribute()
      {

            return $this->receipts->sum('amount_applied') + $this->receipts->sum('discount_original');
      }     

      public function getBalanceAttribute()
      {
          $saldo = $this->amount - $this->receiptsApplied;
            return ($saldo < 0.01 && $saldo > -0.1) ? 0 : $saldo;
      }      
}

