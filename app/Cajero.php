<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Cajero extends Model
{
 	protected $table = 'atm_vendors';

 	public function user()
 	{
 		return $this->belongsTo(User::class);
 	}

 	public function cash()
 	{
 		return $this->hasMany(Caja::class, 'idcashier');
 	}


 	//Attributes
 	public function getNameAttribute()
 	{
 		return $this->user->name;
 	}

 	public function getOpenCashAttribute()
 	{
 		return $this->cash->where('end_date', null)->where('site_id', Session::get('site_id'))->count() > 0;
 	}

}
