<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;
use sisVentas\Cajero;
use sisVentas\FndLookupValue;
use sisVentas\Site;

class Caja extends Model
{
    protected $table = 'pos_op_amt';

    protected $primaryKey = 'idopamt';

    protected $fillable = ['site_id', 'idcashier', 'start_date', 'end_date', 'start_amount', 'comment', 'rate', 'barcode_reader', 'rate_date', 'created_by',];

    //relationships
    public function pos()
    {
    	return $this->hasMany(Pos::class, 'pos_op_amt_id', 'idopamt');
    }

    public function cashier()
    {
        return $this->belongsTo(Cajero::class, 'idcashier', 'id');
    }

    public function site()
    {
        return $this->belongsTo(Site::class);
    }


    public function sells()
    {
        $sells = [];
        $change = 0;
        foreach (FndLookupValue::all() as $pay_type) {
            $amount = 0;
            $count = 0;
            foreach ($this->pos as $pos) {
                $pay = $pos->posPay->where('type_lookup_code', $pay_type->code_value)->sum('amount');
                $amount += $pay;
                $count += $pay > 0 ? 1 : 0;
            }          


            if ($amount > 0) {                
                array_push($sells, [
                    $pay_type->description . " ($count)" => $amount
                ]);
            }
        }

        foreach ($this->pos as $pos) {
            $change += $pos->change;
        }    

        if ($change > 0){
            array_push($sells, [
                'Vueltos' => $change * -1,
            ]);            
        }

        return $sells;        
    }

    public function getPaysTotalAttribute()
    {
        $total = 0;
        foreach ($this->pos as $pos) {
            $total += $pos->posPay->sum('amount') - (isset($pos->change) ? $pos->change : 0);
        }   

        return $total;     

    }    

    public function getSellsTotalAttribute()
    {
        $total = 0;
        foreach ($this->pos as $pos) {
            $total += $pos->posDetail->sum('price') * $pos->posDetail->sum('quantity');
        }   

        return $total;     

    }

    public function getTotalAttribute()
    {
        $total = 0;
        foreach ($this->pos as $pos) {
            foreach ($pos->posDetail as $detail) {
                $disccount = $detail->price * $detail->item_disccount;
                
                $tax = (($detail->price - $disccount)  * $detail->quantity) * $detail->tax_rate / 100;

                $total += (($detail->price - $disccount) * $detail->quantity) + $tax;               
            }
        }   

        return $total;     

    }    

    public function getCashierNameAttribute()
    {       
        return $this->cashier->name;
    }

    public function getSiteNameAttribute()
    {       
        return $this->site->name;
    }    
}
