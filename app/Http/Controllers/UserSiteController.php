<?php

namespace sisVentas\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use sisVentas\Http\Requests;
use sisVentas\Site;
use sisVentas\User;
use sisVentas\UserSite;

class UserSiteController extends Controller
{

    public function create()
    {
        $users = User::all()->pluck('email', 'id');

        $sites = Site::all();

        return view('usersite.create', compact('users', 'sites'));
    }


    public function show(Request $request, $id)
    {
        $userSites = UserSite::where('IDUSUARIO', $id)->get();

        return $userSites;
    }

    public function store(Request $request)
    {
        $this->storeIt($request);
        return ['message' => 'Usuario asociado satisfactoriamente'];
    }

    public function update(Request $request, $id)
    {
        $userSites = UserSite::where('IDUSUARIO', $id)->get();

        if ($userSites->count()) {
            foreach ($userSites as $item) {
                UserSite::destroy($item->IDUSERSITE);
            }            
        }

        $this->storeIt($request);

        return ['message' => 'Usuario actualizado satisfactoriamente'];        
    }

    protected function storeIt(Request $request)
    {
        foreach ($request->all() as $item) {
            $userSite = UserSite::create($item);
            $userSite->CREPOR=Auth::user()->id;
            $userSite->ULTACTPOR=Auth::user()->id;
            $userSite->save();            
        }
        
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function userList(Request $request)
    {

        $users = User::all();

        return $users;
    }

    public function siteList()
    {
        
        $sites = Site::all();

        return $sites;
    }  

    public function emailSite($email)
    {
        $user = User::where('email', $email)->first();
        if (isset($user)) {
            $today = Carbon::now()->toDateString();
            $userSites = UserSite::where('IDUSUARIO', $user->id)
                ->where('FEC_INI', '<=', $today)
                ->where('FEC_FIN', '>=', $today)
                ->get()
                ->pluck('IDSITE');
            $sites = Site::whereIn('id', $userSites)->get();

            return $sites;
        }

        return [];

    }
}
