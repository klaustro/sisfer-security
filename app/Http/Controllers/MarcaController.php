<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;
use sisVentas\Marca;
use Illuminate\Support\Facades\Redirect;
use sisVentas\Http\Requests\MarcaFormRequest;
use DB;

use Fpdf;

class MarcaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if ($request)
        {
            $query=trim($request->get('searchText'));
            $marcas=DB::table('marca')->where('nombre','LIKE','%'.$query.'%')
            //->where ('condicion','=','1')
            ->orderBy('idmarca','desc')
            ->paginate(7);
            return view('almacen.marca.index',["marcas"=>$marcas,"searchText"=>$query]);
        }
    }
    public function create()
    {
        return view("almacen.marca.create");
    }
    public function store (MarcaFormRequest $request)
    {
        $marca=new Marca;
        $marca->nombre=$request->get('nombre');
        $request['created_by'] = Auth()->user()->id;
        $request['last_updated_by'] = Auth()->user()->id;
        $marca->condicion='1';
        $marca->save();
        return Redirect::to('almacen/marca');

    }
    public function show($id)
    {
        return view("almacen.marca.show",["marca"=>Marca::findOrFail($id)]);
    }
    public function edit($id)
    {
        return view("almacen.marca.edit",["marca"=>Marca::findOrFail($id)]);
    }
    public function update(MarcaFormRequest $request,$id)
    {
        $marca=Marca::findOrFail($id);
        $marca->nombre=$request->get('nombre');
        $marca->condicion=$request->get('active');
        if ( $request->get('active') == true)
           $marca->condicion=1;

        $request['last_updated_by'] = Auth()->user()->id;
        $marca->update();
        return Redirect::to('almacen/marca');
    }
    public function destroy($id)
    {
        $marca=Marca::findOrFail($id);
        $marca->condicion='0';
        $marca->update();
        return Redirect::to('almacen/marca');
    }
    public function reporte(){
         //Obtenemos los registros
         $registros=DB::table('marca')
            ->where ('condicion','=','1')
            ->orderBy('nombre','asc')
            ->get();

         $pdf = new Fpdf();
         $pdf::AddPage();
         $pdf::SetTextColor(35,56,113);
         $pdf::SetFont('Arial','B',11);
         $pdf::Cell(0,10,utf8_decode("Listado Marcas"),0,"","C");
         $pdf::Ln();
         $pdf::Ln();
         $pdf::SetTextColor(0,0,0);  // Establece el color del texto 
         $pdf::SetFillColor(206, 246, 245); // establece el color del fondo de la celda 
         $pdf::SetFont('Arial','B',10); 
         //El ancho de las columnas debe de sumar promedio 190        
         $pdf::cell(50,8,utf8_decode("Nombre"),1,"","L",true);
          
         
         $pdf::Ln();
         $pdf::SetTextColor(0,0,0);  // Establece el color del texto 
         $pdf::SetFillColor(255, 255, 255); // establece el color del fondo de la celda
         $pdf::SetFont("Arial","",9);
         
         foreach ($registros as $reg)
         {
            $pdf::cell(50,6,utf8_decode($reg->nombre),1,"","L",true);
            
            $pdf::Ln(); 
         }

         $pdf::Output();
         exit;
    }
}
