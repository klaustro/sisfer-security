<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;

use sisVentas\Entity;
use sisVentas\Impuesto;

class ImpuestoController extends Controller
{
	public function index()
	{
		$entity = Entity::first();
		$taxes = Impuesto::all();

		$newTaxes = [];

		foreach ($taxes as $tax) {
			array_push($newTaxes, [
				'tax_id' => $tax->tax_id,
				'name' => $tax->name,
				'tax_rate' => $tax->tax_rate,
				'selected' => ($tax->tax_id == $entity->code_tax),
			]);
		}
		//dd($newTaxes);
		return $newTaxes;

	}

	public function show($id)
	{
		if ($id ==0) {			
			$entity = Entity::first();
			$tax_id = $entity->code_tax;
		}
		else
		{
			$tax_id = $id;
		}

		$tax = Impuesto::where('tax_id', $tax_id)->get()->toArray();

		return $tax;
	}
}
