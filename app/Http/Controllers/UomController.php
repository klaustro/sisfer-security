<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;

use sisVentas\Uom;
use sisVentas\Item;

class UomController extends Controller
{
	public function show($item)
	{
		$item = Item::find($item);

		$uom = Uom::where('uom_code', $item->primary_uom_code)->get()->toArray();
		
		return $uom;
	}
}
