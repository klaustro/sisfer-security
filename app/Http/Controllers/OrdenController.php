<?php

namespace sisVentas\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use sisVentas\Buyer;
use sisVentas\DailyRate;
use sisVentas\Direccion;
use sisVentas\Entity;
use sisVentas\FndLookup;
use sisVentas\FndLookupValue;
use sisVentas\Http\Requests;
use sisVentas\Impuesto;
use sisVentas\Item;
use sisVentas\Moneda;
use sisVentas\Orden;
use sisVentas\OrdenDetalle;
use sisVentas\Proveedor;
use sisVentas\TerminoPagoAR;
use sisVentas\TerminoPagoCP;
use sisVentas\Uom;
use sisVentas\UserSite;

class OrdenController extends Controller
{
        protected $currencies = [];
        protected $classes = [];
        protected $sellers = [];
        protected $items = [];
        protected $uoms = [];
        protected $taxes = [];
        protected $terms = [];
        protected $fletes = [];
        protected $ships = [];
        protected $fobs = [];
        protected $enttityTax = 0;

    public function makeLists()
    {
        $this->currencies = ['' => 'Seleccione la moneda'] + Moneda::orderBy('descripcion', 'ASC')->get()->pluck('descripcion', 'currency_code')->toArray();

        $this->buyers = ['' => 'Seleccione el comprador'] + 
            Buyer::orderBy('agent_id', 'ASC')->get()->pluck('buyer_name', 'agent_id')->toArray();   
        
        $this->uoms = ['' => 'Seleccione el unidad de medida'] + Uom::orderBy('uom_code', 'ASC')->get()->pluck('uom_code', 'uom_code')->toArray();

        $this->vendors = ['' => 'Seleccione el Proveedor'] + Proveedor::orderBy('vendor_name', 'ASC')->get()->pluck('vendor_name', 'vendor_id')->toArray();

        $entity = Entity::all();

        $this->enttityTax = ($entity->count() > 0) ? $entity[0]['code_tax'] : null;

        $this->locations =  ['' => 'Seleccione la dicrección'] + Direccion::orderBy('location_id', 'ASC')->get()->pluck('description', 'location_id')->toArray();

        $this->terms = ['' => 'Seleccione el tipo de pago'] + TerminoPagoCP::orderBy('ap_term_id', 'ASC')->get()->pluck('description', 'ap_term_id')->toArray();    

        $type = FndLookup::where('lookup_type', 'TIPO_OC')->first();
        $type = (isset($type)) ? $type->fndLookupValue->pluck('description', 'code_value')->toArray() : [];
        $this->types = ['' => 'Seleccione el tipo'] + $type;        


        $fletes =  FndLookup::where('lookup_type', 'FREIGHT_TERMS')->first();
        $fletes = (isset($fletes) ? $fletes->fndLookupValue->pluck('description', 'code_value')->toArray() : []);
        $this->fletes = ['' => 'Seleccione el tipo de flete'] + $fletes;  

        $ships = FndLookup::where('lookup_type', 'SHIP_VIA')->first();
        $ships = (isset($ships)) ? $ships->fndLookupValue->pluck('description', 'code_value')->toArray() : [];
        $this->ships = ['' => 'Seleccione el transportista'] + $ships;    

        $fobs = FndLookup::where('lookup_type', 'FOB')->first();
        $fobs = isset($fobs) ? $fobs->fndLookupValue->pluck('description', 'code_value')->toArray() : [];
        $this->fobs = ['' => 'Seleccione el FOB'] + $fobs;                                    
    }


    public function index(Request $request)
    {
        $rsOrders = Orden::orderBy('created_at', 'desc')->get(); 

        return view('compras.orden.index')->with('rsOrders', $rsOrders);         

    }

    public function create()
    {
            $this->makeLists();

            $currencies = $this->currencies;
            $uoms = $this->uoms;            
            $locations = $this->locations;            
            $types = $this->types;            
            $buyers = $this->buyers;
            $terms = $this->terms;            
            $fletes = $this->fletes;
            $ships = $this->ships;  
            $fobs = $this->fobs;

            $rates =  DailyRate::where('conversion_date', date_format(Carbon::now(),"Y-m-d"))->get()->pluck('from_currency');

            $defaultCurrency = Entity::first()->currency_code;


            return view("compras.orden.create", compact('currencies', 'uoms', 'locations', 'types', 'buyers', 'terms','fletes','ships','fobs', 'rates', 'defaultCurrency')); 
    }



    public function show($id)
    {
            $this->makeLists();

            $currencies = $this->currencies;
            $uoms = $this->uoms;                        
            $locations = $this->locations;
            $types = $this->types;            
            $buyers = $this->buyers;
            $terms = $this->terms;
            $fletes = $this->fletes;
            $ships = $this->ships;
            $fobs = $this->fobs;           

            $rates =  DailyRate::where('conversion_date', date_format(Carbon::now(),"Y-m-d"))->get()->pluck('from_currency');

            $defaultCurrency = Entity::first()->currency_code;            

            $purchaseOrder =  Orden::find($id);

            return view("compras.orden.show", compact('purchaseOrder', 'currencies', 'uoms', 'locations', 'types', 'buyers', 'terms','fletes','ships','fobs', 'defaultCurrency', 'rates', 'purchaseOrder')); 
    }   

    public function store(Request $request)
    {
        //dd(request()->all());
        $this->validate($request, [
                'vendor_id' => 'required|',
                'ship_to_location_id' => 'required',
                'bill_to_location_id' => 'required',
                'type_lookup_code' => 'required',
                'currency_code' => 'required',
                'agent_id' => 'required',
        ]);

        $entity = Entity::first();
        $segment1 = Orden::count() + 1;
        $purchaseOrder = Orden::create($request->except('_token'));
        $purchaseOrder->segment1 = $segment1;
        $purchaseOrder->rate_type = $entity->type_convertion;
        $purchaseOrder->rate_date = $purchaseOrder->created_at;
        $purchaseOrder->rate = $purchaseOrder->rate_amount;
        $purchaseOrder->authorization_status = 'INCOMPLETE';
        $purchaseOrder->site_id = Session::get('site_id');
        $purchaseOrder->last_updated_by = Auth::user()->id;
        $purchaseOrder->created_by = Auth::user()->id;
        $purchaseOrder->save();


        return redirect('compras/orden/' . $purchaseOrder->po_header_id);
    }

    public function update(Request $request, $id)
    {
        //dd(request()->all());
        $this->validate($request, [
                'vendor_id' => 'required|',
                'ship_to_location_id' => 'required',
                'bill_to_location_id' => 'required',
                'agent_id' => 'required',
            ]);

        $purchaseOrder  = Orden::find($id);
        
        $purchaseOrder->fill($request->except('_token'));
        $purchaseOrder->last_updated_by = Auth::user()->id;     
        $purchaseOrder->site_id = Session::get('site_id');     
        $purchaseOrder->save();

        return redirect('/compras/orden');
    }   

    public function delete($id)
    {
/*        $bill = Factura::find($id);
      return view('ventas.factura.delete', compact('bill'));  */
    }
    
    public function destroy($id)
    {
/*        $details = FacturaDetalle::where('customer_trx_id', $id)->get();

        if ($details->count() > 0)
        {                
            foreach ($details as $detail) {
                $detail->delete();
            }
        }  
              
        Factura::destroy($id);

        return redirect()->route('ventas.factura.index');*/
    }

    public function cancel($id)
    {
        $po = Orden::find($id);
        return view('compras.orden.cancel', compact('po'));  
    }

    public function setCancel($id)
    {
        $po = Orden::find($id);

        $po->authorization_status = 'cancelado';

        $po->cancel_flag = 'Y';

        $po->closed_code = 'CANCEL';

        $po->closed_date = Carbon::now();

        $po->save();

        return redirect()->route('compras.orden.index');
    }

    public function approve($id)
    {
        $po = Orden::find($id);

        $po->authorization_status = 'aprobado';   
        
        $po->save();

        return ['message' => 'Orden aprobada'];
    }
    
    public function vendorList()
    {
        $vendors = Proveedor::with('tax')->get();

        return $vendors;
    }

    public function print($id)
    {
        $order = Orden::find($id);
        $company = Entity::first();
        $userSite = UserSite::find(Session::get('site_id'));
        return view('compras.orden.order', compact('order', 'company', 'userSite'));
    }

}
