<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;
use sisVentas\Http\Requests;
use sisVentas\Factura;
use sisVentas\Cliente;
use sisVentas\Moneda;
use sisVentas\Uom;
use sisVentas\SalesPerson;
use sisVentas\TerminoPagoAR;
use sisVentas\Item;
use sisVentas\Impuesto;
use sisVentas\Entity;
use sisVentas\FacturaDetalle;
use Illuminate\Support\Facades\Auth;

class FacturaController extends Controller
{
        protected $currencies = [];
        protected $classes = [];
        protected $sellers = [];
        protected $terms = []; 
        protected $items = [];
        protected $uoms = [];
        protected $taxes = [];
        protected $enttityTax = 0;

    public function makeLists()
    {
        $this->currencies = ['' => 'Seleccione la moneda'] + Moneda::orderBy('descripcion', 'ASC')->get()->pluck('descripcion', 'idcurrency')->toArray();
        
        $this->classes =  ['' => 'Seleccione la clase', '1' => 'Factura' , '2' => 'Boleta' , '3' => 'Nota de crédito', '4'=> 'Nota de débito'];

        $this->sellers = ['' => 'Seleccione el vendedor'] + SalesPerson::orderBy('name', 'ASC')->get()->pluck('name', 'salesrep_id')->toArray();;

        $this->terms =  ['' => 'Seleccione el término de pago'] + TerminoPagoAR::orderBy('name', 'ASC')->get()->pluck('name', 'term_id')->toArray(); 
        
        $this->uoms = ['' => 'Seleccione el unidad de medida'] + Uom::orderBy('uom_code', 'ASC')->get()->pluck('uom_code', 'uom_code')->toArray();

        $this->taxes = ['' => 'Seleccione el impuesto'] + Impuesto::orderBy('name', 'ASC')->get()->pluck('name', 'tax_id')->toArray();

        $this->enttityTax = Entity::all()[0]['code_tax'];
    }


    public function index(Request $request)
    {
        $client = $request->client;

        $trxStart = $request->trxStart;
        
        $trxEnd = $request->trxEnd;

        $dateStart = $request->dateStart;
        
        $dateEnd = $request->dateEnd;

        $bills = Factura::id($request->trxStart, $request->trxEnd)
                ->idClient($client)
                ->client($client)
                ->idRange($request->trxStart, $request->trxEnd)
                ->dateRange($request->dateStart, $request->dateEnd)
                ->orderBy('customer_trx_id', 'DESC')
                ->paginate();

        return view('ventas.factura.index', compact('bills', 'client', 'trxStart', 'trxEnd', 'dateStart', 'dateEnd'));
    }

    public function create()
    {
        $this->makeLists();

        $currencies = $this->currencies;
        $classes = $this->classes;
        $sellers = $this->sellers;
        $terms = $this->terms;
        $items = $this->items;
        $uoms = $this->uoms;
        $taxes = $this->taxes;
        $entityTax = $this->enttityTax;

        return view("ventas.factura.create", compact('currencies', 'classes', 'sellers', 'terms', 'items', 'uoms', 'taxes', 'entityTax'));        
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'trx_number' => 'required|',
            'class_code' => 'required',
            'class_trx' => 'required|unique:CC_CUSTOMER_TRX_ALL',
            'trx_date' => 'required',
            'invoice_currency_code' => 'required',
            'bill_to_customer_id' => 'required',
            'ship_to_customer_id' => 'required',
        ]);
//dd($request->all());
        $bill = Factura::create($request->all());
        
        $bill->last_updated_by = Auth::user()->id;
        $bill->created_by = Auth::user()->id;
        
        $bill->save();

        return redirect('/ventas/factura/' . $bill->customer_trx_id);
    }

    public function show(Request $request, $id)
    {
        $this->makeLists();
        
        $bill = Factura::find($id);
        $currencies = $this->currencies;
        $classes = $this->classes;
        $sellers = $this->sellers;
        $terms = $this->terms;
        $items = $this->items;
        $uoms = $this->uoms;  
        $taxes = $this->taxes;  
        $entityTax = $this->enttityTax;

        $billClient = Cliente::where('idcliente', $bill->bill_to_customer_id)->get();

        $billAddress = $billClient[0]->address;
        $shipAddress = $billClient[0]->address;

        $shipClient = Cliente::where('idcliente', $bill->ship_to_customer_id)->get();
        $shipAddress = $shipClient[0]->address;

        return view("ventas.factura.edit", compact('bill', 'billAddress', 'shipAddress', 'currencies', 'classes', 'sellers', 'terms', 'items', 'uoms', 'taxes', 'entityTax'));        
    }

    public function update(Request $request, $id)
    {
        
        $this->validate($request, [
            'trx_number' => 'required',
            'class_code' => 'required',
            'trx_date' => 'required',
            'invoice_currency_code' => 'required',
            'bill_to_customer_id' => 'required',
            'ship_to_customer_id' => 'required',
        ]);

        $bill = Factura::find($id);
        $bill->fill($request->all());
        $bill->last_updated_by = Auth::user()->id;
        $bill->save();

        return redirect('/ventas/factura');
    }

    public function delete($id)
    {
        $bill = Factura::find($id);
      return view('ventas.factura.delete', compact('bill'));  
    }
    
    public function destroy($id)
    {
        $details = FacturaDetalle::where('customer_trx_id', $id)->get();

        if ($details->count() > 0)
        {                
            foreach ($details as $detail) {
                $detail->delete();
            }
        }  
              
        Factura::destroy($id);

        return redirect()->route('ventas.factura.index');
    }

    public function cancel($id)
    {
        $bill = Factura::find($id);
        return view('ventas.factura.cancel', compact('bill'));  
    }

    public function setCancel($id)
    {
        $bill = Factura::find($id);

        $bill->status_trx = 'CANCEL';

        $bill->save();

        return redirect()->route('ventas.factura.index');
    }

}
