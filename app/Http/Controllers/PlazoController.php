<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;
use sisVentas\Factura;
use sisVentas\Plazo;
use Illuminate\Support\Facades\Auth;

class PlazoController extends Controller
{
	public function create($id)
	{
		$bill = Factura::find($id);

		return view('ventas.plazo.create', compact('bill'));
	}

	public function store(Request $request)
	{
		$bill = $request->all()['0']['customer_trx_id'];

		$details = Plazo::where('customer_trx_id', $bill)                
		    ->get();

		if ($details->count() > 0)
		{                
		    foreach ($details as $detail) {
		    	if ($detail->status != 'Pagado') {
		        $detail->delete();
		    	}
		    }
		}

		foreach ($request->all() as $item) {
			if ($item['status'] != 'Pagado') {
				$term = Plazo::create($item);
				$term->last_updated_by = auth::user()->id;
				$term->created_by = auth::user()->id;
				$term->save();				
			}
		}

		return ['message' => 'Plazos creados satirfactoriamente'];
	}

 	public function show($id)
 	{
 		$terms = Plazo::where('customer_trx_id', $id)->get();


 		foreach ($terms as $term) {
 			$term['balance'] = $term->balance;
 		}
 		//dd($terms);

 		return $terms;
 	}
}
