<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;
use sisVentas\Distribution;
use sisVentas\Http\Requests;
use sisVentas\PurchaseOrder;
use sisVentas\PurchaseOrderDetail;

class PurchaseOrderDetailController extends Controller
{
	public function index(Request $request)
	{
		$ocDetail = PurchaseOrderDetail::where('po_header_id', $request->id)
			->with('categoria')
			->get();

		return $ocDetail;

	}

	public function store(Request $request)
	{
		$detail = PurchaseOrderDetail::where('po_header_id', $request->all()[0]['po_header_id'])->get();
		foreach ($detail as $detail) {
			PurchaseOrderDetail::destroy($detail->po_line_id);
		}

		$distribution = Distribution::where('po_header_id', $request->all()[0]['po_header_id'])->get();
		foreach ($distribution as $detail) {
			Distribution::destroy($detail->po_distribution_id);
		}


		foreach ($request->all() as $item) {
			$purchaseOrderDetail = PurchaseOrderDetail::create($item);
			$purchaseOrderDetail->save();

			$distribution = Distribution::create([
				'distribution_num' => 1, 
				'po_header_id' => $item['po_header_id'], 
				'po_line_id' => $purchaseOrderDetail->po_line_id, 
				'quantity_ordered' => $item['quantity'], 
				'recoverable_tax' => $item['recoverable_tax'], 
			]);
	
		}


		return ['message' => 'Detalle guardado satisfactoriamente'];
	}
}
