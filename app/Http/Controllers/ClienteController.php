<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;

use sisVentas\Cliente;
use sisVentas\Factura;

class ClienteController extends Controller
{
    public function showApi(Request $request)
    {

    	if ($request->mount == '1') {
                	$factura = Factura::find($request->customer_trx_id);
                	$shipTo =  Cliente::find($factura->ship_to_customer_id)->toArray();
                	$billTo = Cliente::find($factura->bill_to_customer_id)->toArray();

                	$clients = [
                		'shipTo' => $shipTo,
                		'billTo' => $billTo,
    	];

    	}
    	else if(isset($request->idcliente))
            {
    		$clients =  Cliente::find($request->idcliente)->toArray();
    	}
            else
            {
                $clients = Cliente::all()->toArray();
            }

            return  $clients;
    }

    public function pendingBills($id)
    {
        $bills = Factura::where('bill_to_customer_id', $id)     
            ->with('terms')       
            ->with('billCurrency')       
            ->where('status_trx', '<>', 'CANCEL')
            ->where('complete_flag', 'Y')
            ->get();

        foreach ($bills as $key => $bill) {
            if ($bill->balance> 0) {
                $bill['balance'] = $bill->balance;
            }        

            if (count($bill->terms) > 0){
                foreach ($bill->terms as $term) {
                $term['balance'] = $term->balance;                    
                }
            }
        }
        return $bills;
    }

    public function indexApi(){
        $customers = Cliente::all();
        return $customers;
    }
}
