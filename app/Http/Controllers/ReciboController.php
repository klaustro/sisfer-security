<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;

use sisVentas\Recibo;
use sisVentas\ReciboDetalle;
use sisVentas\ReciboHistory;
use Illuminate\Support\Facades\Auth;

class ReciboController extends Controller
{
	protected $banks =[
		'' => 'Seleccione el banco',
		1 => 'Banco de Comercio',
		2 => 'Banco de Crédito del Perú',
		3 => 'Banco Interamericano de Finanzas (BanBif)',
		4 => 'Banco Financiero',
		5 => 'BBVA Continental',
		6 => 'Citibank',
		7 => 'Interbank',
		8 => 'MiBanco',
		9 => 'Scotiabank Perú',
		10 => 'Banco GNB Perú',
		11 => 'Banco Falabella',
		12 => 'Banco Ripley',
		13 => 'Banco Santander Perú',
		14 => 'Banco Azteca',
		15 => 'Banco Cencosud',
		16 => 'ICBC PERU BANK',
	];

	public function index(Request $request)
	{
		$receipts = Recibo::bill($request->bill)->orderBy('receipt_number', 'DESC')->paginate();		

		return view('ventas.recibo.index', compact('receipts'));
	}

	public function show(Request $request, $id)
	{
		$receipt = Recibo::find($id);
		$banks = $this->banks;

		return view('ventas.recibo.edit', compact('receipt',  'banks'));
	} 

	public function update(Request $request, $id)
	{
        $this->validate($request, [
		'receipt_number' => 'required',
		'amount' => 'required',
		'type' => 'required',
		'status' => 'required',
		'receipt_date' => 'required',
		'receipt_due_date' => 'required',
        ]);

        $receipt = Recibo::find($id);
        $receipt->fill($request->all());
        $receipt->last_updated_by = Auth::user()->id;        
        $receipt->save();

        return redirect('/ventas/recibo');
	}

	public function create()
	{
		$banks = $this->banks;
		return view('ventas.recibo.create', compact( 'banks'));
	}

	public function store(Request $request)
	{		
		$this->validate($request, [
			'receipt_number' => 'required|unique:CC_CASH_RECEIPTS_ALL',
			'amount' => 'required',
			'type' => 'required',
			'status' => 'required',
			'receipt_date' => 'required',
			'receipt_due_date' => 'required',
		]);

		$receipt = Recibo::create($request->all());
        	$receipt->last_updated_by = Auth::user()->id;        
        	$receipt->created_by = Auth::user()->id;        		

		$receipt->save();		

		return redirect('/ventas/recibo/' . $receipt->cash_receipt_id);
	}

    public function delete($id)
    {
        $receipt = Recibo::find($id);
      return view('ventas.recibo.delete', compact('receipt'));  
    }	

    public function destroy($id)
    {
        $details = ReciboDetalle::where('cash_receipt_id', $id)->get();

        if ($details->count() > 0)
        {                
            foreach ($details as $detail) {
                $detail->delete();
            }
        }  
              
        Recibo::destroy($id);

        return redirect()->route('ventas.recibo.index');
    }    

    public function reverse($id)
    {
      $receipt = Recibo::find($id);
      	return view('ventas.recibo.reverse', compact('receipt'));  
    }	


    public function setReverse(Request $request, $id)
    {
    	$receipt = Recibo::find($id);

    	$receipt->status = 'REV';

    	$receipt->save();

    	$details = ReciboDetalle::where('cash_receipt_id', $receipt->cash_receipt_id)->get();

    	foreach ($details as $detail) {
    		$detail->amount_due_original = $detail->amount_applied;
    		$detail->amount_applied = 0;
    		$detail->save();
    	}

    	$history = ReciboHistory::create($request->all());

    	$history->save();

    	//dd($history);

    	return redirect()->route('ventas.recibo.index');

    }
}
