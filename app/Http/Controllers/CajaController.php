<?php

namespace sisVentas\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use sisVentas\Caja;
use sisVentas\Http\Requests;

class CajaController extends Controller
{
    public function index()
    {
        $cashes = Caja::where('site_id', Session::get('site_id'))->paginate();

        return view('ventas.caja.index', compact('cashes'));
    }

    public function show($id)
    {
        $cash = Caja::find($id);

        return view('ventas.caja.edit', compact('cash'));
    }

    public function open()
    {
        //dd(request()->all());
        $caja = Caja::create(request()->all());
        $caja->site_id = Session::get('site_id');
        $caja->created_by = auth()->user()->id;
        $caja->last_updated_by = auth()->user()->id;
        $caja->start_date = $caja->created_at;
        if (request()->idcashier == null)
        {
            $caja->idcashier = auth()->user()->cashier->id;
        }
        $caja->save();

        return [
            'message' => 'Caja abierta satisfactoriamente', 
            'cash_id' => $caja->idopamt
        ];
    }

    public function close($id)
    {
        $caja = Caja::find($id);
        $caja->end_date = Carbon::now();
        $caja->last_updated_by = auth()->user()->id;
        $caja->save();

        return ['message' => 'Caja cerrada satisfactoriamente'];
    }

}
