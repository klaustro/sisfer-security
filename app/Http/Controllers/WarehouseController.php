<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use sisVentas\Http\Requests;
use sisVentas\Warehouse;

class WarehouseController extends Controller
{
    public function index()
    {
    	$warehouses = Warehouse::where('site_id', Session::get('site_id'))->paginate();

    	return view('almacen.warehouse.index', compact('warehouses'));
    }

    public function create()
    {
    	return view('almacen.warehouse.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:inv_sub_inventory,name,NULL,id,site_id,'.Session::get('site_id')
        ]);

    	$warehouse = Warehouse::create([
    		'name' => $request->name,
    		'site_id' => Session::get('site_id'),
    	]);

    	$warehouse->save();

    	return redirect('/almacen/warehouse');
    }

    public function show($id)
    {
    	$warehouse = Warehouse::find($id);

    	return view('almacen.warehouse.show', compact('warehouse'));
    }

    public function update(Request $request, $id)
    {
    	$warehouse = Warehouse::find($id);
    	
    	$warehouse->name= $request->name;

    	$warehouse->save();

    	return redirect('/almacen/warehouse');
    }    
}
