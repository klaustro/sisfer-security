<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;

use sisVentas\Item;

class ItemController extends Controller
{
    public function showFA($id)
    {
    	$item = Item::find($id);

    	return  $item;
    }

    public function indexFA()
    {
       $items =  Item::where('inventory_item_status_code', 'active')
                ->where('mtl_transactions_enabled_flag', '1')
                ->with('categoria')
                ->orderBy('inv_item_id', 'ASC')
                ->get()
                ->toArray();

        return  $items;		
    }
}


