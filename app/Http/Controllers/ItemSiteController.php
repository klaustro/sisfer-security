<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use sisVentas\Entity;
use sisVentas\Http\Requests;
use sisVentas\ItemSite;

class ItemSiteController extends Controller
{
    public function store()
    {
    	$items = request()->all();

    	//dd($items);

    	foreach ($items as $item) {
    		$itemSite = ItemSite::create($item);
    		$itemSite->last_updated_by = auth()->user()->id;
    		$itemSite->created_by = auth()->user()->id;
    		$itemSite->save();
    	}

    	return ['message' => 'Items agregados a la sucursal'];
    }

    public function priceList()
    {
        $items = ItemSite::where('site_id', Session::get('site_id'))->with(['item', 'disccount', 'promotion'])->paginate();
        $entity = Entity::first();
        //dd($items);
        return view('ventas.pos.price', compact('items', 'entity'));
    }
}
