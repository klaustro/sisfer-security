<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;

use sisVentas\ReciboDetalle;
use sisVentas\Recibo;
use sisVentas\Plazo;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class ReciboDetalleController extends Controller
{
    public function store(Request $request)
    {
        $receipt = Recibo::find($request->all()['0']['cash_receipt_id']);


        $details = ReciboDetalle::where('cash_receipt_id', $receipt->cash_receipt_id)                
        ->get();


        if ($details->count() > 0)
        {                
            foreach ($details as $detail) {
                $detail->delete();
            }
        }			

        foreach ($request->except('_token') as $item) {
            $term_id = null;
            $separator = strpos($item['customer_trx_id'], '-');
            $customer_trx_id = $separator ? substr($item['customer_trx_id'], 0, $separator) : $item['customer_trx_id'];
            $term_no = substr($item['customer_trx_id'], $separator + 1);

            if ($separator) {
                $term = Plazo::where([
                    'customer_trx_id' => $customer_trx_id,
                    'term' => $term_no,
                ])->get();

                $term[0]->status = ($item['amount_applied'] - $term[0]->balance == 0) ? 'Pagado' : 'No Pagado';
                $term[0]->receipt_id = $item['cash_receipt_id'];
                $term[0]->pay_date = Carbon::now();
                $term[0]->save();
                $term_id = $term[0]->id;
            }            

            $detail = ReciboDetalle::create([
                'cash_receipt_id' => $item['cash_receipt_id'],
                'customer_trx_id' =>  $customer_trx_id,
                'amount_applied' => $item['amount_applied'],
                'amount_due_remaining' => $item['amount_due_remaining'],
                'discount_original' => $item['discount_original'],
                'term_id' => $term_id,
            ]);

            $detail->last_updated_by = Auth::user()->id;        
            $detail->created_by = Auth::user()->id;     
            $detail->save();
        }

        if ($receipt->notApplied == 0){
            $receipt->status = 'comp';
            $receipt->save();
        }

        return response()->json([
            'title' => 'Detalle de recibo',
            'message' => 'El detalle del recibo ha sido creado'
        ]);	    	
    }

    public function show($id)
    {
        $details = ReciboDetalle::where('cash_receipt_id', $id)->with('terms')->get()->all();

        return $details;
    }
}
