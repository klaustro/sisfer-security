<?php

namespace sisVentas\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use sisVentas\Http\Requests;
use sisVentas\TipoCambio;

class TipoCambioController extends Controller
{
    public function index()
    {
        $rate = TipoCambio::where('conversion_date', date_format(Carbon::now(), 'Y-m-d'))->get();  

        return $rate;
    }
}
