<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use sisVentas\Buyer;
use sisVentas\Direccion;
use sisVentas\FndLookupValue;
use sisVentas\Http\Requests;
use sisVentas\Moneda;
use sisVentas\Proveedor;
use sisVentas\PurchaseOrder;
use sisVentas\Uom;

class PurchaseOrderController extends Controller
{
	protected $currencies = [];
	protected $uoms = [];
	protected $locations = [];
	protected $types = [];
	protected $buyers = [];

	public function makeLists()
	{
		$this->currencies = ['' => 'Seleccione la moneda'] + Moneda::orderBy('descripcion', 'ASC')->get()->pluck('descripcion', 'idcurrency')->toArray();		

		$this->uoms = ['' => 'Seleccione el unidad de medida'] + Uom::orderBy('uom_code', 'ASC')->get()->pluck('uom_code', 'uom_code')->toArray();		

		$this->locations =  ['' => 'Seleccione la dicrección'] + Direccion::orderBy('location_id', 'ASC')->get()->pluck('description', 'location_id')->toArray();

		$this->types = ['' => 'Seleccione el tipo'] + FndLookupValue::where('idlookup', 18)->orderBy('code_value', 'ASC')->get()->pluck('description', 'code_value')->toArray();

		$this->buyers = ['' => 'Seleccione el comprador'] + 
			Buyer::orderBy('agent_id', 'ASC')->get()->pluck('buyer_name', 'agent_id')->toArray();		
	}

	public function index(Request $request)
	{
	     
	    /*
	    $rsOrders = Orden::id($request->trxStart, $request->trxEnd)
	            ->IdVendor($client)
	            ->Vendor($client)                 
	            ->orderBy('created_at', 'DESC')
	            ->paginate();
	    */
	    $rsOrders = PurchaseOrder::orderBy('created_at', 'desc')->get();             
	        return view('compras.orden.index')->with('rsOrders', $rsOrders);         

	    //return view('compras.orden.index', compact('rsOrders'));
	}
	

	public function create()
	{
	        $this->makeLists();

	        $currencies = $this->currencies;

	        $uoms = $this->uoms;
	        
	        $locations = $this->locations;
	        
	        $types = $this->types;
	        
	        $buyers = $this->buyers;

	        return view("compras.orden.create", compact('currencies', 'uoms', 'locations', 'types', 'buyers')); 
	}

	public function show($id)
	{
	        $this->makeLists();

	        $currencies = $this->currencies;

	        $uoms = $this->uoms;
	        
	        $locations = $this->locations;

	        $types = $this->types;
	        
	        $buyers = $this->buyers;

	        $purchaseOrder =  PurchaseOrder::find($id);

	        return view("compras.orden.show", compact('purchaseOrder', 'currencies', 'uoms', 'locations', 'types', 'buyers')); 
	}	

	public function vendorList()
	{
		$vendors = Proveedor::all();

		return $vendors;
	}

	public function store(Request $request)
	{
		//dd(request()->all());
		$this->validate($request, [
	            'vendor_id' => 'required|',
	            'ship_to_location_id' => 'required',
	            'bill_to_location_id' => 'required',
	            'type_lookup_code' => 'required',
	            'currency_code' => 'required',
	            'agent_id' => 'required',
	        ]);
		$segment1 = PurchaseOrder::count() + 1;
		$purchaseOrder = PurchaseOrder::create($request->except('_token'));
		$purchaseOrder->segment1 = $segment1;
		$purchaseOrder->authorization_status = 'INCOMPLETE';
		$purchaseOrder->last_updated_by = Auth::user()->id;
		$purchaseOrder->created_by = Auth::user()->id;
		$purchaseOrder->save();

		return redirect('compras/orden/' . $purchaseOrder->po_header_id);
	}

	public function update(Request $request, $id)
	{
		//dd(request()->all());
		$this->validate($request, [
	            'vendor_id' => 'required|',
	            'ship_to_location_id' => 'required',
	            'bill_to_location_id' => 'required',
	            'type_lookup_code' => 'required',
	            'currency_code' => 'required',
	            'agent_id' => 'required',
	        ]);

		$purchaseOrder  = PurchaseOrder::find($id);
		
		$purchaseOrder->fill($request->except('_token'));
		$purchaseOrder->last_updated_by = Auth::user()->id;		
		$purchaseOrder->save();

		return redirect('/home');
	}	
}
