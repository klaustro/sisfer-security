<?php

namespace sisVentas\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use sisVentas\Caja;
use sisVentas\Cajero;
use sisVentas\FndLookup;
use sisVentas\Http\Requests;
use sisVentas\InvMaterialTrx;
use sisVentas\InvOnhandQty;
use sisVentas\ItemSite;
use sisVentas\Pos;
use sisVentas\PosDetail;
use sisVentas\PosPay;
use sisVentas\TipoCambio;

class PosController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = FndLookup::where('lookup_type', 'POS')->first();
        $types = isset($types) ? $types->fndLookupValue->pluck('description', 'code_value')->toArray() : [];

        $pay_types = FndLookup::where('lookup_type', 'TIPO_PAGO')->first();
        $pay_types = isset($pay_types) ? $pay_types->fndLookupValue->pluck('description', 'code_value')->toArray() : [];
        $cash = Caja::where('site_id', Session::get('site_id'))
            ->where('idcashier', auth()->user()->cashier->id)
            ->where('end_date', null)
            ->first();
      
        return view('ventas.pos.create', compact('types', 'pay_types', 'cash'));
    }

    public function getArticles($site_id)
    {
        $items = ItemSite::where('site_id', $site_id)->with(['item', 'stock', 'promotion'])->get();

        foreach ($items as $key => $item) {            
            $item->item_disccount = $item->item_disccount;
        }

        //dd($items[0]->disccount);

        return $items;
    }
    
    public function getVendors()
    {
        $vendors = Cajero::all();
        $cashier = [];
        $talfi = [];
        foreach ($vendors as $vendor) {
            if (!$vendor->open_cash) {
                array_push($cashier, [
                    'value' => $vendor->id,
                    'label' => $vendor->name,
                ]);
            }
        }


        return $cashier;
    }    

    public function store()
    {

        $pos = Pos::create(request()->except(['pos_lines', 'pos_pay']));
        $pos->created_by = auth()->user()->id;
        $pos->last_updated_by = auth()->user()->id;
        $pos->save();

        $posLines = request()->pos_lines;

        foreach ($posLines as $line) {
            $posDetail = new PosDetail();
            $posDetail->inv_item_id = $line['inv_item_id'];
            $posDetail->price = $line['price'];
            $posDetail->quantity = $line['quantity'];
            $posDetail->disccount = $line['disccount'];
            $posDetail->tax_rate = $line['tax_rate'];
            $posDetail->tax_id = $line['tax_id'];
            $posDetail->pos_header_id = $pos->header_id;
            $posDetail->site_id = $pos->site_id;
            $posDetail->created_by = auth()->user()->id;
            $posDetail->last_updated_by = auth()->user()->id;
            $posDetail->save();

            $this->updateStock($line);
            $this->storeHistory($line, $posDetail->line_id);
        }


        $posPays = request()->pos_pay;


        foreach ($posPays as $pay) {
            $posPay = new PosPay();
            $posPay->amount = $pay['amount'];
            $posPay->type_lookup_code = $pay['type_lookup_code'];
            $posPay->reference = $pay['reference'];
            $posPay->pos_header_id = $pos->header_id;
            $posPay->save();
        }

        return [
            'pos_id' => $pos->header_id,
            'message' => 'Venta creada exitosamente'
        ];


    }

    protected function updateStock($item)
    {
        $stock = InvOnhandQty::where('item_id', $item['inv_item_id'])
            ->where('site_id', Session::get('site_id'))
            ->where('subinventory_id', $item['subinventory_id'])
            ->where('locator_id', $item['locator_id'])
            ->first();
            
        $stock->primary_transaction_quantity = $stock->primary_transaction_quantity - $item['quantity'];
        $stock->transaction_quantity = $stock->transaction_quantity - $item['quantity'];
        $stock->save();
    }

    protected function storeHistory($item, $detail_id)
    {
        $history = new InvMaterialTrx();
        $history->item_id = $item['inv_item_id'];
        $history->site_id = Session::get('site_id');
        $history->subinventory_id = $item['subinventory_id'];
        $history->locator_id = $item['locator_id'];
        $history->transaction_quantity = $item['quantity'];
        $history->transaction_date = Carbon::now();
        $history->pos_line_id = $detail_id;
        $history->last_updated_by = auth()->user()->id;
        $history->created_by = auth()->user()->id;
        $history->save();
    }
}
