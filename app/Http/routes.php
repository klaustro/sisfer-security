<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('auth/login');
});
Route::get('/acerca', function () {
    return view('acerca');
});

Route::auth();
/*
Route::resource('almacen/categoria','CategoriaController');
Route::resource('almacen/articulo','ItemController'); //'ArticuloController');
Route::resource('almacen/marca','MarcaController');
Route::resource('almacen/familia','FamiliaController');
Route::resource('almacen/modelo','ModeloController');
Route::resource('almacen/labor','LaborController');
Route::resource('asesor/vehiculo','VehiculoController');
Route::resource('ventas/cliente','ClienteController');
Route::resource('ventas/venta','VentaController');
Route::resource('compras/proveedor','ProveedorController');
Route::resource('compras/ingreso','IngresoController');
Route::resource('seguridad/usuario','UsuarioController');
Route::resource('hr/position','PositionController');
Route::resource('configuracion/uom','UomController');

// personal
Route::group(['prefix' => 'hr'], function () {
    Route::post('/personal/{flag}', 'PersonalController@index')->name('all');
    Route::resource('personal', 'PersonalController');
});

Route::get('excel',  'PersonalController@export')->name('export');



//Vehiculos
Route::group(['prefix' => 'asesor'], function () {
    Route::resource('vehiculo', 'VehiculoController');
});
Route::post('select-ajax', ['as'=>'select-ajax','uses'=>'VehiculoController@selectAjax']);

Route::get('excelvehiculos',  'VehiculoController@export')->name('exportvehiculos');


// Cliente
Route::group(['prefix' => 'ventas'], function () {
    Route::post('/cliente/{flag}', 'ClienteController@index')->name('all');
    Route::resource('cliente', 'ClienteController');
});
Route::get('excel2',  'ClienteController@export')->name('exportCliente');
// FIN Cliente

// proveedor
Route::group(['prefix' => 'compras'], function () {
    Route::post('/proveedor/{flag}', 'ProveedorController@index')->name('all');
    Route::resource('proveedor', 'ProveedorController');
});
Route::get('excel3',  'ProveedorController@export')->name('exportvendor');
// FIN proveedor
 
// organizacion

Route::get('/home', 'HomeController@index');

Route::resource('organizacion', 'OrganizacionController');

Route::get('configuracion/organizacion', 'OrganizacionController@index');

Route::get('configuracion/organizacion/create', 'OrganizacionController@create');

Route::post('configuracion/organizacion/store', 'OrganizacionController@store');

Route::post('configuracion/organizacion/update', 'OrganizacionController@update');

Route::get('/reporteorganizacion', 'OrganizacionController@reporte');

Route::get('/home', 'HomeController@index');


// fndlookup

Route::resource('fndlookup', 'FndLookupController');

Route::get('configuracion/fndlookup', 'FndLookupController@index');

Route::get('configuracion/fndlookup/create', 'FndLookupController@create');

Route::post('configuracion/fndlookup/store', 'FndLookupController@store');

Route::post('configuracion/fndlookup/update', 'FndLookupController@update');

Route::get('configuracion/destroy_fndlookup/{id}', 'FndLookupController@destroy');

Route::get('/activar_fndlookup/{id}', 'FndLookupController@activar');

Route::get('/reportefndlookup', 'FndLookupController@reporte');

Route::get('/ajax_edit_lookup', 'FndLookupValueController@ajax_edit_lookup');


// labor

Route::resource('labor', 'LaborController');

Route::get('almacen/labor', 'LaborController@index');

Route::get('almacen/labor/create', 'LaborController@create');

Route::post('almacen/labor/store', 'LaborController@store');

Route::post('almacen/labor/update', 'LaborController@update');

Route::get('almacen/destroy_labor/{id}', 'LaborController@destroy');

Route::get('almacen/activar_labor/{id}', 'LaborController@activar');

Route::get('reportelabor', 'LaborController@reporte');

Route::get('ajax_edit_labor', 'LabormmaController@ajax_edit_labor');


// subinventario

Route::resource('subinventario', 'SubInventarioController');

Route::get('almacen/subinventario', 'SubInventarioController@index');

Route::get('almacen/subinventario/create', 'SubInventarioController@create');

Route::post('almacen/subinventario/store', 'SubInventarioController@store');

Route::post('almacen/subinventario/update', 'SubInventarioController@update');

Route::get('almacen/destroy_subinventario/{id}', 'SubInventarioController@destroy');

Route::get('almacen/activar_subinventario/{id}', 'SubInventarioController@activar');

Route::get('reportesubinventario', 'SubInventarioController@reporte');

Route::get('/ajax_edit_subinventario', 'SubInventarioController@ajax_edit_subinventario');

// Ajax validacion




Route::get('ajax_modelos', 'LaborController@ajax_modelo');

Route::get('ajax_validacion', 'OrganizacionController@validationall');

Route::get('ajax_validacion_update', 'OrganizacionController@validationallupdate');

Route::get('ajax_validad_marca', 'LaborController@ajax_validad_marca');

Route::get('ajax_guardar_marca', 'LabormmaController@store');

Route::get('eliminar_marca', 'LabormmaController@destroy');

Route::get('/ajax_labor_eliminar', 'LaborController@destroy');

Route::get('/ajax_labor_activar', 'LaborController@activar');

Route::get('/ajax_lookup_eliminar', 'FndLookupController@destroy');

Route::get('/ajax_lookup_activar', 'FndLookupController@activar');

Route::get('/guardar_lookup_value', 'FndLookupValueController@store');

Route::get('/eliminar_lookup_value', 'FndLookupValueController@destroy');

Route::get('/ajax_sub_inventario_destroy', 'SubInventarioController@destroy');

Route::get('/ajax_sub_inventario_activar', 'SubInventarioController@activar');



//Reportes
Route::get('reportecategorias', 'CategoriaController@reporte');
Route::get('reportemarcas', 'MarcaController@reporte');
Route::get('reportemodelos', 'ModeloController@reporte');
Route::get('reportefamilias', 'FamiliaController@reporte');
Route::get('reportearticulos', 'ArticuloController@reporte');
Route::get('reporteclientes', 'ClienteController@reporte');
Route::get('reporteproveedores', 'ProveedorController@reporte');
Route::get('reporteventas', 'VentaController@reporte');
Route::get('reporteventa/{id}', 'VentaController@reportec');
Route::get('reporteingresos', 'IngresoController@reporte'); 
Route::get('reporteingreso/{id}', 'IngresoController@reportec'); 
*/

Route::get('/{slug?}', 'HomeController@index');

//JP
Route::group(['prefix' => 'ventas'], function()
{
	//Facturas
	Route::resource('factura', 'FacturaController'); 
	Route::get('factura/delete/{factura}', ['as' => 'ventas.factura.delete', 'uses' => 'FacturaController@delete']);
	Route::get('factura/cancel/{factura}', ['as' => 'ventas.factura.cancel', 'uses' => 'FacturaController@cancel']);
	Route::post('cancelFactura/{factura}', ['as' => 'ventas.recibo.setCancel', 'uses' => 'FacturaController@setCancel']);

	//Recibos
	Route::resource('recibo', 'ReciboController'); 
	Route::get('recibo/delete/{recibo}', ['as' => 'ventas.recibo.delete', 'uses' => 'ReciboController@delete']);
	Route::get('recibo/reverse/{recibo}', ['as' => 'ventas.recibo.reverse', 'uses' => 'ReciboController@reverse']);
	Route::post('reverseRecibo/{recibo}', ['as' => 'ventas.recibo.setReverse', 'uses' => 'ReciboController@setReverse']);

	//ReciboDetalle
	Route::post('reciboDetalle', ['as' => 'ventas.recibo.detalle.store', 'uses' => 'ReciboDetalleController@store']);
	Route::get('reciboDetalle/{recibo}', ['as' => 'ventas.recibo.detalle.show', 'uses' => 'ReciboDetalleController@show']);

	//Articulos
	Route::get('item/{item}', ['as' => 'ventas.item.show', 'uses' => 'ItemController@showFA']);
	Route::get('item', ['as' => 'ventas.item.index', 'uses' => 'ItemController@indexFA']);	

	//FacturaDetalle
	Route::post('facturaDetalle', ['as' => 'ventas.factura.detalle.store', 'uses' => 'FacturaDetalleController@store']);
	Route::get('facturaDetalle/{factura}', ['as' => 'ventas.factura.detalle.show', 'uses' => 'FacturaDetalleController@show']);	

	//Impuestos
	Route::get('tax/{tax}', ['as' => 'ventas.tax', 'uses' => 'ImpuestoController@show']);
	Route::get('tax', ['as' => 'ventas.tax', 'uses' => 'ImpuestoController@index']);

	//Unidad de medida
	Route::get('item/uom/{item}', ['as' => 'ventas.item.uom', 'uses' => 'UomController@show']);
	
	//Clientes
	Route::get('clienteFa/', ['as' => 'ventas.cliente.api', 'uses' => 'ClienteController@showApi']);
	Route::get('clienteFa/{cliente}', ['as' => 'ventas.cliente.factura', 'uses' => 'ClienteController@pendingBills']);

	Route::get('create/plazo/{factura}', ['as' => 'ventas.plazo.create', 'uses' => 'PlazoController@create']);	
	Route::get('plazo/{factura}', ['as' => 'ventas.plazo.show', 'uses' => 'PlazoController@show']);	
	
	Route::post('plazo', ['as' => 'ventas.plazo.store', 'uses' => 'PlazoController@store']);	
});

Route::resource('almacen/warehouse', 'WarehouseController');

Route::get('userSite/user', [ 'as' => 'userSite.user', 'uses' => 'UserSiteController@userList']);
Route::get('userSite/site', [ 'as' => 'userSite.site', 'uses' => 'UserSiteController@siteList']);
Route::get('emailSite/{email}', ['as' => 'emailSite.user', 'uses' => 'UserSiteController@emailSite']);

Route::resource('seguridad/usersite', 'UserSiteController');

Route::get('password/email', 'Auth\PasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');

Route::resource('compras/orden', 'OrdenController');
Route::resource('compras/poDetail', 'PurchaseOrderDetailController');
Route::get('compras/cancelOrden/{order}', 'OrdenController@cancel');
Route::put('compras/cancelOrden/{order}', 'OrdenController@setCancel');
Route::put('compras/approveOrder/{order}', 'OrdenController@Approve');
Route::get('compras/printOrder/{order}', 'OrdenController@print');


Route::get('compras/vendor', 'OrdenController@vendorList');

Route::resource('ventas/pos', 'PosController');
Route::get('ventas/posClientes', 'ClienteController@indexApi');
Route::get('ventas/posArticles/{site_id}', 'PosController@getArticles');
Route::get('/ventas/posVendors', 'PosController@getVendors');
Route::post('ventas/posItemSite', 'ItemSiteController@store');
Route::get('ventas/priceList', 'ItemSiteController@priceList');

Route::resource('ventas/cash', 'CajaController');
Route::post('ventas/openCash', 'CajaController@open');
Route::put('ventas/closeCash/{id}', 'CajaController@close');

Route::get('exchange/rate', 'TipoCambioController@index');



//JP