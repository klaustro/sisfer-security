    @extends ('layouts.admin')
@section ('contenido')

    <div v-cloak class="row">   
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <input type="hidden" ref="site_id" value="{{Session::get('site_id')}}">
                <input type="hidden" ref="tax_id" value="{{\sisVentas\Entity::first()->code_tax}}">
                <input type="hidden" ref="tax_rate" value="{{\sisVentas\Entity::first()->tax_rate}}">
                <input type="hidden" ref="cash_id" value="{{$cash ? $cash->idopamt : ''}}">
                <input type="hidden" ref="barcode_reader" value="{{$cash ? $cash->barcode_reader : ''}}">

                @if (count($errors)>0)
                <div class="alert alert-danger">
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
                </ul>
                </div>
                @endif
                
                <template v-if="!cash_id">
                    <div class="text-center">                        
                        <button class="btn btn-warning btn-lg" data-toggle="modal" data-target="#modalCash">Abrir Caja</button>
                    </div>                    
                </template>
                    @include('ventas.pos.partials.fields')                                             
                    @include('ventas.pos.partials.customer')                                             
                    @include('ventas.pos.partials.checkout')      
                    @include('ventas.pos.partials.cash')                       

                </div>
    </div>
    {{-- @include('ventas.factura.partials.message') --}}
    
@endsection

@section('print')
    @include('ventas.pos.partials.voucher')
@endsection

@section('js')
<script src="{{asset('js/pos.js')}}"></script>
<style>
    .scrolled{
       height: 200px;
       overflow-y: auto;
    }
</style>
@endsection

