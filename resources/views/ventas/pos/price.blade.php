@extends ('layouts.admin')
@section ('contenido')
    <div class="col-md-12">        
        <h3>Descuentos y Promociones</h3>
            <table class="table">
                <tr>
                    <th>ID</th>
                    <th>Producto</th>
                    <th class="text-right">Precio</th>
                    <th class="text-right">% Descuento</th>
                    <th class="text-right">Monto Descuento</th>
                    <th class="text-right">Impuesto</th>
                    <th class="text-right">Promoción</th>
                </tr>
                @foreach($items as $item)
                <tr>
                    <td>{{$item->inv_item_id}}</td>
                    <td>{{$item->item->nombre}}</td>
                    <td class="text-right">{{number_format($item->list_price_per_unit, 2)}}</td>
                    <td class="text-right">{{number_format($item->disccount[0]->rate_disccount * 100, 2)}} %</td>
                    <td class="text-right">{{number_format($item->disccount[0]->rate_disccount * $item->list_price_per_unit, 2)}}</td>
                    <td class="text-right">{{number_format($entity->tax_rate, 2)}}</td>
                    <td class="text-right">
                        {{isset($item->promotion) ? $item->promotion->quantity_start . ' x ' .$item->promotion->quantity_end : ''}}
                    </td>
                </tr>
                @endforeach
            </table>
            <div class="text-center">
                
                {{$items->render()}}
            </div>
    </div>
@endsection