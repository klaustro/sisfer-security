<div class="modal fade" tabindex="-1" role="dialog" id="modalCustomer">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-user" aria-hidden="true"></i> Agregar cliente</h4>
      </div>
      <div class="modal-body">

        <div class="form-group col-md-6">          
        <label>Nombres</label>
        <input class="form-control" type="text" v-model="new_customer.first_name">
        </div>

        <div class="form-group col-md-6">          
        <label>Apellidos</label>
        <input class="form-control" type="text" v-model="new_customer.first_last_name">
        </div>

        <div class="form-group col-md-4">          
        <label>Tipo de Documento</label>
        <input class="form-control" type="text" v-model="new_customer.tipo_documento">
        </div>

        <div class="form-group col-md-4">          
        <label>Nro de Documento</label>
        <input class="form-control" type="text" v-model="new_customer.num_documento">
        </div>

        <div class="form-group col-md-4">          
        <label>Sexo</label>
        <select class="form-control" v-model="new_customer.sex">
          <option value="">Seleccione el sexo</option>
          <option value="F">Femenino</option>
          <option value="M">Masculino</option>
        </select>
        </div>

        <div class="form-group col-md-6">
          <label>Fecha de Nacimiento</label>
          <div class="input-group date">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            <input class="form-control datepicker" type="text" readonly="">
          </div>
        </div>   

        <div class="form-group col-md-6">          
        <label>Distrito</label>
        <input class="form-control" type="text" v-model="new_customer.address">
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" @click="addCustomer" data-dismiss="modal">Guardar</button>
      </div>
    </div>
  </div>
</div>