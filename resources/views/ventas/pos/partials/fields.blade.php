<div  class="col-md-12" v-if="cash_id">
    <div class="box box-solid box-primary">
        <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-barcode" aria-hidden="true"></i> Producto</h3>
        </div>
        <div class="box-body">       
            <v-select v-model="product" :options="items" ref="products" max-height="134px">  
            </v-select>
        </div>
    </div>

</div>

<template v-if="selItems.length > 0">

    <div  class="col-md-12">
        <div v-cloak class="box box-solid box-primary">
            <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-cubes" aria-hidden="true"></i> Líneas</h3>
            </div>
            <div class="box-body">
                <div class="scrolled">
                     
                <table class="table table-responsive">
                    <tr>                    
                        <th>#</th>
                        <th>ID</th>
                        <th>Producto</th>
                        <th>Cantidad</th>
                        <th>Precio unitario</th>
                        <th>Descuento</th>
                        <th>Impuesto</th>
                        <th>Importe</th>
                        <th>Eliminar</th>
                    </tr>
                    <tr v-for="(item, index) in selItems">
                        <td>@{{index + 1}}</td>
                        <td>@{{item.inv_item_id}}</td>
                        <td>@{{item.name}}</td>
                        <td>
                            <button class="btn btn-xs"@click="subs(index)"><i class="fa fa-minus" aria-hidden="true"></i></button>
                            <strong> @{{item.quantity}} </strong>
                            <button class="btn btn-xs"@click="plus(index)"><i class="fa fa-plus" aria-hidden="true"></i></button>
                        </td>
                        <td>@{{item.price | money}}</td>
                        <td>@{{discountAmount(index) | money}}</td>
                        <td>@{{taxAmount(index) | money}}</td>
                        <td>@{{ grossTotal(index) | money}} </td>
                        <td>
                            <button class="btn btn-danger btn-xs" @click="remove(index)">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </button>
                        </td>
                    </tr>
                </table>

                </div>
            </div>
        </div>

    </div>


    <div  class="col-md-9">
        <div v-cloak class="box box-solid box-primary">
            <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-user" aria-hidden="true"></i> Cliente</h3>
            </div>
            <div class="box-body">
                <div class="col-md-11">                
                    <v-select v-model="customer" :options="customers" max-height="180px"></v-select>                
                </div>
                <div class="col-md-1">
                    <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#modalCustomer">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-3">
      <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-money"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Total</span>
          <span class="info-box-number">$  @{{total | money}}</span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>

    <div class="col-md-4">
      <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa fa-file-text-o"></i></span>

        <div class="info-box-content">
          <span class="info-box-number">N° de Líneas: @{{selItems.length}}</span>
          <span class="info-box-number">Total Items: @{{total | money}}</span>
        </div>
      </div>
      <!-- /.info-box -->
    </div>

    <div class="col-md-5">
      <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="fa fa-files-o"></i></span>

        <div class="info-box-content">
          <span class="info-box-number">Tipo de documento</span>
          <span class="info-box-number">
            {!! Form::select('type_lookup_code', $types, 'boleta', ['class' => 'form-control', 'v-model' => 'type']) !!}               
          </span>
        </div>
        <!-- /.info-box-content -->
      </div>
      <!-- /.info-box -->
    </div>    



    <div class="col-md-3">
        <button class="btn btn-danger btn-lg" @click="reset">
            <i class="fa fa-close" aria-hidden="true"></i>
             Cancelar
        </button>
        <template v-if="pos_id == ''">
            <button class="btn btn-success btn-lg" data-toggle="modal" data-target="#modalCheckout" :disabled="!canPay">            
                <i class="fa fa-money" aria-hidden="true"></i>
                Pagar
            </button>
        </template>
        <template v-else>
            <button class="btn btn-warning btn-lg" @click="printVoucher">
                <i class="fa fa-print" aria-hidden="true"></i> Imprimir
            </button>            
        </template>
            
        </template>
    </div>

</template>