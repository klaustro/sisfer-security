<div class="modal fade" tabindex="-1" role="dialog" id="modalCheckout">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Pagar</h4>
      </div>
      <div class="modal-body">
        <div class="form-group col-md-12">
          <label>Medio de pago</label>
          {!! Form::select('pay_type', $pay_types, null, ['class' => 'form-control', 'v-model' => 'pay_type']) !!}
        </div>

      <div class="form-group col-md-12">
        <label>Monto</label>
        <input type="number" v-model="pay_amount" class="form-control" @keyup.13="addPay">
      </div>      

      <div class="form-group col-md-12" v-if="showReference">
        <label>N° de comprobante</label>
        <input type="text" v-model="pay_reference" class="form-control" @keyup.13="addPay">
      </div>

      <div class="form-group col-md-12">
        <div class="text-center">
          <button class="btn btn-success" @click="addPay" :disabled="!canAddPayTaype"><i class="fa fa-plus" aria-hidden="true"></i> Agregar forma de pago</button>         
        </div>
      </div>

      <div class="form-group col-md-12">
        <div class="col-md-6" v-for="(pay, index) in pays">          
          <span class="label label-success">
            @{{pay.type_lookup_code.toUpperCase()}} (@{{pay.amount | money}}) 
            <a href="##" @click="removePay(index)">
                <span class="label label-danger"><i class="fa fa-close" aria-hidden="true"></i></span>
            </a>
          </span>
        </div>
      </div>

      <table class="table">
        <tr>
          <th>Total a pagar</th>
          <td>@{{total | money}}</td>
        </tr>
        <tr>
          <th>Total pagado</th>
          <td>@{{payed | money}}</td>
        </tr>  
        <tr v-if="total - payed > 0">
          <th>Por pagar</th>
          <td>@{{total - payed | money}}</td>
        </tr>        
        <tr v-if="change > 0">
          <th>Vuelto</th>
          <td>@{{change | money}}</td>
        </tr>
      </table>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" :disabled="!totalPayed" data-dismiss="modal" @click="savePos" v-if="pos_id == ''">Confirmar pago</button>
      </div>
    </div>
  </div>
</div>