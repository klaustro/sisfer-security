<div class="modal fade" tabindex="-1" role="dialog" id="modalCash">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Apertura de caja</h4>
      </div>
      <div class="modal-body">

        <div class="form-group checkbox col-md-12">
            <label>
                <input type="checkbox" v-model="barcode_reader"> Lector de Codigo de barras
            </label>
        </div>        

        <div class="form-group col-md-6">
            <label>Monto de Apertura</label>
            <input type="number" v-model="start_amount" class="form-control">
        </div>

        <div class="form-group col-md-6">
            <label>Fecha de Apertura</label>
            <input type='text' class="form-control" value="{{date_format(Carbon\Carbon::now(),'Y-m-d H:i:s')}}" readonly />
        </div>

        <div class="form-group">
            <label class="col-md-12">Tipo de cambio</label>
            <div class="col-md-6">
              <select v-model="rate" class="form-control">
                  <option value="">Seleccione un tipo de cambio</option>
                  <option v-for="rate in rates" :value="rate.rowid">@{{rate.from_currency}}</option>
              </select>
            </div>
            <div class="col-md-6">
                <input type="number" v-model="rate_amount" class="form-control" readonly>
            </div>

        </div>

        <div class="form-group col-md-12">
            <label>Observaciones</label>
            <textarea class="form-control" rows="3" v-model="comment"></textarea>
        </div>        

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" :disabled="!canOpen" data-dismiss="modal" @click="saveCash">Abrir caja</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->