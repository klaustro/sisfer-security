<h3>RUT</h3>
<h3>Boleta N° @{{ pos_id }}</h3>
<p>Giro:</p>
<p>Dirección:</p>
<p>Vendedor: {{auth()->user()->name}}</p>
<p>Fecha: {{date_format(Carbon\Carbon::now(), 'd-m-Y')}}</p>
<p>Forma de pago: <strong v-for="pay in pays" class="text-uppercase">@{{pay.type_lookup_code}} / </strong> </p>
<table class="table">
	<tr>
		<th>CANT</th>
		<th>ITEM</th>
		<th>VALOR U.</th>
		<th>DESC.</th>
		<th>IMP.</th>
		<th>SUBTOTAl</th>
	</tr>
	<tr v-for="(item, index) in selItems">
		<td>@{{item.quantity}}</td>
		<td>@{{item.name | limitText}}</td>
		<td>@{{item.price | money}}</td>
		<td>@{{discountAmount(index) | money}}</td>
		<td>@{{taxAmount(index) | money}}</td>
		<td>@{{grossTotal(index) | money}}</td>
	</tr>
</table>
<div class="text-right">	
	<p>Total pagado: @{{+total + +change | money}}</p>
	<p>Vuelto: @{{change | money}}</p>
	<p>Total: @{{total | money}}</p>
</div>

