@extends ('layouts.admin')
@section ('contenido')

<div class="box box-solid box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Listado de cajas</h3>
    <div class="box-tools pull-right">
      <button class="btn btn-success" data-toggle="modal" data-target="#modalCash" @click="getVendors">Abrir Caja</button>
    </div>
  </div>
  <div class="box-body">
	{{-- Search --}}
  </div>



<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover">
				<thead>
					<th>Almacen</th>
					<th>Vendedor</th>
					<th>Saldo inicial</th>
					<th>Vendido</th>
          <th>Fecha de apertura</th>
					<th>Fecha de cierre</th>
					<th>Acciones</th>
				</thead>
				@foreach ($cashes as $cash)
				<tr>
					<td>{{ $cash->site_name}}</td>
					<td>{{ isset($cash->cashier_name) ? $cash->cashier_name : '' }}</td>
					<td>{{ number_format($cash->start_amount, 2)}}</td>			
					<td>{{ number_format($cash->total, 2)}}</td>			
                    <td>{{ date_format(date_create($cash->start_date),"d-m-Y H:i:s")}}</td>
                    <td>{{ isset($cash->end_date) ? date_format(date_create($cash->end_date),"d-m-Y H:i:s") : ''}}</td>                    
					<td>
                        @if(isset($cash->end_date))
                        <a class="btn btn-info btn-xs" role="button" href="{{url('ventas/cash/' . $cash->idopamt)}}">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a> 
                        @else                                
						<a class="btn btn-danger btn-xs" role="button" href="{{url('ventas/cash/' . $cash->idopamt)}}">
                            <i class="fa fa-close" aria-hidden="true"></i>
                        </a>  
                        @endif                   			
					</td>
				</tr>
				@endforeach
				@include('ventas.caja.partials.cash')
			</table>
		</div>
		<div class="text-center">
			{{$cashes->render()}}			
		</div>
	</div>
</div>
</div>
@endsection