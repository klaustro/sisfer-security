@extends ('layouts.admin')
@section ('contenido')
<div v-cloak class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

	<div class="box box-solid box-primary">
	  <div class="box-header with-border">
	    <h3 class="box-title">Cerrar Caja {{$cash->idopamt}}</h3>
	    <input type="hidden" ref="cash_id" value="{{$cash->idopamt}}">
	    <div class="box-tools pull-right">
	      <!-- Buttons, labels, and many other things can be placed here! -->
	      <!-- Here is a label for example -->
	      <span class="label label-success">Label</span>
	    </div>
	  </div>
	  <div class="box-body">
		<table class="table">
			<tr class="active">
				<th><h4>Resumen de Ventas</h4></th>
				<th class="text-right"><h4>{{number_format($cash->pays_total, 2)}}</h4></th>
			</tr>
			@foreach($cash->sells() as $sell)
			<tr>
				<th>{{key($sell)}}</th>
				<td class="text-right">{{number_format($sell[key($sell)], 2)}}</td>
			</tr>
			@endforeach
		</table>

	  </div>

	  <div class="box-footer">
	  	<div class="pull-right">	  		
	    <a class="btn btn-danger" href="/ventas/cash">Volver</a>
	    @if(!isset($cash->end_date))
	    	<button class="btn btn-success" v-if="!closed" @click="close">Cerrar Caja</button>	  	
	    @endif
	  	</div>
	  </div>
	</div>	

	</div>	
</div>

@endsection

@section('js')
	<script>
		var app = new Vue({
			el: '#app',
			data: function(){
				return{
					closed: false,
				}
			},

			methods:{
				close: function(){
					var answer = confirm("Seguro que desea cerrar la caja?")
					if (answer) {
						var self = this;
						axios.put('/ventas/closeCash/' + this.$refs.cash_id.value)
							.then(function(response){
								alert(response.data.message);
								self.closed = true;
							})
							.catch(function(error){
								alert(error);
							});
					    //some code
					}
				
				}
			},
		});
	</script>
@endsection