<div class="modal fade" tabindex="-1" role="dialog" id="modalCash">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Apertura de caja</h4>
      </div>
      <div class="modal-body">
        <div class="form-group checkbox col-md-12">
            <label>
                <input type="checkbox" v-model="barcode_reader"> Lector de Codigo de barras
            </label>
        </div>

        <div class="form-group col-md-12">
          <label>Cajero responsable</label>
          <v-select v-model="vendor" :options="vendors"></v-select>
        </div>
        
        <div class="form-group col-md-6">
            <label>Monto de Apertura</label>
            <input type="number" v-model="start_amount" class="form-control">
        </div>

        <div class="form-group col-md-6">
            <label>Fecha de Apertura</label>
            <input type='text' class="form-control" value="{{date_format(Carbon\Carbon::now(),'Y-m-d H:i:s')}}" readonly />
        </div>

        <div class="form-group">
            <label class="col-md-12">Tipo de cambio</label>
            <div class="col-md-6">
              <select v-model="rate" class="form-control">
                  <option value="">Seleccione un tipo de cambio</option>
                  <option v-for="rate in rates" :value="rate.rowid">@{{rate.from_currency}}</option>
              </select>
            </div>
            <div class="col-md-6">
                <input type="number" v-model="rate_amount" class="form-control" readonly>
            </div>

        </div>    


        <div class="form-group col-md-12">
            <label>Observaciones</label>
            <textarea class="form-control" rows="3" v-model="comment"></textarea>
        </div>        

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" :disabled="!canOpen" data-dismiss="modal" @click="saveCash">Abrir caja</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@section('js')
<script>
  Vue.component('v-select', VueSelect.VueSelect);     

  var app = new Vue({
    el: '#modalCash',
    data: function(){
      return{
        vendors: [],
        vendor: '',
        cashOpened: false,
        start_amount: '',            
        rate: '',
        rate_amount: '',
        comment: '',
        rates: [],
        cash_id: '',   
        barcode_reader: false,     
      }
    },

    mounted: function(){
        this.getRates();
        this.getVendors();
    },

    watch:{
        rate: function(val){
            this.rate_amount = val == '' ? '' : this.getRate(val).conversion_rate;

        },
    },

    methods:{
        getRates(){
            var self = this;
            axios.get('/exchange/rate')
                .then(function(response){
                    self.rates = response.data
                })
                .catch(function(error){
                    alert(error);
                })
        },
        getVendors: function(){
            var self = this;
            axios.get('/ventas/posVendors')
                .then(function(response){
                    console.log(response.data);
                    self.vendors = response.data;                    
                })
                .catch(function(error){
                    alert(error);
                });
        },

        getRate: function(id){
            for (var i = 0; i < this.rates.length; i++) {
                if(this.rates[i].rowid == id){
                    return this.rates[i];
                }                
            }            
        },        

        saveCash(){
            var self = this;
            axios.post('/ventas/openCash', {        
                idcashier: this.vendor.value,
                start_amount: this.start_amount,
                rate: this.rate,
                comment: this.comment,    
                barcode_reader: this.barcode_reader            
            })
            .then(function(response){
                self.cashOpened = true;
                self.cash_id = response.data.cash_id;
                alert(response.data.message);
                location.reload();
            })
            .catch(function(error){
                alert(error);
            });
        },      
      },

    computed:{
        canOpen: function(){
            return true;
        }      
    }
  });
</script>
@endsection