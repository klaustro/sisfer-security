@extends('layouts.admin')

@section('title', 'Listado de Marcas')

// Los estilos de modal
@section('header_styles')
<link rel="stylesheet" href="{{asset('css/modal.css')}}">
@endsection

@section('contenido')
    <div class="box box-primary">
         
        <div class="box-header with-border">
            <h3 class="box-title">
                Listado de Ordenes de Compra
            </h3>
            <div class="box-tools">

                <div class="text-center">
                    <a class="btn btn-danger btn-sm" href="orden/create">
                        NUEVO REGISTRO
                    </a>
                    <a class="btn btn-success btn-sm" href="{{url('reportecategorias')}}" target="_blank">
                        IMPRIMIR REPORTE
                    </a>
                </div>

            </div>

        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box-body table-responsive no-padding">
                        <table id="table" class="hover" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Nro Orden</th>
                                    <th>Proveedor</th>
                                    <th>Importe</th>
                                    <th>Fecha Creacion</th>
                                    <th>Estado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach ($rsOrders as $orden)
                            <tr>
                                <td>{{ $orden->segment1}}</td>
                                <td>{{ $orden->proveedor->vendor_name}}</td>                  
                                <td>{{ $orden->vendor_id}}</td>
                                <td>{{ date_format(date_create($orden->created_at),"d-m-Y")}}</td>
                                <td>{{ $orden->authorization_status}}</td> 
                                <td>
                                    <a href="orden/{{$orden->po_header_id}}" class="btn btn-sm btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    @if($orden->authorization_status != 'cancelado')
                                        <a href="cancelOrden/{{$orden->po_header_id}}" class="btn btn-sm btn-danger"><i class="fa fa-close" aria-hidden="true"></i></a>
                                    @endif  
                                    <a href="printOrder/{{$orden->po_header_id}}" class="btn btn-sm btn-warning" target="blank">
                                        <i class="fa fa-print" aria-hidden="true"></i>
                                    </a>                                  
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">

                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <!-- footer-->
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->
    </div>

@push ('scripts')
<script>
$('#liAlmacen').addClass("treeview active");
$('#liMarcas').addClass("active");
</script>
@endpush

@endsection

@section('js')
<!-- El modal -->


    <script type="text/javascript">
        $(document).ready(function () {
            $('#table').DataTable({
                "language": {
                    "url": "{{ asset('AdminLTE/plugins/datatables/esp.lang') }}"
                }
            });
        });
    </script>
@endsection