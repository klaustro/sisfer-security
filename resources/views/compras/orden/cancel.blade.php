@extends ('layouts.admin')
@section ('contenido')
<div class="col-md-6 col-md-offset-3">
	<div class="info-box">

	  <span class="info-box-icon bg-red"><i class="fa fa-trash"></i></span>
	  <div class="info-box-content">
	    <span class="info-box-text"><h3>Cancelar Orden {{$po->segment1}} </h3></span>
	    <span class="info-box-number">¿Seguro que desea cancelar la orden  {{$po->segment1}}?</span>
	    <hr>
	    {!!Form::open(['url'=>'/compras/cancelOrden/' . $po->po_header_id,'method'=>'PUT', 'class' => ''])!!}
		    <button type="submit" class="btn btn-danger">Aceptar</button>
		    <a href="/compras/orden"> <button class="btn btn-success">Cancelar</button></a>
	    {!!Form::close()!!}	
	  </div>

	</div>	
</div>

@endsection



