@extends('layouts.admin')
@section('contenido')

    <div class="text-center">    
        <button class="btn btn-lg btn-warning" onClick="window.print();">
            <i class="fa fa-print" aria-hidden="true"></i> Imprimir Orden # {{$order->segment1}}
        </button>
    </div>


@endsection
@section('print')
<div class="col-md-12">
    <table width="100%" border="0" class="table">
        <tr>
            <td width="20%">
                <img src="{{url($userSite->path_image)}}" width="90px" height="px">                    
            </td>
            <td>
                <table>
                    <tr><td><b>{{$order->proveedor->vendor_name}}</b></td></tr>
                    <tr><td>RUC: {{$order->proveedor->segment1}}</td></tr>
                    <tr><td>{{$order->proveedor->address}}</td></tr>
                    <tr><td>{{$order->proveedor->telef1}}</td></tr>
                    <tr><td>{{$order->proveedor->email}}</td></tr>
                </table>                
            </td>

            <td width="20%" valign="top">
                <table>
                    <tr><td>Ticket {{$order->segment1}} </td></tr>
                    <tr><td><b>Fecha</b></td></tr>
                    <tr><td>{{date_format($order->created_at, 'd/m/Y')}}</td></tr>
                </table>              
            </td>
        </tr>

    </table>
    <hr>
    <table class="table table-bordered" width="100%">
        <tr>
            <th>Codigo Articulo</th>
            <th>Nombre</th>
            <th>Cantidad</th>
            <th>Precio</th>
            <th>subtotal</th>
        </tr>
        @foreach($order->detail as $detail)
        <tr>
            <td>{{$detail->item->codigo}}</td>
            <td>{{$detail->item_description}}</td>
            <td class="text-right">{{$detail->quantity}}</td>
            <td class="text-right">{{number_format($detail->unit_price, 2)}}</td>
            <td class="text-right">{{number_format($detail->unit_price * $detail->quantity, 2)}}</td>
        </tr>
        @endforeach
    </table>    
    <br>

    <table class="table table-bordered" width="100%">
        <tr>
            <td width="70%">
                <b>Consideraciones de recepcion</b>
            </td>
            <td width="30%" class="text-center">
                <b>Totales</b>
            </td>
        </tr>
        <tr>
            <td>
                <p style="{font-size: 5px;}">
                    Preparar el texto para que entre la mayor cantidad de texto en un tamaño de letra 7 de word                    
                </p>
            </td>
            <td>
                <table class="table table-bordered">
                    <tr>
                        <th>Subtotal</th>
                        <td class="text-right">{{number_format($order->amount - $order->tax, 2)}}</td>
                    </tr>
                    <tr>
                        <th>IGV {{$order->proveedor->tax->tax_rate}}%</th>
                        <td class="text-right">{{number_format($order->tax, 2)}}</td>
                    </tr>
                    <tr>
                        <th>Total a pagar</th>
                        <td class="text-right">{{number_format($order->amount, 2)}}</td>
                    </tr>
                </table>              
            </td>
        </tr>
    </table>

    <table class="table table-bordered">
        <tr>
            <th>Importe total con letra</th>
        </tr>
        <tr>
            <td>{{$order->total_spelled}}</td>
        </tr>
    </table>    
</div>
@endSection