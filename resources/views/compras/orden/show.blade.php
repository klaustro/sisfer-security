@extends ('layouts.admin')
@section ('contenido')

	<div v-cloak class="row">	
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                @if (count($errors)>0)
                <div class="alert alert-danger">
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
                </ul>
                </div>
                @endif

                {!!Form::open(['url'=>'compras/orden/' . $purchaseOrder->po_header_id,'method'=>'PUT','autocomplete'=>'off'])!!}
                {{Form::token()}}

                    <input type="hidden" ref="rates" value={{$rates}}>
                    <input type="hidden" ref="defaultCurrency" value={{$defaultCurrency}}>

                    
                    @include('compras.orden.partials.fields')                  	                       
                   
                    <div class="form-group">
                        @if($purchaseOrder->authorization_status != 'cancelado')
                            <button class="btn btn-primary" type="submit">Finalizar</button>
                        @endif
                        <a class="btn btn-danger" href="{{url('/compras/orden')}}">Cancelar</a>
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#detailModal" @click="getArticles()">lineas <span class="badge">@{{items.length}}</span></button>

                        @if($purchaseOrder->authorization_status == 'INCOMPLETE')
                            <button class="btn btn-info" @click.prevent="approve" :disabled="items.length == 0">Aprobar</button>                    
                        @endif
                    </div>


                {!!Form::close()!!}		

                </div>
                @include('compras.orden.partials.detalle')
	</div>
    
@endsection

@section('js')
<script src="{{asset('js/vue-select.js')}}"></script>  
<script src="{{asset('js/numeral.js')}}"></script>  
<script src="{{asset('js/po.js')}}"></script>

<script>
        $('.datepicker').datepicker({
            format: "yyyy-mm-dd",
            language: "es",
            autoclose: true,
            viewMode: 'years'
        });    
</script>

@endsection