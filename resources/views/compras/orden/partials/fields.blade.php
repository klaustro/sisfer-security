<div class="col-md-12">
<div  class="col-md-8">
	<div class="box box-solid box-primary">
		<div class="box-header with-border">
		<h3 class="box-title">Transacción</h3>
		</div>
		<div class="box-body">

		<div class="form-group col-md-6">
		{!! Form::label('segment1', 'OC Rev') !!}
		{!! Form::text('segment1',  (isset($purchaseOrder->segment1)) ? $purchaseOrder->segment1 : null , ['class' => 'form-control', 'placeholder' =>'Número de factura', 'readonly'  => isset($purchaseOrder->segment1), 'ref' => 'segment1', 'readonly' => 'true']) !!}
		
		</div>

		<div class="form-group col-md-6">
		{!! Form::label('type_lookup_code', 'Tipo') !!}
		{!! Form::select('type_lookup_code', $types, (isset($purchaseOrder->type_lookup_code)) ? $purchaseOrder->type_lookup_code : null, ['class' => 'form-control', 'disabled' => Route::currentRouteNamed('compras.orden.show')]) !!}
	
		</div>				

		<div class="form-group col-md-6">
		{!! Form::label('ship_to_location_id', 'Envio') !!}
		{!! Form::select('ship_to_location_id', $locations, (isset($purchaseOrder->ship_to_location_id)) ? $purchaseOrder->ship_to_location_id : null, ['class' => 'form-control']) !!}
		</div>	

		<div class="form-group col-md-6">
		{!! Form::label('bill_to_location_id', 'Facturacion') !!}
		{!! Form::select('bill_to_location_id', $locations, (isset($purchaseOrder->bill_to_location_id)) ? $purchaseOrder->bill_to_location_id : null, ['class' => 'form-control']) !!}
		</div>			

		<div class="form-group col-md-6">
		{!! Form::label('agent_id', 'Comprador') !!}
		{!! Form::select('agent_id', $buyers, (isset($purchaseOrder->agent_id)) ? $purchaseOrder->agent_id : null, ['class' => 'form-control']) !!}
		</div>		

		<div class="form-group col-md-6">
		{!! Form::label('agent_id', 'Estado') !!}
		<input type="text" class="form-control" readonly="" value="{{isset($purchaseOrder->authorization_status) ? $purchaseOrder->authorization_status : ''}}">
		</div>					


	</div>

</div>	
</div>

<div  class="col-md-4">
	<div v-cloak class="box box-solid box-primary">
		<div class="box-header with-border">
		<h3 class="box-title">Financiera</h3>
		</div>
		<div class="box-body">

			<div class="form-group col-md-6">
				{!! Form::label('created_ad','Creado') !!}
				<input type="text" class="form-control" value="{{(isset($purchaseOrder->created_at)) ? date_format($purchaseOrder->created_at,"d-m-Y") : null}}" readonly="">
			</div>			

			<div class="form-group col-md-6">
				{!! Form::label('currency_code','Moneda') !!}
				{!! Form::select('currency_code', $currencies, null, ['class' => 'form-control', 'v-model' => 'currency', 'disabled' => Route::currentRouteNamed('compras.orden.show')]) !!}
				<input type="hidden" ref="currency" value="{{(isset($purchaseOrder->currency_code)) ? $purchaseOrder->currency_code : null}}">
			</div>	

			<div class="form-group col-md-6">
				{!! Form::label('total','Total Líneas') !!}
				<input type="text" :value="totalOrden | money" class="form-control" readonly="">
			</div>

			<div class="form-group col-md-6">
				{!! Form::label('total','Total Impuesto') !!}
				<input type="text" :value="totalTax | money" class="form-control" readonly="">
			</div>

			<div class="form-group col-md-12">
				{!! Form::label('total','Total Orden') !!}
				<input type="text" :value="totalOrden + totalTax | money" class="form-control" readonly="">
			</div>
	
		</div>

	</div>
</div>

<div  class="col-md-8">
	<div class="box box-solid box-primary">
		<div class="box-header with-border">
		<h3 class="box-title">Proveedor</h3>
		</div>
		<div class="box-body">
			<div class="form-group col-md-12">
				{!! Form::label('vendor_id', 'Proveedor') !!}
				@if(Route::currentRouteNamed('compras.orden.create'))
					<v-select v-model="vendor" :options="selectVendors"></v-select>	   
				@else 
					<input type="text" v-model="vendor.label" class="form-control" readonly="">
				@endif
				<input type="hidden" ref="vendor_id" name ="vendor_id" value="{{ isset($purchaseOrder->vendor_id) ? $purchaseOrder->vendor_id : null }}" readonly="">
			</div>	

			<div class="form-group col-md-12">
				{!! Form::label('agent_id', 'Dirección') !!}
				<input type="text" class="form-control" readonly="" v-model="vendor_address">
			</div>		

			<div class="form-group col-md-12">
				{!! Form::label('agent_id', 'Telfono') !!}
				<input type="text" class="form-control" readonly="" v-model="vendor_phone">
			</div>				

			<div class="form-group col-md-12">
				{!! Form::label('contact','Contacto') !!}
				{!! Form::text('contact', (isset($purchaseOrder->contact)) ? $purchaseOrder->contact : null, ['class' => 'form-control', 'placeholder' =>'Contacto']) !!}
			</div>				
	</div>

</div>	
</div>

<div  class="col-md-4">
	<div class="box box-solid box-primary">
		<div class="box-header with-border">
		<h3 class="box-title">Términos</h3>
		</div>
		<div class="box-body">
			<div class="form-group col-md-12">
				{!! Form::label('terms_id','Termino de Pago') !!}
				{!! Form::select('terms_id', $terms, (isset($purchaseOrder->terms_id)) ? $purchaseOrder->terms_id : null, ['class' => 'form-control']) !!}
			</div>	

			<div class="form-group col-md-12">
				{!! Form::label('freight_terms_lookup_code','Flete') !!}
				{!! Form::select('freight_terms_lookup_code', $fletes, (isset($purchaseOrder->freight_terms_lookup_code)) ? $purchaseOrder->freight_terms_lookup_code : null, ['class' => 'form-control']) !!}
			</div>	

			<div class="form-group col-md-12">
				{!! Form::label('ship_via_lookup_code','Transportista') !!}
				{!! Form::select('ship_via_lookup_code', $ships, (isset($purchaseOrder->ship_via_lookup_code)) ? $purchaseOrder->ship_via_lookup_code : null, ['class' => 'form-control']) !!}
			</div>		

			<div class="form-group col-md-12">
				{!! Form::label('fob_lookup_code','FOB') !!}
				{!! Form::select('fob_lookup_code', $fobs, (isset($purchaseOrder->fob_lookup_code)) ? $purchaseOrder->fob_lookup_code : null, ['class' => 'form-control']) !!}
			</div>						
	</div>

</div>	
</div>

</div>