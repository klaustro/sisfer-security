<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">

      <div class="modal-body">
	{!! Form::open(['url'=>'#','method'=>'GET','autocomplete'=>'off','role'=>'search', 'name' => 'buscar', 'class' => 'form-inline']) !!}

        <div class="box box-primary">
                    <div class="form-group col-xs-12 col-md-12">
                      {!! Form::label('item_id','Articulo') !!}
                      <v-select v-model="item_id" :options="selectArticles"></v-select>
                    </div>  
                    <br>  
                    <br>  
                    <br>  
                    <br>  


                    <div class="form-group col-xs-4 col-md-4">
                      {!! Form::label('uom_code','Unidad de Medida') !!}
                      {!! Form::select('uom_code',$uoms, null, ['class' => 'form-control item-select', 'v-model' => 'uom_code']) !!}
                    </div>       

                  <div class="form-group col-md-4">
                    <label for="need_by_date"  class="control-label">Fecha Pactada</label>
                    <div class="input-group date">
                      <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                      </div>
                      {!! Form::text('need_by_date', date_format(date_create(\Carbon\Carbon::now()),"Y-m-d"), ['class' => 'form-control datepicker', 'placeholder' => '', 'ref' => 'need_by_date', 'readonly' => 'true']) !!}
                    </div>
                  </div>         

                    <div class="col-lg-4">
                      <div class="input-group">
                      
                      {!! Form::label('quantity', 'Cantidad') !!}
                      {!! Form::number('quantity', null, ['class' => 'form-control item-text', 'v-model' => 'quantity', 'v-on:keydown.enter.prevent' => 'addItem']) !!}
                        <span class="input-group-btn">

                          <button type="button" class="btn btn-success form-control btn-plus" @click="addItem" :disabled="!isFormValid()" style="{position: absolute; top: 13px;}"><i class="fa fa-plus" aria-hidden="true"></i></button>
                        </span>
                      </div>
                    </div>

            <div class="clearfix"></div>
            <hr>

          {!! Form::close() !!}            

          <div class="box-body">
          <template v-if="items.length > 0">
            <table class="table table-responsive table-items">
            <form class="form-inline"></form>
              <tr>
                <th>Nro</th>
                <th>Categoria</th>
                <th>Descripción</th>
                <th>UDM</th>
                <th>Cantidad</th>
                <th>Precio Unitario</th>
                <th>Impuesto</th>
                <th>Importe</th>
                <th>Pactado</th>
                <th>Acciones</th>
              </tr>
                <tr v-for="(item, index) in items">
                 <td>@{{ item.line_num}}</td>

                 <td>@{{item.category_name}}</td>
                 <td>
                  <template v-if="item.editing && item.allow_item_desc_update_flag==1">
                    <input type="text" v-model="draft.descripcion">                    
                  </template>
                  <template v-else>
                  @{{item.item_description | limitText }}                 
                  </template>
                </td>

                 <td>@{{item.unit_meas_lookup_code}}</td>                 

                <template v-if="item.editing">
                  <td><input type="number" v-model="draft.quantity" class="col-xs-12"></td>
                  <td><input type="number" v-model="draft.unit_price" class="col-xs-12"></td>
                </template>
                <template v-else>
                  <td>@{{item.quantity}}</td>
                  <td>@{{item.unit_price | money}}</td>
                </template>

                <td>@{{item.RECOVERABLE_TAX | money}}</td>

                 <td>@{{calculeExtendedAmount(index) | money}}</td>

                 <td>@{{item.need_by_date}}</td>
                  <td> 
                  <template v-if="!isEditing(index)">

                    <button type="button" class="btn btn-info btn-xs" @click="editItem(index)">
                      <i class="fa fa-pencil" aria-hidden="true"></i>
                    </button>   
                    
                    <button type="button" class="btn btn-danger btn-xs" @click="removeItem(index)">
                      <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>

                  </template>
                  <template v-else>

                    <button type="button" class="btn btn-success btn-xs" @click="saveItem(index)">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </button>       

                    <button type="button" class="btn btn-warning btn-xs" @click="undoItem(index)">
                      <i class="fa fa-undo" aria-hidden="true"></i>
                    </button>                          
                    
                  </template> 

                  </td>
                </tr>          
              </table>
            </template>
          </div>

         <div class="box-footer" v-if="totalOrden > 0">
          <table class="table table-striped">
            <tr>
              <th></th>
              <th>Transacción</th>
              <th>Líneas</th>
              <th>Impuesto</th>
            </tr>
            <tr>
              <th>Totales</th>
              <td>@{{ totalOrden + totalTax | money }}</td>
              <td>@{{ totalOrden | money }}</td>
              <td>@{{ totalTax | money }}</td>
            </tr>
          </table>
          </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal" @click="getDetail">Cerrar</button>
        @if($purchaseOrder->authorization_status != 'cancelado')
          <button type="button" class="btn btn-primary" :disabled="!items.length > 0" @click="saveDetail" data-dismiss="modal">Guardar</button>
        @endif      
      </div>
    </div>
  </div>
</div>