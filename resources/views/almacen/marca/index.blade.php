@extends ('layouts.admin')
@section ('contenido')
<div class="row">
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
		<h3>Listado de Marcas <a href="marca/create"><button class="btn btn-success">Nuevo</button></a> <a href="{{url('reportemarcas')}}" target="_blank"><button class="btn btn-info">Reporte</button></a></h3>
		@include('almacen.marca.search')
	</div>
</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover">
				<thead>
					<th>Id</th>
					<th>Nombre</th>
					<th>Estado</th>
					<th>Opciones</th>
				</thead>
               @foreach ($marcas as $cat)
				<tr>
					<td>{{ $cat->idmarca}}</td>
					<td>{{ $cat->nombre}}</td>
					<td>@if($cat->condicion == 1)
                          Activo
                      @else
                          Inactivo
                      @endif
                    </td> 
					<td>
						<a href="{{URL::action('MarcaController@edit',$cat->idmarca)}}"><button class="btn btn-info">Editar</button></a>
                         <a href="" data-target="#modal-delete-{{$cat->idmarca}}" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a>
					</td>
				</tr>
				@include('almacen.marca.modal')
				@endforeach
			</table>
		</div>
		{{$marcas->render()}}
	</div>
</div>
@push ('scripts')
<script>
$('#liAlmacen').addClass("treeview active");
$('#liMarcas').addClass("active");
</script>
@endpush
@endsection