@extends ('layouts.admin')
@section ('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<h3>Editar Marca: {{ $marca->nombre}}</h3>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif

			{!!Form::model($marca,['method'=>'PATCH','route'=>['almacen.marca.update',$marca->idmarca]])!!}
            {{Form::token()}}
            <div class="form-group">
            	<label for="nombre">Nombre</label>
            	<input type="text" name="nombre" class="form-control" value="{{$marca->nombre}}" placeholder="Nombre...">
            </div>
            
            <div class="form-group">     
              @if($marca->condicion == '1')            
                <input type="checkbox"  name="active" class="checkbox" checked="true">Activo
              @else
              	<input type="checkbox"  name="active" class="checkbox"  >Activo
              @endif
            </div>

            <div class="form-group">
            	<button class="btn btn-primary" type="submit">Guardar</button>
            	<a class="btn btn-danger" href="{{url('almacen/marca')}}">Cancelar</a>
            </div>

			{!!Form::close()!!}		
            
		</div>
	</div>
@push ('scripts')
<script>
$('#liAlmacen').addClass("treeview active");
$('#liMarcas').addClass("active");
</script>
@endpush
@endsection