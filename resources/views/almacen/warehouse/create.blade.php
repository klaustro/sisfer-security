@extends ('layouts.admin')
@section ('contenido')

	<div class="row">	
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h3>Nuevo subinventario</h3>
                @if (count($errors)>0)
                <div class="alert alert-danger">
                <ul>
                @foreach ($errors->all() as $error)
                	<li>{{$error}}</li>
                @endforeach
                </ul>
                </div>
                @endif

                {!!Form::open(['url'=>'almacen/warehouse','method'=>'POST','autocomplete'=>'off'])!!}
                {{Form::token()}}

                    <input type="text" name="name" class="form-control">              	                       
                   
                  <div class="form-group">
                    <button class="btn btn-primary" type="submit">Finalizar</button>
                    <a class="btn btn-danger" href="{{url('almacen/warehouse')}}">Cancelar</a>
                  </div>

                {!!Form::close()!!}		

                </div>
	</div>
    
@endsection