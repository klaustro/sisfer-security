@extends ('layouts.admin')
@section ('contenido')

<div class="box box-solid box-primary">
  <div class="box-header with-border">
    <h3 class="box-title">Listado de subinventarios</h3>
    @if(Session::get('site_id') != '')
    <div class="box-tools pull-right">
      <a href="/almacen/warehouse/create"><button class="btn btn-success">Nuevo subinventario</button></a></h3>
    </div>
    @endif
  </div>

</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover">
				<thead>
					<th>ID</th>
					<th>Nombre</th>
					<th>Sucursal</th>
				</thead>
               	@foreach ($warehouses as $warehouse)
				<tr>
					<td>{{ $warehouse->id}}</td>
					<td>{{ $warehouse->name}}</td>
					<td>{{ $warehouse->site->name}}</td>
					<td>
						<a href="{{url('/almacen/warehouse/' .$warehouse->id)}}"><button class="btn btn-info btn-xs">Editar</button></a>
					</td>
				</tr>
			@endforeach
			</table>
		</div>
		<div class="text-center">
			{{$warehouses->render()}}			
		</div>
	</div>
</div>

@endsection

@section('js')
    <script>
        $('.datepicker').datepicker({
            format: "yyyy-mm-dd",
            language: "es",
            autoclose: true,
            viewMode: 'years'
        });

    </script>
@endsection