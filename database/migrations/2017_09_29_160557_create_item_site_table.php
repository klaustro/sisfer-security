<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemSiteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inv_item_site', function (Blueprint $table) {
            $table->increments('iditemsite');
            $table->integer('inv_item_id');
            $table->integer('site_id');
            $table->string('sku')->nullable();
            $table->integer('min_minmax_quantity')->nullable();
            $table->integer('max_minmax_quantity')->nullable();
            $table->decimal('list_price_per_unit')->nullable();
            $table->integer('mtl_transactions_enabled_flag')->nullable();
            $table->string('inventory_item_status_code')->nullable();
            $table->integer('last_updated_by')->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inv_item_site');
    }
}
