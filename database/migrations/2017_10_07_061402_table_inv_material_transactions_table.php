<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableInvMaterialTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inv_material_transactions', function (Blueprint $table) {
            $table->increments('transaction_id');
            $table->integer('item_id')->nullable();
            $table->integer('organization_id')->nullable();
            $table->integer('site_id')->nullable();
            $table->integer('subinventory_id')->nullable();
            $table->integer('locator_id')->nullable();
            $table->integer('transaction_type_id')->nullable();
            $table->decimal('transaction_quantity')->nullable();
            $table->string('transaction_uom')->nullable();
            $table->string('primary_uom_code')->nullable();
            $table->decimal('primary_quantity')->nullable();
            $table->datetime('transaction_date')->nullable();
            $table->string('costed_flag')->nullable();
            $table->string('invoiced_flag')->nullable();
            $table->decimal('actual_cost')->nullable();
            $table->decimal('transaction_cost')->nullable();
            $table->decimal('prior_cost')->nullable();
            $table->decimal('new_cost')->nullable();
            $table->string('currency_code')->nullable();
            $table->decimal('currency_conversion_rate')->nullable();
            $table->integer('currency_conversion_type')->nullable();
            $table->date('currency_conversion_date')->nullable();
            $table->integer('rcv_transaction_id')->nullable();
            $table->integer('shipment_header_id')->nullable();
            $table->integer('shipment_line_id')->nullable();
            $table->integer('pos_line_id')->nullable();
            $table->integer('type_doc_id')->nullable();
            $table->string('num_doc')->nullable();
            $table->string('ref_doc')->nullable();
            $table->string('obs_doc')->nullable();
            $table->integer('last_updated_by')->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inv_material_transactions');        //
    }
}
