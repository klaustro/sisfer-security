<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCcCustomerTrxLinesAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cc_customer_trx_lines_all', function (Blueprint $table) {
            $table->increments('customer_trx_line_id');
            $table->integer('customer_trx_id')->unsigned();
            $table->foreign('customer_trx_id')->references('customer_trx_id')->on('cc_customer_trx_all');
            $table->integer('line_number')->nullable();
            $table->string('item_id')->nullable();
            $table->string('description')->nullable();
            $table->decimal('previous_customer_trx_id')->nullable();
            $table->decimal('previous_customer_trx_line_id')->nullable();
            $table->decimal('quantity_ordered')->nullable();
            $table->decimal('quantity_credited')->nullable();
            $table->decimal('quantity_invoiced')->nullable();
            $table->decimal('unit_selling_price')->nullable();
            $table->string('sales_order')->nullable();
            $table->decimal('sales_order_revision')->nullable();
            $table->string('sales_order_line')->nullable();
            $table->dateTime('sales_order_date')->nullable();
            $table->string('line_type')->nullable();
            $table->integer('tax_id')->nullable();
            $table->string('taxable_flag')->nullable();
            $table->decimal('link_to_cust_trx_line_id')->nullable();
            $table->decimal('tax_rate')->nullable();
            $table->string('uom_code')->nullable();
            $table->string('amount_includes_tax_flag')->nullable();
            $table->decimal('taxable_amount')->nullable();
            $table->decimal('extended_amount')->nullable();
            $table->decimal('revenue_amount')->nullable();
            $table->integer('last_updated_by')->nullable();        
            $table->integer('created_by')->nullable();       
            $table->timestamps();                

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cc_customer_trx_lines_all');
    }
}
