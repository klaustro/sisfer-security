<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMtlTxnRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mtl_txn_request_lines', function (Blueprint $table) {
            $table->increments('line_id')->nullable();
            $table->integer('header_id')->nullable();
            $table->integer('line_numbel')->nullable();
            $table->integer('site_id')->nullable();
            $table->integer('item_id')->nullable();
            $table->integer('from_locator_id')->nullable();
            $table->integer('to_locator_id')->nullable();
            $table->integer('quantity')->nullable();
            $table->integer('quantity_delivered')->nullable();
            $table->integer('quantity_detailed')->nullable();
            $table->date('date_required')->nullable();
            $table->integer('to_site_id')->nullable();
            $table->integer('from_subinventory_id')->nullable();
            $table->integer('to_subinventory_id')->nullable();
            $table->integer('Employee')->nullable();
            $table->string('Comments')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('last_updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mtl_txn_request_lines');
    }
}
