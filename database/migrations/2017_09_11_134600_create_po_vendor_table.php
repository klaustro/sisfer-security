<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoVendorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('po_vendor', function (Blueprint $table) {
            $table->increments('vendor_id');
            $table->string('vendor_name');
            $table->string('segment1')->nullable();
            $table->date('end_date_active')->nullable();
            $table->integer('no_atender')->nullable();
            $table->string('motivo_no_atencion')->nullable();
            $table->string('vendor_type_lookup_code')->nullable();
            $table->integer('invoice_amount_limit')->nullable();
            $table->integer('match_option')->nullable();
            $table->integer('hold_all_payments_flag')->nullable();
            $table->string('hold_unmatched_invoices_flag')->nullable();
            $table->string('hold_invoice_no_validate')->nullable();
            $table->string('hold_reason')->nullable();
            $table->string('purchasing_hold_reason')->nullable();
            $table->string('hold_by')->nullable();
            $table->integer('terms_id')->nullable();
            $table->string('payment_group')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('invoice_currency')->nullable();
            $table->string('pay_currency')->nullable();
            $table->string('telef1')->nullable();
            $table->string('telef2')->nullable();
            $table->string('contacto')->nullable();
            $table->string('email')->nullable();
            $table->string('address')->nullable();            
            $table->integer('created_by')->nullable();
            $table->integer('tax_id')->nullable();
            $table->integer('last_updated_by')->nullable();
            $table->timestamps();
        });   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('po_vendor');
    }
}
