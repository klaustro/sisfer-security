<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoAgentsCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('po_agents', function (Blueprint $table) {
            $table->increments('agent_id');
            $table->integer('person_id');
            $table->integer('location_id');
            $table->integer('idcategoria');
            $table->decimal('authorization_limit');
            $table->integer('enabled');
            $table->integer('last_updated_by');
            $table->integer('created_by');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('po_agents');
    }
}
