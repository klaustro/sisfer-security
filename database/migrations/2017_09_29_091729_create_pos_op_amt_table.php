<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosOpAmtTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pos_op_amt', function (Blueprint $table) {
            $table->increments('idopamt');
            $table->integer('site_id')->nullable();
            $table->integer('idcashier')->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->decimal('start_amount')->nullable();
            $table->string('comment')->nullable();
            $table->decimal('rate')->nullable();
            $table->date('rate_date')->nullable();
            $table->boolean('barcode_reader')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('last_updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pos_op_amt');//
    }
}
