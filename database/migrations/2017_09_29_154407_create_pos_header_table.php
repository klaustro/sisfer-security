<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosHeaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oe_order_headers_all', function (Blueprint $table) {
            $table->increments('header_id'); //header_id
            $table->integer('site_id');//ok
            $table->integer('client_id');//sold_to_customer_id
            $table->string('type_lookup_code');//not exists
            $table->decimal('change')->nullable();
            $table->integer('pos_op_amt_id')->unsigned(); //not exists
            $table->foreign('pos_op_amt_id')->references('idopamt')->on('pos_op_amt');
            $table->integer('created_by')->nullable();//ok
            $table->integer('last_updated_by')->nullable();//ok

            $table->string('order_type')->nullable();
            $table->string('order_number')->nullable();
            $table->date('expiration_date')->nullable();
            $table->integer('order_source_id')->nullable();
            $table->string('orig_sys_document_ref')->nullable();
            $table->date('ordered_date')->nullable();
            $table->date('request_date')->nullable();
            $table->date('pricing_date')->nullable();
            $table->integer('shipment_priority_code')->nullable();
            $table->integer('price_list_id')->nullable();
            $table->decimal('conversion_rate')->nullable();
            $table->integer('conversion_type_code')->nullable();
            $table->date('conversion_rate_date')->nullable();
            $table->string('cust_po_number')->nullable();
            $table->integer('payment_term_id')->nullable();
            $table->integer('shipping_method_code')->nullable();
            $table->integer('freight_carrier_code')->nullable();
            $table->integer('fob_point_code')->nullable();
            $table->integer('freight_terms_code')->nullable();
            $table->string('cancelled_flag')->nullable();
            $table->integer('salesrep_id')->nullable();
            $table->integer('return_reason_code')->nullable();
            $table->integer('earliest_schedule_limit')->nullable();
            $table->string('payment_type_code')->nullable();
            $table->decimal('payment_amount')->nullable();
            $table->string('check_number')->nullable();
            $table->string('credit_card_code')->nullable();
            $table->string('credit_card_holder_name')->nullable();
            $table->string('credit_card_number')->nullable();
            $table->date('credit_card_expiration_date')->nullable();
            $table->string('sales_channel_code')->nullable();
            $table->integer('sold_to_customer_id')->nullable();
            $table->string('sold_to_contact')->nullable();
            $table->integer('ship_from_org_id')->nullable();
            $table->integer('ship_to_customer_id')->nullable();
            $table->integer('bill_to_customer_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('oe_order_headers_all');
    }
}
