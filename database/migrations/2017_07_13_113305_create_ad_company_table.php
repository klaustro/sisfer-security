<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdCompanyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ad_company', function(Blueprint $table)
		{
			$table->integer('rowid', true);
			$table->string('code', 50)->nullable();
			$table->string('name', 50)->nullable();
			$table->string('description', 100)->nullable();
			$table->string('currency_code', 3)->nullable();
			$table->string('logo', 100)->nullable();
			$table->string('server_email', 100)->nullable();
			$table->string('from_email', 50)->nullable();
			$table->integer('type_convertion')->nullable();
			$table->string('code_tax', 15)->nullable();
			$table->decimal('percentage', 10, 0)->nullable();
			$table->timestamps();
			$table->integer('created_by')->nullable();
			$table->integer('last_updated_by')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ad_company');
	}

}
