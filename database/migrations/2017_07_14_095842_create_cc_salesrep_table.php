<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCcSalesrepTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cc_salesrep', function(Blueprint $table)
        {
            $table->increments('salesrep_id');
            $table->integer('sales_credit_type_id')->nullable() ;
            $table->string('name', 50)->nullable();
            $table->integer('status')->nullable() ;
            $table->dateTime('start_date_active')->nullable();
            $table->dateTime('end_date_active')->nullable();
            $table->string('salesrep_number', 30)->nullable();
            $table->integer('org_id')->nullable();
            $table->string('email_address',50)->nullable();
            $table->string('telephone',50)->nullable();
            $table->integer('person_id')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('last_updated_by')->nullable();
            $table->timestamps();

    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cc_salesrep');
    }
}
