<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoLinesAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('po_lines_all', function (Blueprint $table) {
            $table->increments('po_line_id')->nullable();
            $table->integer('po_header_id')->nullable();
            $table->integer('line_num')->nullable();
            $table->integer('line_type_id')->nullable();
            $table->integer('item_id')->nullable();
            $table->integer('category_id')->nullable();
            $table->string('item_description',250)->nullable();
            $table->string('unit_meas_lookup_code')->nullable();
            $table->decimal('list_price_per_unit',10)->nullable();
            $table->decimal('unit_price',10)->nullable();
            $table->decimal('quantity',10)->nullable();
            $table->date('need_by_date')->nullable();
            $table->string('note_to_vendor',240)->nullable();
            $table->string('cancel_flag')->nullable();
            $table->integer('cancelled_by')->nullable();
            $table->date('cancel_date')->nullable();
            $table->string('cancel_reason',250)->nullable();
            $table->string('taxable_flag')->nullable();
            $table->string('tax_name')->nullable();
            $table->string('type_1099')->nullable();
            $table->string('closed_flag')->nullable();
            $table->string('closed_code')->nullable();
            $table->date('closed_date')->nullable();
            $table->string('closed_reason',250)->nullable();
            $table->integer('closed_by')->nullable();
            $table->integer('quotation_id')->nullable();
            $table->integer('project_id')->nullable();
            $table->integer('tax_id')->nullable();
            $table->integer('org_id')->nullable();
            $table->integer('contract_id')->nullable();
            $table->integer('last_updated_by')->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('po_lines_all');
    }
}
