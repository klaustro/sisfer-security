<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFndLookupCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fnd_lookup', function (Blueprint $table) {
            $table->increments('idlookup');
            $table->string('lookup_type');
            $table->string('customization_level');
            $table->string('description');
            $table->string('active');
            $table->integer('created_by');
            $table->integer('last_updated_by');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fnd_lookup');
    }
}
