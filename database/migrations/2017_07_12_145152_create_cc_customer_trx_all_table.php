<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCcCustomerTrxAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cc_customer_trx_all', function (Blueprint $table) {
            $table->increments('customer_trx_id');
            $table->integer('trx_number');
            $table->string('class_code');
            $table->string('class_trx')->nullable();
            $table->decimal('cust_trx_type_id', 15)->nullable();
            $table->dateTime('trx_date');
            $table->decimal('bill_to_contact_id',15)->nullable();
            $table->string('bill_to_contact');
            $table->decimal('source_code',15)->default(1);
            $table->integer('bill_to_customer_id');
            $table->integer('ship_to_customer_id');
            $table->string('ship_to_contact');
            $table->decimal('ship_to_contact_id',15)->nullable();
            $table->decimal('shipment_id',15)->nullable();
            $table->decimal('remit_to_address_id',15)->nullable();
            $table->decimal('term_id',15);
            $table->dateTime('term_due_date');
            $table->integer('salesrep_id')->unsigned();
            $table->string('purchase_order')->nullable();
            $table->string('purchase_order_revision')->nullable();
            $table->dateTime('purchase_order_date')->nullable();
            $table->string('comments');
            $table->string('exchange_rate_type')->nullable();
            $table->dateTime('exchange_date')->nullable();
            $table->decimal('exchange_rate')->nullable();
            $table->string('invoice_currency_code')->nullable();
            $table->string('complete_flag')->default('N');
            $table->decimal('bill_to_address_id',15)->nullable();
            $table->decimal('ship_to_address_id',15)->nullable();
            $table->decimal('receipt_method_id',15)->nullable();
            $table->string('status_trx')->default('REG');
            $table->decimal('doc_sequence_id',15)->nullable();
            $table->decimal('doc_sequence_value',15)->nullable();
            $table->integer('last_updated_by')->nullable();        
            $table->integer('created_by')->nullable();       
            $table->timestamps();            
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cc_customer_trx_all');
    }
}
