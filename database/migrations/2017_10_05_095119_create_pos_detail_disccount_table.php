<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosDetailDisccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pos_detail_disccount', function (Blueprint $table) {
            $table->increments('detail_id');
            $table->decimal('dis_header_id')->nullable();
            $table->integer('site_id')->nullable();
            $table->integer('item_id')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_data')->nullable();
            $table->decimal('rate_disccount')->nullable();
            $table->integer('quantity_disccount')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('last_updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pos_detail_disccount');
    }
}
