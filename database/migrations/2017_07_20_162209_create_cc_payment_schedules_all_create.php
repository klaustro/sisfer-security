<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCcPaymentSchedulesAllCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cc_payment_schedules_all', function(Blueprint $table)
        {
            $table->increments('payment_schedule_id' ,true);
            $table->decimal('amount_due_original')->nullable();
            $table->decimal('amount_due_remaining')->nullable();
            $table->decimal('number_of_due_dates' ,15)->nullable();
            $table->string('status')->nullable();
            $table->string('invoice_currency_code')->nullable();
            $table->string('class')->nullable();
            $table->decimal('cust_trx_type_id' ,15)->nullable();
            $table->integer('customer_id')->nullable();
            $table->decimal('customer_site_use_id' ,15)->nullable();
            $table->integer('customer_trx_id')->nullable();
            $table->integer('cash_receipt_id')->nullable();
            $table->integer('associated_cash_receipt_id')->nullable();
            $table->integer('term_id')->nullable();
            $table->decimal('amount_line_items_original')->nullable();
            $table->decimal('amount_line_items_remaining')->nullable();
            $table->decimal('amount_applied')->nullable();
            $table->decimal('amount_adjusted')->nullable();
            $table->decimal('amount_in_dispute')->nullable();
            $table->decimal('amount_credited')->nullable();
            $table->decimal('tax_original')->nullable();
            $table->decimal('tax_remaining')->nullable();
            $table->decimal('discount_original')->nullable();
            $table->decimal('discount_remaining')->nullable();
            $table->decimal('discount_taken_earned')->nullable();
            $table->string('exchange_rate_type')->nullable();
            $table->dateTime('exchange_date')->nullable();
            $table->decimal('exchange_rate')->nullable();
            $table->string('trx_number')->nullable();
            $table->dateTime('trx_date')->nullable();
            $table->decimal('reversed_cash_receipt_id' ,15)->nullable();
            $table->decimal('amount_adjusted_pending')->nullable();
            $table->string('receipt_confirmed_flag')->nullable();
            $table->decimal('org_id' ,15)->nullable();
            $table->dateTime('last_charge_date')->nullable();
            $table->dateTime('due_date')->nullable();
            $table->decimal('number_of_installment')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('last_updated_by')->nullable();
            $table->timestamps();          
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cc_payment_schedules_all');
    }
}
