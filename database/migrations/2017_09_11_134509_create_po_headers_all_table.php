<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoHeadersAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('po_headers_all', function (Blueprint $table) {
            $table->increments('po_header_id')->nullable();
            $table->integer('agent_id')->nullable();
            $table->string('type_lookup_code')->nullable();
            $table->string('contact')->nullable();
            $table->integer('segment1')->nullable();
            $table->string('enabled_flag')->nullable();
            $table->date('start_date_active')->nullable();
            $table->date('end_date_active')->nullable();
            $table->integer('vendor_id')->nullable();
            $table->integer('ship_to_location_id')->nullable();
            $table->integer('bill_to_location_id')->nullable();
            $table->integer('terms_id')->nullable();
            $table->string('ship_via_lookup_code')->nullable();
            $table->string('fob_lookup_code')->nullable();
            $table->string('freight_terms_lookup_code')->nullable();
            $table->string('status_lookup_code')->nullable();
            $table->string('currency_code')->nullable();
            $table->string('rate_type')->nullable();
            $table->date('rate_date')->nullable();
            $table->decimal('rate',10)->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->decimal('blanket_total_amount',10)->nullable();
            $table->string('authorization_status')->nullable();
            $table->integer('revision_num')->nullable();
            $table->date('revised_date')->nullable();
            $table->string('approved_flag')->nullable();
            $table->date('approved_date')->nullable();
            $table->decimal('amount_limit',10)->nullable();
            $table->decimal('min_release_amount',10)->nullable();
            $table->string('note_to_authorizer')->nullable();
            $table->string('note_to_vendor')->nullable();
            $table->integer('note_to_receiver')->nullable();
            $table->integer('comments')->nullable();
            $table->integer('closed_code')->nullable();
            $table->date('closed_date')->nullable();
            $table->string('cancel_flag')->nullable();
            $table->integer('site_id')->nullable();            
            $table->string('wf_item_type')->nullable();
            $table->string('wf_item_key')->nullable();
            $table->string('consigned_consumption_flag')->nullable();
            $table->integer('org_id')->nullable();
            $table->integer('last_updated_by')->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('po_headers_all');
    }
}
