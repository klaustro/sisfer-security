<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHrPerPeopleInfTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_per_people_inf', function (Blueprint $table) {
            $table->increments('PERSON_ID');
            $table->date('EFFECTIVE_START_DATE')->nullable();
            $table->date('EFFECTIVE_END_DATE')->nullable();
            $table->string('FIRST_NAME')->nullable();
            $table->string('SECOND_NAME')->nullable();
            $table->date('DATE_OF_BIRTH')->nullable();
            $table->string('EMAIL_ADDRESS')->nullable();
            $table->integer('TYPE_DOCUMENT')->nullable();
            $table->integer('EMPLOYEE_NUMBER')->nullable();
            $table->string('first_LAST_NAME')->nullable();
            $table->string('SECOND_LAST_NAME')->nullable();
            $table->string('FULL_NAME')->nullable();
            $table->string('SEX')->nullable();
            $table->integer('condicion')->nullable();
            $table->integer('LAST_UPDATED_BY')->nullable();
            $table->integer('CREATED_BY')->nullable();
            $table->integer('idtipo_doc')->nullable();
            $table->integer('idposition')->nullable();
            $table->integer('asesor')->nullable();
            $table->string('TELEF1')->nullable();
            $table->string('TELEF2')->nullable();
            $table->integer('SOLD_MIN')->nullable();
            $table->integer('SALARY')->nullable();
            $table->integer('DISCCOUNT')->nullable();
            $table->string('COUNTRY')->nullable();
            $table->string('ADDRESS')->nullable();          
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hr_per_people_inf');
    }
}
