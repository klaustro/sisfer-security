<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClienteTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cliente', function(Blueprint $table)
		{
			$table->increments('idcliente');
			$table->string('first_name', 25);
			$table->string('second_name', 25)->nullable();
			$table->string('first_last_name', 25);
			$table->string('second_last_name', 25);
			$table->string('full_name', 100);
			$table->date('effective_end_date')->nullable();
			$table->date('date_of_birth')->nullable();
			$table->string('sex', 1);
			$table->integer('tipo_documento');
			$table->string('num_documento', 25);
			$table->string('email_address', 50)->nullable();
			$table->string('contacto', 50)->nullable();
			$table->string('telef1', 20);
			$table->string('telef2', 25)->nullable();
			$table->boolean('no_atender')->default(0);
			$table->text('motivo_no_atencion', 65535)->nullable();
			$table->integer('disccount')->nullable();
			$table->string('country', 25)->nullable();
			$table->string('address', 50)->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('last_updated_by')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cliente');
	}

}
