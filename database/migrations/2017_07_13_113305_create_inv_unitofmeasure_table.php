<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvUnitofmeasureTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inv_unitofmeasure', function(Blueprint $table)
		{
			$table->increments('iduom', true);
			$table->string('uom_code', 15)->unique('unq_uom');
			$table->string('description', 50)->nullable();
			$table->integer('idclase')->nullable();
			$table->boolean('base_uom_flag')->default(0);
			$table->boolean('condicion')->default(1);
			$table->timestamps();
			$table->integer('last_updated_by')->nullable();
			$table->integer('created_by')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('inv_unitofmeasure');
	}

}
