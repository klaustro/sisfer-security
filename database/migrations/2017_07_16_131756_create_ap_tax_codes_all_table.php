<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApTaxCodesAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ap_tax_codes_all', function(Blueprint $table)
        {
            $table->increments('tax_id', true);
            $table->string('tax_type',25)->nullable();
            $table->string('code_tax',15)->nullable();
            $table->string('name',15)->nullable();
            $table->string('description',240)->nullable();
            $table->decimal('tax_rate',10)->nullable();
            $table->date('inactive_date')->nullable();
            $table->date('start_date')->nullable();
            $table->integer('org_id')->nullable();
            $table->integer('enabled_flag')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('last_updated_by')->nullable();
            $table->timestamps();          
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ap_tax_codes_all');
    }
}
