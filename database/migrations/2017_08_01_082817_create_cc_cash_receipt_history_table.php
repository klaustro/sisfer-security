<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCcCashReceiptHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cc_cash_receipt_history', function(Blueprint $table)
        {
            $table->increments('cc_cash_receipt_history_id' ,true);
            $table->decimal('amount' ,15);
            $table->string('status');
            $table->dateTime('date');
            $table->string('reason');
            $table->integer('cash_receipt_id');
            $table->integer('last_updated_by');
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cc_cash_receipt_history');
    }
}
