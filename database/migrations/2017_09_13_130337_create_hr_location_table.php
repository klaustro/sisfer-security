<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHrLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hr_location', function (Blueprint $table) {
            $table->increments('location_id');
            $table->string('location_code')->nullable();
            $table->string('adress_line_1')->nullable();
            $table->string('adress_line_2')->nullable();
            $table->string('adress_line_3')->nullable();
            $table->string('country')->nullable();
            $table->string('description')->nullable();
            $table->integer('ship_to_site_flag')->nullable();
            $table->integer('bill_to_site_flag')->nullable();
            $table->integer('receiving_site_flag')->nullable();
            $table->date('inactive_date')->nullable();
            $table->integer('last_updated_by')->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('hr_location');
    }
}
