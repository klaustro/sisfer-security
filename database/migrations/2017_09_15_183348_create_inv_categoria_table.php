<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvCategoriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inv_categoria', function (Blueprint $table) {      
            $table->increments('idcategoria');
            $table->string('nombre')->nullable();
            $table->string('descripcion')->nullable();
            $table->integer('condicion')->nullable();
            $table->integer('last_updated_by')->nullable();
            $table->integer('created_by')->nullable();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inv_categoria');        //
    }
}
