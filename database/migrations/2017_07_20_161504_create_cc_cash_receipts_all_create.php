<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCcCashReceiptsAllCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cc_cash_receipts_all', function(Blueprint $table)
        {
            $table->increments('cash_receipt_id', true);
            $table->string('type')->nullable();
            $table->string('receipt_number')->nullable();
            $table->dateTime('receipt_date')->nullable();
            $table->string('currency_code')->nullable();
            $table->decimal('amount')->nullable();
            $table->string('comments')->nullable();
            $table->decimal('receipt_method_id' ,15)->nullable();
            $table->dateTime('receipt_due_date')->nullable();
            $table->string('status')->nullable();
            $table->integer('pay_from_customer')->nullable();
            $table->integer('customer_bank_account_id')->nullable();
            $table->decimal('customer_bank_branch_id' ,15)->nullable();
            $table->string('exchange_rate_type')->nullable();
            $table->decimal('exchange_rate')->nullable();
            $table->dateTime('exchange_date')->nullable();
            $table->string('confirmed_flag')->nullable();
            $table->decimal('customer_site_use_id' ,15)->nullable();
            $table->dateTime('deposit_date')->nullable();
            $table->decimal('doc_sequence_value' ,15)->nullable();
            $table->decimal('doc_sequence_id' ,15)->nullable();
            $table->decimal('org_id' ,15)->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('last_updated_by')->nullable();
            $table->timestamps();          
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cc_cash_receipts_all');
    }
}
