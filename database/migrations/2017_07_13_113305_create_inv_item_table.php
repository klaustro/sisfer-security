<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('inv_item', function(Blueprint $table)
		{
			$table->increments('inv_item_id', true);
			$table->integer('idcategoria')->nullable();
			$table->string('codigo', 250)->nullable();
			$table->string('nombre', 250)->nullable();
			$table->string('descripcion', 512)->nullable();
			$table->string('imagen', 50)->nullable();
			$table->boolean('enabled_flag')->nullable();
			$table->string('locator_id', 10)->nullable();
			$table->boolean('purchasing_item_flag')->default(0);
			$table->boolean('shippable_item_flag')->default(0);
			$table->boolean('customer_order_flag')->default(0);
			$table->boolean('service_item_flag')->default(0);
			$table->boolean('inventory_item_flag')->default(0);
			$table->boolean('eng_item_flag')->default(0);
			$table->boolean('inventory_asset_flag')->default(0);
			$table->boolean('mtl_transactions_enabled_flag')->default(1);
			$table->boolean('stock_enabled_flag')->default(0);
			$table->boolean('returnable_flag')->default(0);
			$table->string('qty_rcv_exception_code', 25)->nullable();
			$table->boolean('allow_item_desc_update_flag')->default(0);
			$table->boolean('inspection_required_flag')->default(0);
			$table->boolean('receipt_required_flag')->default(0);
			$table->boolean('rfq_required_flag')->default(0);
			$table->decimal('list_price_per_unit', 10, 0)->nullable();
			$table->decimal('price_sell', 10, 0)->nullable()->default(0);
			$table->integer('asset_category_id')->nullable();
			$table->integer('days_early_receipt_allowed')->nullable();
			$table->integer('days_late_receipt_allowed')->nullable();
			$table->string('primary_uom_code', 15)->nullable();
			$table->string('inventory_item_status_code', 10)->default('active');
			$table->integer('inventory_planning_code')->nullable();
			$table->integer('min_minmax_quantity')->nullable();
			$table->integer('max_minmax_quantity')->nullable();
			$table->integer('minimum_order_quantity')->nullable();
			$table->integer('maximum_order_quantity')->nullable();
			$table->integer('costing_enabled_flag')->nullable();
			$table->boolean('invoiceable_item_flag')->default(0);
			$table->string('item_type', 30)->nullable();
			$table->boolean('consigned_flag')->default(0);
			$table->boolean('critical_component_flag')->default(0);
			$table->timestamps();
			$table->integer('last_updated_by')->nullable();
			$table->integer('created_by')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('inv_item');
	}

}
