<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosPromotionsHeaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pos_promotions_header', function (Blueprint $table) {
            $table->increments('header_id');
            $table->string('name')->nullable();
            $table->string('apply_autom_flag')->nullable();
            $table->date('start_date')->nullable();
            $table->integer('status')->nullable();
            $table->date('end_date')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('last_updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pos_promotions_header');
    }
}
