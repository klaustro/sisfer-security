<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCcTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cc_terms', function(Blueprint $table)
        {
            $table->integer('term_id');
            $table->integer('credit_check_flag')->nullable();
            $table->integer('start_date_active')->nullable();
            $table->dateTime('end_date_active')->nullable();
            $table->integer('base_amount')->nullable();
            $table->integer('calc_discount_on_lines_flag')->nullable();
            $table->integer('in_use')->nullable();
            $table->integer('partial_discount_flag')->nullable();
            $table->string('name', 50);
            $table->string('description', 100)->nullable();
            $table->integer('prepayment_flag')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('last_updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cc_terms');
    }
}
