<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFndLookupValueCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fnd_lookup_value', function (Blueprint $table) {
            $table->increments('idlvalue');
            $table->integer('idlookup');
            $table->string('code_value');
            $table->string('description');
            $table->date('date_from');
            $table->date('date_to');
            $table->integer('active');
            $table->integer('created_by');
            $table->integer('last_updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fnd_lookup_value');
    }
}
