<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosLineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oe_order_lines_all', function (Blueprint $table) {
            $table->increments('line_id');
            $table->integer('inv_item_id')->nullable();          
            $table->decimal('price')->nullable();
            $table->decimal('disccount')->nullable();
            $table->integer('quantity')->nullable();
            $table->decimal('tax_rate')->nullable();
            $table->integer('tax_id')->nullable();
            $table->integer('pos_header_id')->unsigned();            
            $table->foreign('pos_header_id')->references('header_id')->on('oe_order_headers_all');  

            $table->integer('site_id')->nullable();
            $table->integer('header_id')->nullable();
            $table->integer('line_category_id')->nullable();
            $table->integer('line_number')->nullable();
            $table->string('item_description')->nullable();
            $table->date('promise_date')->nullable();
            $table->date('schedule_ship_date')->nullable();
            $table->string('order_quantity_uom')->nullable();
            $table->decimal('cancelled_quantity')->nullable();
            $table->decimal('shipped_quantity')->nullable();
            $table->decimal('ordered_quantity')->nullable();
            $table->integer('delivery_lead_time')->nullable();
            $table->integer('ship_from_site_id')->nullable();
            $table->integer('ship_to_site_id')->nullable();
            $table->integer('cust_po_number')->nullable();
            $table->integer('ship_tolerance_above')->nullable();
            $table->integer('item_id')->nullable();
            $table->date('tax_date')->nullable();
            $table->string('tax_code')->nullable();
            $table->integer('price_list_id')->nullable(); 
            $table->integer('shipment_number')->nullable();
            $table->integer('shipment_priority_code')->nullable();
            $table->decimal('unit_selling_price')->nullable();
            $table->integer('last_updated_by')->nullable();
            $table->integer('created_by')->nullable();

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('oe_order_lines_all');        //
    }
}
