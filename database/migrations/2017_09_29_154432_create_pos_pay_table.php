<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosPayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pos_pay', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type_lookup_code');
            $table->decimal('amount');
            $table->string('reference')->nullable();
            $table->integer('pos_header_id')->unsigned();            
            $table->foreign('pos_header_id')->references('header_id')->on('oe_order_headers_all');              
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {    
        Schema::drop('pos_pay');    
    }
}
