<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoDistributionAllTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('po_distribution_all', function (Blueprint $table) {
            $table->increments('po_distribution_id');
            $table->integer('distribution_num');
            $table->integer('po_header_id');
            $table->integer('po_line_id');
            $table->decimal('quantity_ordered',10);
            $table->integer('po_release_id');
            $table->decimal('quantity_delivered',10);
            $table->decimal('quantity_billed',10);
            $table->decimal('quantity_cancelled',10);
            $table->integer('req_header_reference_num');
            $table->integer('req_line_reference_num');
            $table->integer('req_distribution_id');
            $table->integer('deliver_to_location_id');
            $table->integer('deliver_to_person_id');
            $table->date('rate_date');
            $table->decimal('rate',10);
            $table->decimal('recoverable_tax',10);
            $table->decimal('amount_billedd',10);
            $table->string('destination_type_code');
            $table->integer('destination_organization_id');
            $table->integer('destination_subinventory');
            $table->integer('source_distribution_id');
            $table->date('gl_closed_date');
            $table->integer('org_id');
            $table->decimal('amount_ordered',10);
            $table->decimal('amount_delivered',10);
            $table->decimal('amount_cancelled',10);
            $table->integer('last_updated_by');
            $table->integer('created_by');
            $table->timestamps();
        });   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('po_distribution_all');
    }
}
