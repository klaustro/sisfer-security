<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMtlTxnRequestHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mtl_txn_request_headers', function (Blueprint $table) {
            $table->integer('header_id')->nullable();  
            $table->string('request_number')->nullable(); 
            $table->integer('transaction_type_id')->nullable();
            $table->integer('site_id')->nullable();
            $table->string('description')->nullable();
            $table->date('date_required')->nullable();  
            $table->integer('from_subinventory_id')->nullable();   
            $table->integer('to_subinventory_id')->nullable(); 
            $table->integer('header_status')->nullable();  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mtl_txn_request_headers');
    }
}
