<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGlDailyRateTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gl_daily_rate', function(Blueprint $table)
		{
			$table->increments('rowid', true);
			$table->string('from_currency', 15)->nullable();
			$table->string('to_currency', 15)->nullable();
			$table->date('conversion_date')->nullable();
			$table->integer('conversion_type')->nullable();
			$table->decimal('conversion_rate', 10, 2)->nullable();
			$table->boolean('condicion')->default(1)->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('last_updated_by')->nullable();
			$table->timestamps();
			$table->unique(['from_currency','to_currency','conversion_date','conversion_type'], 'idx_rec');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gl_daily_rate');
	}

}
