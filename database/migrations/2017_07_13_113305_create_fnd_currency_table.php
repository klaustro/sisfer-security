<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFndCurrencyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fnd_currency', function(Blueprint $table)
		{
			$table->string('currency_code', 15)->unique('currency_code');
			$table->string('descripcion', 50)->nullable();
			$table->string('simbolo', 15)->nullable();
			$table->integer('idcurrency', true);
			$table->integer('condicion')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('last_updated_by')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fnd_currency');
	}

}
