<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvOnhandQuantitiesDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inv_onhand_quantities_detail', function (Blueprint $table) {
            $table->increments('onhand_quantities_id');
            $table->integer('item_id')->nullable();
            $table->integer('organization_id')->nullable();
            $table->integer('site_id')->nullable();
            $table->datetime('date_received')->nullable();
            $table->decimal('primary_transaction_quantity')->nullable();
            $table->integer('subinventory_id')->nullable();
            $table->integer('locator_id')->nullable();
            $table->integer('create_transaction_id')->nullable();
            $table->datetime('orig_date_received')->nullable();
            $table->string('transaction_uom_code')->nullable();
            $table->decimal('transaction_quantity')->nullable();
            $table->boolean('is_consigned')->nullable();
            $table->integer('last_updated_by')->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inv_onhand_quantities_detail');
    }
}
