<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePosPromotionsLineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pos_promotions_line', function (Blueprint $table) {
            $table->increments('detail_id');
            $table->integer('header_id')->nullable();
            $table->integer('site_id')->nullable();
            $table->integer('item_id')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_data')->nullable();
            $table->integer('quantity_start')->nullable();
            $table->integer('quantity_end')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('last_updated_by')->nullable();
            $table->timestamps();
        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pos_promotions_line');
    }
}

