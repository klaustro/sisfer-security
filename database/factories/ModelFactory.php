<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(sisVentas\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});



$factory->define(sisVentas\Entity::class, function (Faker\Generator $faker) {
    return [
        'rowid' => $faker->unique()->numberBetween(1, 5),
        'name' => $faker->word(),
        'code_tax' => $faker->numberBetween(1, 5),
        'currency_code' => 'PEN',
        'type_convertion' => '1',
    ];
});

$factory->define(sisVentas\Impuesto::class, function (Faker\Generator $faker) {
    return [
      'name' => $faker->unique->word,
      'tax_rate' => $faker->randomFloat(2, 8, 20),
    ];
});

$factory->define(sisVentas\Categoria::class, function (Faker\Generator $faker) {
    return [
      'nombre' => $faker->word,
    ];
});

$factory->define(sisVentas\Item::class, function (Faker\Generator $faker) {
  $name = $faker->word();
    return [
        'codigo' => $name,
        'nombre' => $name,
        'descripcion' => $faker->sentence(),
        'primary_uom_code' => $faker->randomElement(['LIT','BOX','KG','UNI', 'DOC']),
        'price_sell' => $faker->numberBetween(100, 1000),
        'list_price_per_unit' => $faker->numberBetween(100, 1000),
        'idcategoria' => $faker->numberBetween(1, 10),
        'allow_item_desc_update_flag' => $faker->numberBetween(0, 1),

    ];
});

$factory->define(sisVentas\SalesPerson::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name(),
    ];
});

$factory->define(sisVentas\TerminoPagoAR::class, function (Faker\Generator $faker) {
    return [
        'term_id' => $faker->unique()->numberBetween(1, 500),
        'name' => $faker->word(),
        'description' => $faker->sentence(),
    ];
});

$factory->define(sisVentas\Uom::class, function (Faker\Generator $faker) {
    return [
        'uom_code' => $faker->unique()->randomElement(['LIT','BOX','KG','UNI', 'DOC']),
        'description' => $faker->word(),
        'idclase' => $faker->numberBetween(1, 500),
        'base_uom_flag' => $faker->boolean,
        'condicion' => $faker->boolean,
        'last_updated_by' => $faker->numberBetween(1, 500),
        'created_by'  => $faker->numberBetween(1, 500),
    ];
});

$factory->define(sisVentas\Moneda::class, function (Faker\Generator $faker) {
  $currency = $faker->unique()->currencyCode;
    return [
      'idcurrency' => $faker->unique()->randomNumber,
      'currency_code' => $currency,
      'condicion' => $faker->numberBetween(1, 5),
      'descripcion' => $currency 
    ];
});


$factory->define(sisVentas\Cliente::class, function (Faker\Generator $faker) {
    return [            
            'first_name' => $faker->firstName(),
            'second_name' => $faker->firstName(),
            'first_last_name' => $faker->lastName(),
            'second_last_name' => $faker->LastName(),
            'full_name' => $faker->word(),
            'effective_end_date' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
            'date_of_birth' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
            'sex' => $faker->randomElement(['M','F']),
            'tipo_documento' => $faker->randomNumber,
            'num_documento' => $faker->randomNumber(),
            'email_address' => $faker->word(),
            'contacto' => $faker->word(),
            'telef1' => substr($faker->phoneNumber, 0, 20),
            'telef2' => substr($faker->phoneNumber, 0, 20),
            'no_atender' => $faker->boolean,
            'motivo_no_atencion' => $faker->word(),
            'disccount' => $faker->randomNumber,
            'country' => $faker->word(),
            'address' => substr($faker->address(), 0, 20),
            'created_by' => $faker->numberBetween(1,20),
            'last_updated_by' => $faker->numberBetween(1,20),

    ];
});

$factory->define(sisVentas\Factura::class, function (Faker\Generator $faker) {
    $client_id = $faker->numberBetween(1,5);
    $trx_number = $faker->unique()->randomNumber;
    $class_code = $faker->numberBetween(1,4);
    return [
           'trx_number' => $trx_number,
           'class_code' => $class_code,
           'class_trx' => $class_code .  $trx_number,
           'cust_trx_type_id'  => $faker->randomNumber,
           'trx_date' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
           'bill_to_contact_id' => $faker->randomNumber,
           'bill_to_contact' => $faker->word(),
           'source_code' => $faker->randomNumber,
           'bill_to_customer_id' =>$client_id,
           'ship_to_customer_id' => $client_id,
           'ship_to_contact' => $faker->word(),
           'ship_to_contact_id' => $faker->randomNumber,
           'shipment_id' => $faker->randomNumber,
           'remit_to_address_id' => $faker->randomNumber,
           'term_id' => $faker->randomNumber,
           'term_due_date' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
           'salesrep_id' => $faker->numberBetween(1, 50),
           'purchase_order' => $faker->word(),
           'purchase_order_revision' => $faker->word(),
           'purchase_order_date' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
           'comments' => $faker->word(),
           'exchange_rate_type' => $faker->word(),
           'exchange_date' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
           'exchange_rate' => $faker->numberBetween(1, 500),
           'invoice_currency_code' => $faker->currencyCode,
           'complete_flag' => $faker->randomElement(['Y', 'N']),
           'bill_to_address_id' => $faker->randomNumber,
           'ship_to_address_id' => $faker->randomNumber,
           'receipt_method_id' => $faker->randomNumber,
           'doc_sequence_id' => $faker->randomNumber,
           'doc_sequence_value' => $faker->randomNumber,
            'created_by' => $faker->numberBetween(1,20),
            'last_updated_by' => $faker->numberBetween(1,20), 
    ];
});

$factory->define(sisVentas\FacturaDetalle::class, function (Faker\Generator $faker) {
  //$items = sisVentas\Item::all()->pluck('codigo')->toArray();
  //dd($items);
    return [            
            'customer_trx_id' => function (){
              return factory(\sisVentas\Factura::class)->create()->customer_trx_id;
            } ,
            'line_number' => $faker->numberBetween(1, 50),
            'item_id' => $faker->numberBetween(343, 953),
            'description' => $faker->word(),
            'previous_customer_trx_id' => $faker->numberBetween(1, 50),
            'previous_customer_trx_line_id' => $faker->numberBetween(1, 50),
            'quantity_ordered' => $faker->numberBetween(1, 10),
            'quantity_credited' => $faker->numberBetween(1, 10),
            'quantity_invoiced' => $faker->numberBetween(1, 10),
            'unit_selling_price' => $faker->numberBetween(1, 1000),
            'sales_order' => $faker->word(),
            'sales_order_revision' => $faker->numberBetween(1, 50),
            'sales_order_line' => $faker->word(),
            'sales_order_date' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
            'line_type' => $faker->word(),
            'tax_id' => $faker->numberBetween(1, 5),
            'taxable_flag' => $faker->word(),
            'link_to_cust_trx_line_id' => $faker->numberBetween(1, 50),
            'tax_rate' => $faker->numberBetween(1, 5),
            'uom_code' => $faker->word(),
            'amount_includes_tax_flag' => $faker->word(),
            'taxable_amount' => $faker->numberBetween(1, 50),
            'extended_amount' => $faker->numberBetween(1, 50),
            'revenue_amount' => $faker->numberBetween(1, 50),
            'created_by' => $faker->numberBetween(1,20),
            'last_updated_by' => $faker->numberBetween(1,20),    

    ];
});

$factory->define(sisVentas\Recibo::class, function (Faker\Generator $faker) {
    return [
      'receipt_method_id' => $faker->numberBetween(1, 5),
      'receipt_number' => $faker->numberBetween(1, 1000),
      'Amount' => $faker->numberBetween(1, 1000),
      'currency_code' => $faker->currencyCode,
      'type' => $faker->word(),
      'status' => $faker->word(),
      'receipt_date' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
      'receipt_due_date' =>date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
      'comments' => $faker->sentence(),
      'pay_from_customer' => $faker->numberBetween(1, 5),
      'customer_bank_account_id' => $faker->randomNumber,
      'customer_bank_branch_id' => $faker->numberBetween(1, 5),
      'created_by' => $faker->numberBetween(1,20),
      'last_updated_by' => $faker->numberBetween(1,20),      
    ];
});

$factory->define(sisVentas\ReciboDetalle::class, function (Faker\Generator $faker) {
    return [    
      'customer_trx_id' => $faker->numberBetween(1, 50),
      'cash_receipt_id' => $faker->numberBetween(1, 50),
      'amount_applied' => $faker->numberBetween(1, 1000),
      'created_by' => $faker->numberBetween(1,20),
      'last_updated_by' => $faker->numberBetween(1,20),
    ];
});

$factory->define(sisVentas\Plazo::class, function (Faker\Generator $faker) {
    return [ 
      'customer_trx_id' => $faker->numberBetween(1, 50),
      'term' =>$faker->numberBetween(1, 4),
      'status' => 'No Pagado',
      'due_date' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
      'amount' => $faker->numberBetween(1, 500),     
    ];
});

$factory->define(sisVentas\Site::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->cityPrefix,
        'description' => $faker->unique()->city,
    ];
});

$factory->define(sisVentas\UserSite::class, function (Faker\Generator $faker) {
        return[
            'IDUSUARIO' => $faker->numberBetween(1,5),
            'IDSITE' => $faker->unique()->numberBetween(1,5),
            'FEC_INI' =>date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
            'FEC_FIN' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
            'path_image' => 'img/logo.png',
            'ULTACTPOR' => $faker->randomDigitNotNull,
            'CREPOR' => $faker->randomDigitNotNull,                
        ];
});

$factory->define(sisVentas\Warehouse::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->word,
        'site_id' => $faker->randomDigitNotNull,
    ];
});

$factory->define(sisVentas\Proveedor::class, function (Faker\Generator $faker) {
    return [
      'vendor_name' => $faker->company,
      'segment1' => $faker->creditCardNumber,
      'email' => $faker->email,
      'address' => $faker->address,
      'telef1' => $faker->phoneNumber,
      'tax_id' => $faker->numberBetween(1,5)
    ];
});

$factory->define(sisVentas\Orden::class, function (Faker\Generator $faker) {
    return [
              'vendor_id' =>$faker->numberBetween(1,5),
              'ship_to_location_id' =>$faker->numberBetween(1,5),
              'bill_to_location_id' =>$faker->numberBetween(1,5),
              'type_lookup_code' =>$faker->numberBetween(1,5),
              'currency_code' =>$faker->numberBetween(1,5),
              'agent_id' =>$faker->numberBetween(1,5),
    ];
});

$factory->define(sisVentas\Direccion::class, function (Faker\Generator $faker) {
    return [
        'location_code' => $faker->cityPrefix,
        'adress_line_1' => $faker->streetAddress,
        'country' => $faker->country,
        'description' => $faker->word,
    ];
});

$factory->define(sisVentas\Buyer::class, function (Faker\Generator $faker) {
    return [
      'person_id' => $faker->numberBetween(1,10),
      'location_id' => $faker->numberBetween(1,5),
      'idcategoria' => $faker->numberBetween(1,5),
      'authorization_limit' => $faker->numberBetween(1,5),
      'enabled' => $faker->numberBetween(0,1),
      'last_updated_by' => $faker->numberBetween(1,5),
      'created_by' => $faker->numberBetween(1,5),
    ];
});

$factory->define(sisVentas\FndLookup::class, function (Faker\Generator $faker) {
    return [
      'lookup_type' => $faker->numberBetween(1, 5),
      'customization_level' => $faker->numberBetween(1, 5),
      'description' => $faker->word,
      'active' => $faker->numberBetween(0, 1),
      'created_by' => $faker->numberBetween(1, 5),
      'last_updated_by' => $faker->numberBetween(1, 5),
    ];
});

$factory->define(sisVentas\FndLookupValue::class, function (Faker\Generator $faker) {
    return [
      'idlookup' => $faker->numberBetween(1,5),
      'code_value' => $faker->numberBetween(1,5),
      'description' => $faker->word,
      'date_from' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
      'date_to' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
      'active' => $faker->numberBetween(0,1),
      'created_by' => $faker->numberBetween(1,5),
      'last_updated_by' => $faker->numberBetween(1,5),
    ];
});

$factory->define(sisVentas\Personal::class, function (Faker\Generator $faker) {
    return [
      'FIRST_NAME' => $faker->firstName,
      'SECOND_NAME' => $faker->firstName,
      'first_LAST_NAME' => $faker->lastName,
      'SECOND_LAST_NAME' => $faker->lastName,
      'FULL_NAME' => $faker->name,
      'DATE_OF_BIRTH' => date_format($faker->dateTimeThisCentury(),"Y-m-d H:i:s"),
      'SEX' => $faker->randomElement(['M', 'F']),
      'EFFECTIVE_START_DATE' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
      'EFFECTIVE_END_DATE' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
      'EMAIL_ADDRESS' => $faker->email,
      'TELEF1' => $faker->phoneNumber,
      'TELEF2' => $faker->phoneNumber,
      'ADDRESS' => $faker->address,      
    ];
});

$factory->define(sisVentas\TerminoPagoCP::class, function (Faker\Generator $faker) {
    return [
      'name' => $faker->word
    ];
});

$factory->define(sisVentas\DailyRate::class, function (Faker\Generator $faker) {
    return [
      'from_currency' => $faker->currencyCode,
      'to_currency' => $faker->currencyCode,
      'conversion_date' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
      'conversion_rate' => $faker->numberBetween(3500, 4000),
    ];
});

$factory->define(sisVentas\Pos::class, function (Faker\Generator $faker) {
    return [
      'client_id' => $faker->numberBetween(1, 10),
      'type_lookup_code' => $faker->word,
      'site_id' => $faker->numberBetween(1,5),
      'pos_op_amt_id' => $faker->numberBetween(1,5),
      'created_by' => $faker->numberBetween(1, 10),
      'last_updated_by' => $faker->numberBetween(1, 10),
    ];
});

$factory->define(sisVentas\PosDetail::class, function (Faker\Generator $faker) {
    return [
      'inv_item_id' => $faker->numberBetween(1,10),
      'price' => $faker->numberBetween(100,500),
      'quantity' => $faker->numberBetween(1,10),
      'tax_rate' => $faker->randomFloat(2, $min = 0, $max = 2),
      'tax_id' => $faker->numberBetween(1,10),
      'pos_header_id' => $faker->numberBetween(1,30),
    ];
});

$factory->define(sisVentas\PosPay::class, function (Faker\Generator $faker) {
    return [
      'type_lookup_code' => $faker->randomElement(['efe', 'tdc', 'tdd', 'ndb', 'che','tran']),
      'amount' => $faker->numberBetween(50, 500),
      'reference' => $faker->word,
      'pos_header_id' => $faker->numberBetween(1,30),
    ];
});

$factory->define(sisVentas\ItemSite::class, function (Faker\Generator $faker) {
    return [
      'inv_item_id' => $faker->numberBetween(1,200),
      'site_id' => $faker->numberBetween(1,5),
      'sku' => $faker->ean13,
      'list_price_per_unit' => $faker->numberBetween(100,500),
    ];
});

$factory->define(sisVentas\Caja::class, function (Faker\Generator $faker) {
    return [
      'site_id' => $faker->numberBetween(1,5),
      'idcashier' => $faker->numberBetween(1,5),
      'start_date' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
      'end_date' => null,
      'start_amount' => $faker->numberBetween(50,200),
      'rate' => $faker->numberBetween(1,5),
      'barcode_reader' => $faker->boolean,
    ];
});

$factory->define(sisVentas\Cajero::class, function (Faker\Generator $faker) {
    return [
      'user_id' => $faker->numberBetween(1,5),
    ];
});

$factory->define(sisVentas\DisccountHeader::class, function (Faker\Generator $faker) {
    return [
      'name' => $faker->name,
      'apply_autom_flag' => $faker->numberBetween(0,1),
      'rate_disccount' => $faker->randomFloat(2, 8, 20),
      'quantity_disccount' => $faker->numberBetween(1,5),
      'start_date' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
      'status' => $faker->numberBetween(0,1),
      'end_date' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
    ];
});

$factory->define(sisVentas\DisccountDetail::class, function (Faker\Generator $faker) {
    return [
      'dis_header_id' => $faker->numberBetween(1,5),
      'site_id' => $faker->numberBetween(1,5),
      'item_id' => $faker->numberBetween(1,200),
      'start_date' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
      'end_data' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
      'rate_disccount' => $faker->randomFloat(2, 8, 20) / 100,
      'quantity_disccount'       => $faker->numberBetween(1,5),
    ];
});

$factory->define(sisVentas\InvOnhandQty::class, function (Faker\Generator $faker) {
    return [
      'item_id' => $faker->numberBetween(1,5),
      'organization_id' => $faker->numberBetween(1,5),
      'site_id' => $faker->numberBetween(1,5),
      'date_received' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
      'primary_transaction_quantity' => $faker->numberBetween(1,5),
      'subinventory_id' => $faker->numberBetween(1,5),
      'locator_id' => $faker->numberBetween(1,5),
      'create_transaction_id' => $faker->numberBetween(1,5),
      'orig_date_received' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
      'transaction_uom_code' => $faker->word,
      'transaction_quantity' => $faker->numberBetween(1,5),
      'is_consigned' => $faker->numberBetween(0,1),
      'last_updated_by' => $faker->numberBetween(1,5),
      'created_by' => $faker->numberBetween(1,5),
    ];
});

$factory->define(sisVentas\Promotion::class, function (Faker\Generator $faker) {
    return [
      'name' => $faker->word,
      'apply_autom_flag' => $faker->numberBetween(0,1),
      'start_date' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
      'status' => $faker->numberBetween(1,5),
      'end_date' => date_format($faker->dateTimeThisMonth(),"Y-m-d H:i:s"),
      'created_by' => $faker->numberBetween(1,5),
      'last_updated_by' => $faker->numberBetween(1,5),

    ];
});

$factory->define(sisVentas\PromotionDetail::class, function (Faker\Generator $faker) {
    return [
  'header_id' => $faker->numberBetween(1,5),
  'site_id' => $faker->numberBetween(1,5),
  'item_id' => $faker->numberBetween(1,5),
  'start_date' => date_format($faker->dateTimeThisMonth(),"Y-m-d"),
  'end_data' => date_format($faker->dateTimeThisMonth(),"Y-m-d"),
  'quantity_start' => $faker->numberBetween(1,5),
  'quantity_end' => $faker->numberBetween(1,5),
  'created_by' => $faker->numberBetween(1,5),
  'last_updated_by' => $faker->numberBetween(1,5),
    ];
});
