<?php

use Illuminate\Database\Seeder;
use sisVentas\Direccion;

class DireccionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Direccion::class, 10)->create();
    }
}
