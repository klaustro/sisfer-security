<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(SiteSeeder::class);
        $this->call(EntitySeeder::class);
        $this->call(ImpuestoSeeder::class);
        $this->call(UomSeeder::class);
        $this->call(CurrencySeeder::class);
        $this->call(SalesPersonSeeder::class);
        $this->call(TerminoPagoSeeder::class);
        $this->call(CategoriaSeeder::class);
        $this->call(ItemSeeder::class);
        $this->call(ClienteSeeder::class);        
        $this->call(FacturaDetalleSeeder::class);       
        $this->call(ProveedorSeeder::class);
        $this->call(DireccionSeeder::class);
        $this->call(BuyerSeeder::class);
        $this->call(FndLookupSeeder::class);
        $this->call(FndLookupValueSeeder::class);
        $this->call(PersonalSeeder::class);
        $this->call(TerminoPagoCPSeeder::class);
        $this->call(TipoCambioSeeder::class);
        $this->call(ItemSiteSeeder::class);
        $this->call(UserSiteSeeder::class);
        $this->call(CajaSeeder::class);
        $this->call(CajeroSeeder::class);
/*        $this->call(PosSeeder::class);
        $this->call(PosDetailSeeder::class);
        $this->call(PosPaySeeder::class);*/
        $this->call(DisccountHeaderSeeder::class);
        $this->call(DisccountDetailSeeder::class);
        $this->call(InvOnhandQtySeeder::class);
        $this->call(PromotionSeeder::class);
        $this->call(PromotionDetailSeeder::class);
        
    }
}
