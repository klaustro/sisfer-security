<?php

use Illuminate\Database\Seeder;
use sisVentas\ItemSite;

class ItemSiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 50 ; $i++) { 
            factory(ItemSite::class)->create([
                'site_id' => 1,
                'inv_item_id' => $i+1,
            ]);            
        }
    }
}
