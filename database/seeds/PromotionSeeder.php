<?php

use Illuminate\Database\Seeder;
use sisVentas\Promotion;

class PromotionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Promotion::class)->create();
    }
}
