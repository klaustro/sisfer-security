<?php

use Illuminate\Database\Seeder;
use sisVentas\Entity;

class EntitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	factory(Entity::class, 1)->create();
    }
}
