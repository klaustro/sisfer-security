<?php

use Illuminate\Database\Seeder;
use sisVentas\Cajero;

class CajeroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 10 ; $i++) { 
            factory(Cajero::class)->create([
                'user_id' => $i + 1,
            ]);
        }
    }
}
