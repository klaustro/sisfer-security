<?php

use Illuminate\Database\Seeder;
use sisVentas\TerminoPagoAR;

class TerminoPagoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(TerminoPagoAR::class, 5)->create();
    }
}
