<?php

use Illuminate\Database\Seeder;
use sisVentas\Item;

class ItemSeeder extends Seeder
{

    public function run()
    {
        factory(Item::class)->create([
            'nombre' => 'Servicios',
            'descripcion' => 'Servicios',    	
            'service_item_flag'	 => 1,
    	]);
    	
        factory(Item::class, 50)->create();
    }
}
