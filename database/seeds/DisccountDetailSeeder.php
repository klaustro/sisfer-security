<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use sisVentas\DisccountDetail;

class DisccountDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($i=0; $i < 50 ; $i++) { 
            factory(DisccountDetail::class)->create([
                'item_id' => $i+1,
                'site_id' => '1',
                'start_date' => date_format(Carbon::now()->subDay(),"Y-m-d"),
                'end_data' => date_format(Carbon::now()->addDay(),"Y-m-d"),            
            ]);
        }        
    }
}
