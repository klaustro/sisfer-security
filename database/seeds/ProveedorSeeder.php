<?php

use Illuminate\Database\Seeder;
use sisVentas\Proveedor;

class ProveedorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		factory(Proveedor::class, 10)->create();
    }
}
