<?php

use Illuminate\Database\Seeder;
use sisVentas\TerminoPagoCP;

class TerminoPagoCPSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(TerminoPagoCP::class)->create(['name' => 'T40', 'description' => '40 dias']);
        factory(TerminoPagoCP::class)->create(['name' => 'T60', 'description' => '60 dias']);
    }
}
