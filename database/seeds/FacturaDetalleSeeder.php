<?php

use Illuminate\Database\Seeder;
use sisVentas\FacturaDetalle;

class FacturaDetalleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(FacturaDetalle::class, 100)->create();
    }
}
