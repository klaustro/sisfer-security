<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use sisVentas\Caja;

class CajaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Caja::class)->create([
            'site_id' => 1,
        	'start_date' => Carbon::now(),
            'idcashier' => 1,
        ]);

        factory(Caja::class, 4)->create();
    }
}
