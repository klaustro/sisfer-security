<?php

use Illuminate\Database\Seeder;
use sisVentas\PromotionDetail;

class PromotionDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 10; $i++) { 
            factory(PromotionDetail::class)->create([
                'item_id' => $i+1,
                'start_date' => '2017-01-01',
                'end_data' => '2017-12-31',
            ]);
        }
    }
}
