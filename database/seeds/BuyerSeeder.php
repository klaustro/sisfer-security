<?php

use Illuminate\Database\Seeder;
use sisVentas\Buyer;
use sisVentas\User;


class BuyerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		foreach (User::orderBy('id')->get() as $user) {
			factory(Buyer::class)->create([
				'person_id'=> $user->id,
			]);
		}
    }
}
