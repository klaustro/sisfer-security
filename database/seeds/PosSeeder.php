<?php

use Illuminate\Database\Seeder;
use sisVentas\Pos;

class PosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Pos::class, 30)->create();
    }
}
