<?php

use Illuminate\Database\Seeder;
use sisVentas\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'name' => 'Admin',
            'email' => 'admin@123.com',
            'password' => bcrypt('secret'),
        ]);        

        factory(User::class, 9)->create();
    }
}
