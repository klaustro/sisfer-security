<?php

use Illuminate\Database\Seeder;
use sisVentas\Uom;

class UomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		factory(Uom::class, 5)->create();
    }
}
