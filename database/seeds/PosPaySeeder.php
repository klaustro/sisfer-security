<?php

use Illuminate\Database\Seeder;
use sisVentas\PosPay;

class PosPaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(PosPay::class, 100)->create();
    }
}
