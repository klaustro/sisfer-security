<?php

use Illuminate\Database\Seeder;
use sisVentas\FndLookupValue;

class FndLookupValueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
				factory(FndLookupValue::class)->create(['idlookup' => 1, 'code_value' =>  'e',  'description' =>  'e']);
				factory(FndLookupValue::class)->create(['idlookup' => 2, 'code_value' =>  'DNI',  'description' =>  'documento']);
				factory(FndLookupValue::class)->create(['idlookup' => 2, 'code_value' =>  'LE',  'description' =>  'LIBRETA']);
				factory(FndLookupValue::class)->create(['idlookup' => 2, 'code_value' =>  'CE',  'description' =>  'carnet']);
				factory(FndLookupValue::class)->create(['idlookup' => 4, 'code_value' =>  'y',  'description' =>  'yy']);
				factory(FndLookupValue::class)->create(['idlookup' => 5, 'code_value' =>  'BO',  'description' =>  'BOLETA']);
				factory(FndLookupValue::class)->create(['idlookup' => 6, 'code_value' =>  'A',  'description' =>  'FF']);
				factory(FndLookupValue::class)->create(['idlookup' => 7, 'code_value' =>  'A',  'description' =>  'B']);
				factory(FndLookupValue::class)->create(['idlookup' => 8, 'code_value' =>  'ccd',  'description' =>  'ccd']);
				factory(FndLookupValue::class)->create(['idlookup' => 8, 'code_value' =>  'ccf',  'description' =>  'ccf']);
				factory(FndLookupValue::class)->create(['idlookup' => 8, 'code_value' =>  'ccd33',  'description' =>  'vffr']);
				factory(FndLookupValue::class)->create(['idlookup' => 8, 'code_value' =>  'ffrr',  'description' =>  'ffrrr']);
				factory(FndLookupValue::class)->create(['idlookup' => 8, 'code_value' =>  'ggtt',  'description' =>  'ghgh444']);
				factory(FndLookupValue::class)->create(['idlookup' => 9, 'code_value' =>  'OC',  'description' =>  'usado']);
				factory(FndLookupValue::class)->create(['idlookup' => 9, 'code_value' =>  'Bol',  'description' =>  'Boleta']);
				factory(FndLookupValue::class)->create(['idlookup' => 10, 'code_value' =>  'P',  'description' =>  'Labor']);
				factory(FndLookupValue::class)->create(['idlookup' => 10, 'code_value' =>  'T',  'description' =>  'Labor']);
				factory(FndLookupValue::class)->create(['idlookup' => 11, 'code_value' =>  'MOVILIDAD',  'description' =>  'gastos']);
				factory(FndLookupValue::class)->create(['idlookup' => 11, 'code_value' =>  'RESTAURANT',  'description' =>  'comida']);
				factory(FndLookupValue::class)->create(['idlookup' => 11, 'code_value' =>  'REFRIGERIO',  'description' =>  'gastos']);
				factory(FndLookupValue::class)->create(['idlookup' => 5, 'code_value' =>  'FA',  'description' =>  'FACTURA']);
				factory(FndLookupValue::class)->create(['idlookup' => 5, 'code_value' =>  'RE',  'description' =>  'RECIBO']);
				factory(FndLookupValue::class)->create(['idlookup' => 12, 'code_value' =>  'Mecanico',  'description' =>  'Mecanico']);
				factory(FndLookupValue::class)->create(['idlookup' => 12, 'code_value' =>  'Sistema',  'description' =>  'Sistema']);
				factory(FndLookupValue::class)->create(['idlookup' => 13, 'code_value' =>  'Alto',  'description' =>  'Alto']);
				factory(FndLookupValue::class)->create(['idlookup' => 14, 'code_value' =>  'Rutina',  'description' =>  'Rutina']);
				factory(FndLookupValue::class)->create(['idlookup' => 14, 'code_value' =>  'Preventiva',  'description' =>  'Preventiva']);
				factory(FndLookupValue::class)->create(['idlookup' => 14, 'code_value' =>  'Reconstrucción',  'description' =>  'Reconstrucción']);
				factory(FndLookupValue::class)->create(['idlookup' => 14, 'code_value' =>  'Emergencia',  'description' =>  'Emergencia']);
				factory(FndLookupValue::class)->create(['idlookup' => 14, 'code_value' =>  'Instalaciones',  'description' =>  'Instalaciones']);
				factory(FndLookupValue::class)->create(['idlookup' => 15, 'code_value' =>  'Inspeccion',  'description' =>  'Inspeccion']);
				factory(FndLookupValue::class)->create(['idlookup' => 15, 'code_value' =>  'Revision',  'description' =>  'Revision']);
				factory(FndLookupValue::class)->create(['idlookup' => 15, 'code_value' =>  'Lubricacion',  'description' =>  'Lubricacion']);
				factory(FndLookupValue::class)->create(['idlookup' => 15, 'code_value' =>  'Reparacion',  'description' =>  'Reparacion']);
				factory(FndLookupValue::class)->create(['idlookup' => 15, 'code_value' =>  'Mantenimiento',  'description' =>  'Mantenimiento']);
				factory(FndLookupValue::class)->create(['idlookup' => 15, 'code_value' =>  'Limpieza',  'description' =>  'Limpieza']);
				factory(FndLookupValue::class)->create(['idlookup' => 16, 'code_value' =>  'Desglose',  'description' =>  'Desglose']);
				factory(FndLookupValue::class)->create(['idlookup' => 16, 'code_value' =>  'Vandalismo',  'description' =>  'Vandalismo']);
				factory(FndLookupValue::class)->create(['idlookup' => 16, 'code_value' =>  'Desgaste',  'description' =>  'Desgaste']);
				factory(FndLookupValue::class)->create(['idlookup' => 16, 'code_value' =>  'Ajustes',  'description' =>  'Ajustes']);
				factory(FndLookupValue::class)->create(['idlookup' => 17, 'code_value' =>  'Regulador',  'description' =>  'Regulador']);
				factory(FndLookupValue::class)->create(['idlookup' => 17, 'code_value' =>  'Cumplimiento',  'description' =>  'Cumplimiento']);
				factory(FndLookupValue::class)->create(['idlookup' => 17, 'code_value' =>  'Garantia',  'description' =>  'Garantia']);
				factory(FndLookupValue::class)->create(['idlookup' => 17, 'code_value' =>  'Accidente',  'description' =>  'Accidente']);
				factory(FndLookupValue::class)->create(['idlookup' => 18, 'code_value' =>  'Standard',  'description' =>  'Orden Standard']);
				factory(FndLookupValue::class)->create(['idlookup' => 18, 'code_value' =>  'Blanket',  'description' =>  'Compra Abierta']);
				factory(FndLookupValue::class)->create(['idlookup' => 19, 'code_value' =>  'flete1',  'description' =>  'Flete 1']);
				factory(FndLookupValue::class)->create(['idlookup' => 19, 'code_value' =>  'flete2',  'description' =>  'Flete 2']);
				factory(FndLookupValue::class)->create(['idlookup' => 20, 'code_value' =>  'ship1',  'description' =>  'Ship 1']);
				factory(FndLookupValue::class)->create(['idlookup' => 20, 'code_value' =>  'ship2',  'description' =>  'Ship 2']);
				factory(FndLookupValue::class)->create(['idlookup' => 21, 'code_value' =>  'fob1',  'description' =>  'Fob 1']);
				factory(FndLookupValue::class)->create(['idlookup' => 21, 'code_value' =>  'fob2',  'description' =>  'Fob 2']);
				factory(FndLookupValue::class)->create(['idlookup' => 22, 'code_value' =>  'boleta',  'description' =>  'Boleta Manual']);				
				factory(FndLookupValue::class)->create(['idlookup' => 22, 'code_value' =>  'factura',  'description' =>  'Factura de venta']);	
				factory(FndLookupValue::class)->create(['idlookup' => 23, 'code_value' =>  'che',  'description' =>  'Cheque']);	
				factory(FndLookupValue::class)->create(['idlookup' => 23, 'code_value' =>  'efe',  'description' =>  'Efectivo']);	
				factory(FndLookupValue::class)->create(['idlookup' => 23, 'code_value' =>  'ndb',  'description' =>  'Nota de débito']);	
				factory(FndLookupValue::class)->create(['idlookup' => 23, 'code_value' =>  'tdc',  'description' =>  'Tarjeta de crédito']);	
				factory(FndLookupValue::class)->create(['idlookup' => 23, 'code_value' =>  'tdd',  'description' =>  'Tarjeta de Débito']);	
				factory(FndLookupValue::class)->create(['idlookup' => 23, 'code_value' =>  'tran',  'description' =>  'Transferencia']);	
    }
}
