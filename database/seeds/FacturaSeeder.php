<?php

use Illuminate\Database\Seeder;
use sisVentas\Factura;

class FacturaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Factura::class, 30)->create();
    }
}
