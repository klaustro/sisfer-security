<?php

use Illuminate\Database\Seeder;
use sisVentas\SalesPerson;

class SalesPersonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(SalesPerson::class, 30)->create();
    }
}
