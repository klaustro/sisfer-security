<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use sisVentas\DailyRate;

class TipoCambioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      for ($i=0; $i < 30; $i++) { 
        factory(DailyRate::class)->create([
          'from_currency' => 'USD',
          'to_currency' => 'PEN',
          'conversion_date' => date_format(Carbon::now()->addDays($i),"Y-m-d H:i:s"),          
        ]);

        factory(DailyRate::class)->create([
          'from_currency' => 'EUR',
          'to_currency' => 'PEN',
          'conversion_date' => date_format(Carbon::now()->addDays($i),"Y-m-d H:i:s"),          
        ]);        
      }
    }
}
