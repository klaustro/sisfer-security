<?php

use Illuminate\Database\Seeder;
use sisVentas\FndLookup;

class FndLookupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

			factory(FndLookup::class)->create(['lookup_type' => 'c', 'customization_level' => 'E', 'description' => 'ccc',]);

			factory(FndLookup::class)->create(['lookup_type' => 'TIPO_DOC', 'customization_level' => 'S', 'description' => 'documento',]);

			factory(FndLookup::class)->create(['lookup_type' => 'rrr', 'customization_level' => 'E', 'description' => 'ggggg',]);
			factory(FndLookup::class)->create(['lookup_type' => 'rttt', 'customization_level' => 'E', 'description' => 'ttt',]);
			factory(FndLookup::class)->create(['lookup_type' => 'TIPO_DOC_VENTA', 'customization_level' => 'E', 'description' => 'documento venta',]);
			factory(FndLookup::class)->create(['lookup_type' => 'TIPO_DOC', 'customization_level' => 'E', 'description' => 'CCCC',]);
			factory(FndLookup::class)->create(['lookup_type' => 'TIPO_DOC', 'customization_level' => 'E', 'description' => 'DFFFFF',]);
			factory(FndLookup::class)->create(['lookup_type' => 'DFFFFF33', 'customization_level' => 'E', 'description' => 'DFFFFF33',]);
			factory(FndLookup::class)->create(['lookup_type' => 'Tipo_doc_Ingreso', 'customization_level' => 'E', 'description' => 'usado para documentos de ingreso',]);
			factory(FndLookup::class)->create(['lookup_type' => 'TIPO_LABOR', 'customization_level' => 'S', 'description' => 'Tipo de Labor por AJM']);
			factory(FndLookup::class)->create(['lookup_type' => 'GASTOS_VARIOS', 'customization_level' => 'S', 'description' => 'GASTOS_VARIOS']);
			factory(FndLookup::class)->create(['lookup_type' => 'FALLA_OT', 'customization_level' => 'E', 'description' => 'Falla en Orden de Trabajo',]); factory(FndLookup::class)->create(['lookup_type' => 'PRIORIDAD_OT', 'customization_level' => 'E', 'description' => 'Prioridad en OT']);
			factory(FndLookup::class)->create(['lookup_type' => 'TIPO_OT', 'customization_level' => 'S', 'description' => 'Tipo de Orden de Trabajo']);
			factory(FndLookup::class)->create(['lookup_type' => 'Activity Type', 'customization_level' => 'E', 'description' => 'Tipo de Actividad de OT']);
			factory(FndLookup::class)->create(['lookup_type' => 'Activity Cause', 'customization_level' => 'E', 'description' => 'Causa de Actividad',]);
			factory(FndLookup::class)->create(['lookup_type' => 'Activity Source', 'customization_level' => 'E', 'description' => 'Origen de Actividad',]);
			factory(FndLookup::class)->create(['lookup_type' => 'TIPO_OC', 'customization_level' => 'E', 'description' => 'Tipo de Orden de Compra',]);
			factory(FndLookup::class)->create(['lookup_type' => 'FREIGHT_TERMS', 'customization_level' => 'E', 'description' => 'Flete de Orden de Compra',]);
			factory(FndLookup::class)->create(['lookup_type' => 'SHIP_VIA', 'customization_level' => 'E', 'description' => 'Transporte de Orden de Compra',]);
			factory(FndLookup::class)->create(['lookup_type' => 'FOB', 'customization_level' => 'E', 'description' => 'Fob de Orden de Compra',]);
            factory(FndLookup::class)->create(['lookup_type' => 'POS', 'customization_level' => 'E', 'description' => 'Tipo de venta',]);
            factory(FndLookup::class)->create(['lookup_type' => 'TIPO_PAGO', 'customization_level' => 'E', 'description' => 'Medios de pago',]);              
                  

		}

}
