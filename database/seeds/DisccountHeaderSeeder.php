<?php

use Illuminate\Database\Seeder;
use sisVentas\DisccountHeader;

class DisccountHeaderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(DisccountHeader::class, 5)->create();
    }
}
