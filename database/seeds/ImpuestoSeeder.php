<?php

use Illuminate\Database\Seeder;

use sisVentas\Impuesto;

class ImpuestoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			factory(Impuesto::class)->create(['name' => 'IR']);
			factory(Impuesto::class)->create(['name' => 'ITAN']);
			factory(Impuesto::class)->create(['name' => 'IGV']);
			factory(Impuesto::class)->create(['name' => 'RUS']);
			factory(Impuesto::class)->create(['name' => 'ITF']);
    }
}
