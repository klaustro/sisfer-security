<?php

use Illuminate\Database\Seeder;
use sisVentas\UserSite;

class UserSiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(UserSite::class, 3)->create([
            'IDUSUARIO' => 1,
            'FEC_INI' => '2017-01-01',
            'FEC_FIN' => '2030-12-31',
        ]);
    }
}
