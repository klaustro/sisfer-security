<?php

use Illuminate\Database\Seeder;
use sisVentas\Personal;

class PersonalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Personal::class, 20)->create();
    }
}
