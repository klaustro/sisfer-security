<?php

use Illuminate\Database\Seeder;
use sisVentas\ReciboDetalle;

class ReciboDetalleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(ReciboDetalle::class, 50)->create();
    }
}
