<?php

use Illuminate\Database\Seeder;
use sisVentas\Categoria;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Categoria::class)->create(['nombre' =>'TAMBORES DE FRENO', 'descripcion' => 'TAMBORES DE FRENO']);
				factory(Categoria::class)->create(['nombre' =>'FAJA LISA', 'descripcion' => 'FAJA LISA']);
				factory(Categoria::class)->create(['nombre' =>'FAJA ACANALADA', 'descripcion' => 'FAJA ACANALADA',]);
				factory(Categoria::class)->create(['nombre' =>'SENSOR', 'descripcion' => 'SENSOR']);
				factory(Categoria::class)->create(['nombre' =>'FILTRO DE ACEITE', 'descripcion' => 'FILTRO DE ACEITE',]);
				factory(Categoria::class)->create(['nombre' =>'LIQUIDO REFRIGERANTE', 'descripcion' => 'LIQUIDO REFRIGERANTE',]);
				factory(Categoria::class)->create(['nombre' =>'FILTRO DE COMBUSTIBLE', 'descripcion' => 'FILTRO DE COMBUSTIBLE']);
				factory(Categoria::class)->create(['nombre' =>'PASTILLA DE FRENO', 'descripcion' => 'PASTILLA DE FRENO']);
				factory(Categoria::class)->create(['nombre' =>'FOCO', 'descripcion' => 'FOCO']);
				factory(Categoria::class)->create(['nombre' =>'ACEITE LUBRICANTE', 'descripcion' => 'ACEITE LUBRICANTE',]);
				factory(Categoria::class)->create(['nombre' =>'BUJIA', 'descripcion' => 'BUJIA']);
				factory(Categoria::class)->create(['nombre' =>'LIQUIDO DE FRENO', 'descripcion' => 'LIQUIDO DE FRENO',]);
				factory(Categoria::class)->create(['nombre' =>'SOQUETE', 'descripcion' => 'SOQUETE',]);
				factory(Categoria::class)->create(['nombre' =>'BATERIA', 'descripcion' => 'BATERIA']);
				factory(Categoria::class)->create(['nombre' =>'BOMBA DE GASOLINA', 'descripcion' => 'BOMBA DE GASOLINA']);
				factory(Categoria::class)->create(['nombre' =>'FILTRO DE AIRE', 'descripcion' => 'FILTRO DE AIRE']);
				factory(Categoria::class)->create(['nombre' =>'FAJA DE DISTRIBUCION', 'descripcion' => 'FAJA DE DISTRIBUCION',]);
				factory(Categoria::class)->create(['nombre' =>'DISCO DE EMBRAGUE', 'descripcion' => 'DISCO DE EMBRAGUE']);
				factory(Categoria::class)->create(['nombre' =>'ZAPATA DE FRENO', 'descripcion' => 'ZAPATA DE FRENO']);
				factory(Categoria::class)->create(['nombre' =>'MATERIAL', 'descripcion' => 'MATERIAL']);
				factory(Categoria::class)->create(['nombre' =>'AMORTIGUADOR', 'descripcion' => 'AMORTIGUADOR']);
				factory(Categoria::class)->create(['nombre' =>'PLUMILLA', 'descripcion' => 'PLUMILLA']);
				factory(Categoria::class)->create(['nombre' =>'DISCO DE FRENO', 'descripcion' => 'DISCO DE FRENO']);
				factory(Categoria::class)->create(['nombre' =>'FILTRO DE AIRE ACONDICION', 'descripcion' => 'FILTRO DE AIRE ACONDICION',]);
				factory(Categoria::class)->create(['nombre' =>'DISCO DE EMBRAGUE', 'descripcion' => 'DISCO DE EMBRAGUE']);
				factory(Categoria::class)->create(['nombre' =>'COLLARIN', 'descripcion' => 'COLLARIN']);
				factory(Categoria::class)->create(['nombre' =>'PLATO DE EMBRAGUE', 'descripcion' => 'PLATO DE EMBRAGUE']);
				factory(Categoria::class)->create(['nombre' =>'RODAJE', 'descripcion' => 'RODAJE']);
				factory(Categoria::class)->create(['nombre' =>'RETEN', 'descripcion' => 'RETEN']);
				factory(Categoria::class)->create(['nombre' =>'PERNO', 'descripcion' => 'PERNO']);
				factory(Categoria::class)->create(['nombre' =>'SEGURO DE RUEDA', 'descripcion' => 'SEGURO DE RUEDA']);
    }
}
