<?php

use Illuminate\Database\Seeder;
use sisVentas\PosDetail;

class PosDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(PosDetail::class, 100)->create();
    }
}
