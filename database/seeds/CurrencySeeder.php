<?php

use Illuminate\Database\Seeder;
use sisVentas\Moneda;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Moneda::class)->create([
        	'currency_code' => 'PEN',
        	'descripcion' => 'Soles'
        ]);

        factory(Moneda::class)->create([
        	'currency_code' => 'USD',
        	'descripcion' => 'Dolares Americanos'
        ]);    

        factory(Moneda::class)->create([
        	'currency_code' => 'EUR',
        	'descripcion' => 'Euros'
        ]);
    }
}
