<?php

use Illuminate\Database\Seeder;
use sisVentas\InvOnhandQty;

class InvOnhandQtySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 50 ; $i++) { 
            factory(InvOnhandQty::class)->create([
                'item_id' => $i+1,
                'site_id' => 1,
                'primary_transaction_quantity' => 100
            ]);            
        }        
    }
}
