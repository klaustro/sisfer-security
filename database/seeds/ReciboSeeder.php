<?php

use Illuminate\Database\Seeder;
use sisVentas\Recibo;

class ReciboSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Recibo::class, 20)->create();
    }
}
