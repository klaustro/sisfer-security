<?php

use Illuminate\Database\Seeder;
use sisVentas\Site;

class SiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Site::class, 5)->create();
    }
}
