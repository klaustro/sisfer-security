<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class Orden extends Model
{
      protected $table = 'po_headers_all';

      protected $primaryKey='po_header_id';

      protected $fillable = [
            
            'agent_id',
            'type_lookup_code',
            'contact',
            'segment1',
            'enabled_flag',
            'start_date_active',
            'end_date_active',
            'vendor_id',
            'ship_to_location_id',
            'bill_to_location_id',
            'terms_id',
            'ship_via_lookup_code',
            'fob_lookup_code',
            'freight_terms_lookup_code',
            'status_lookup_code',
            'currency_code',
            'rate_type',
            'rate_date',
            'rate',
            'blanket_total_amount',
            'authorization_status',
            'approved_flag',
            'approved_date',
            'amount_limit',
            'min_release_amount',
            'note_to_authorizer',
            'note_to_vendor',
            'note_to_receiver',
            'comments',
            'closed_code',
            'closed_date',
            'cancel_flag',
            'consigned_consumption_flag',
            'org_id',
            'last_updated_by',
            'created_by'		
          ];
		
		public $timestamps= true;
		
      //Relationship
        public function proveedor()
{
    return $this->belongsTo(Proveedor::class, 'vendor_id');
}

      public function clientToBill()
      {
            return $this->belongsTo('\sisVentas\Proveedor', 'vendor_id', 'vendor_id');
      }

      public function billCurrency()
      {
            return $this->belongsTo('\sisVentas\Moneda', 'currency_code', 'currency_code');        
      }

      public function detail()
      {
        return $this->hasMany(OrdenDetalle::class, 'po_header_id', 'po_header_id');
      }

    /* public function receipts()
      {
        return $this->hasMany(ReciboDetalle::class, 'customer_trx_id', 'customer_trx_id');
      }*/

      
      //Attributes

      public function getAmountAttribute()      
      {
        $total = 0;
      
        foreach ($this->detail()->get()->toArray() as $detail) {
        $total += $detail['quantity_ordered'] * $detail['unit_selling_price'];
        }
      
        return round($total + $this->tax, 2); 
      }

      public function getTaxAttribute()      
      {
        $total = 0;
      
        foreach ($this->detail()->with('tax')->get()->toArray() as $detail) {
          $total += $detail['quantity_ordered'] * $detail['unit_selling_price'] * ($detail['tax']['tax_rate']/100);
        }
      
        return $total;
      }      

      public function getReceiptsAppliedAttribute()
      {

            return $this->receipts->sum('amount_applied') + $this->receipts->sum('discount_original');
      }      


      public function getBalanceAttribute()
      {
          $saldo = $this->amount - $this->receiptsApplied;
            return ($saldo < 0.01 && $saldo > -0.1) ? 0 : $saldo;
      }      

      public function getIdAttribute()
      {
            return $this->customer_trx_id;
      }

      public function getBillToAttribute()
      {
             return  "{$this->clientToBill->first_name} {$this->clientToBill->first_last_name}";
      }

      public function getCurrencyAttribute()
      {
             return "{$this->clientToBill->first_name} {$this->clientToBill->first_last_name}";
      }    

      public function getIsClosedAttribute()
      {
        return $this->amount > 0 && $this->balance == 0;
      }  

      public function getIsCompletedAttribute()
      {
        return $this->complete_flag == 'Y';
      }  

      //Scopes

      public function scopeId($query,  $start, $end)
      {
          if ($start != '' && $end == '')
              $query->where('trx_number', $start);
      }

      public function scopeIdVendor($query, $id)
      {
          if ($id != "" and is_numeric($id))
              $query->where('vendor_id', $id);
      }      

      public function scopeVendor($query, $name)
      {
          if ($name != "" and !is_numeric($name)){
              $users = Cliente::where('first_name', 'LIKE', "%$name%")
                ->orWhere('first_last_name', 'LIKE', "%$name%")
                ->get();
              //dd($users);

              $query->whereIn('bill_to_customer_id', $users->pluck('idcliente'));
          }
      }

      public function scopeIdRange($query, $start, $end)
      {
        if ($start != '' && $end != '') {
              $query->whereBetween('trx_number', [$start, $end]);
        }
      }

      public function scopeDateRange($query, $start, $end)
      {
        if ($start != '' && $end != '') {
              $query->whereBetween('trx_date', [$start, $end]);
        }
      }     

}
