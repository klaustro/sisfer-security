Vue.component('v-select', VueSelect.VueSelect);     
   // Register a global custom directive called v-focus

Vue.filter('money', function (value) {
    return numeral(value).format('0,0.00');
});

Vue.filter('limitText', function (value) {
    return value.substring(0, 15);
});    

var app = new Vue({
  el: '#app',
  data(){
    return{
        product: '',
        items:[],
        selItems: [],
        customer: '',
        customers:[],
        type: '1',
        types: [
            {
                id: '1',
                name: 'Boleta manual',
            },
            {
                id: '2',
                name: 'Factura de venta',
            },                
        ],
        new_customer:{
            first_name: '',
            first_last_name: '',
            tipo_documento: '',
            num_documento: '',
            sex: '',
            date_of_birth: '',
            address: '',
        },
        pay_type: '',
        pay_reference: '',
        pay_amount: 0,  
        pays: [],
        item_id: '',
        selectArticles: [],
        site_id: '',
        tax_id: '',
        tax_rate: '',
        barcode_reader: false,
        cashOpened: false,
        start_amount: '',            
        rate: '',
        rate_amount: '',
        comment: '',
        rates: [],
        cash_id: '',
        barcode_reader: false,   
        pos_id: '',
    }
  },

  mounted(){
    this.site_id = this.$refs.site_id.value;
    this.tax_id = this.$refs.tax_id.value;
    this.tax_rate = this.$refs.tax_rate.value;  
    this.cash_id = this.$refs.cash_id.value;  
    this.barcode_reader = this.$refs.barcode_reader.value == '1';  
    this.type = 'boleta';
    this.reset();
  },

  watch:{
    product: function(val){
        if (val) {
            this.addItem(val)     
            this.product = null;                          
        }
    },

    rate: function(val){
        this.rate_amount = val == '' ? '' : this.getRate(val).conversion_rate;

    },

    selItems: function(val){
        this.pay_amount = this.toPay;
    }

  },

  methods:{     

    getRates: function(){
        var self = this;
        axios.get('/exchange/rate')
            .then(function(response){
                self.rates = response.data
            })
            .catch(function(error){
                alert(error);
            })
    },

    getItems: function(){
        var self = this;
        this.items = [];
        axios.get('/ventas/posArticles/' + this.site_id)
            .then(function(response){                    
                for (var i = 0; i < response.data.length; i++) {                        
                    var item = response.data[i].item;
                    var max_quantity = parseInt(response.data[i].stock.primary_transaction_quantity);
                    if (max_quantity > 0) {
                        self.items.push({
                            value: item.inv_item_id,
                            label: response.data[i].sku + ' -   ' + item.descripcion,
                            name: item.descripcion,
                            price: response.data[i].list_price_per_unit,
                            tax_rate: +self.tax_rate,
                            tax_id: self.tax_id,
                            disccount:  response.data[i].item_disccount,
                            max_quantity: max_quantity,
                            subinventory_id: response.data[i].stock.subinventory_id,
                            locator_id: response.data[i].stock.locator_id,
                            quantity_start: response.data[i].promotion ? response.data[i].promotion.quantity_start : 0,
                            quantity_end: response.data[i].promotion ? response.data[i].promotion.quantity_end : 0,
                        });                            
                    }
                }
            })
            .catch(function(error){
                alert(error);
            });
    },

    getCustomers: function(){
        var self = this
        axios.get('/ventas/posClientes')
            .then(function(response){
                for (var i = 0; i < response.data.length; i++) {                        
                    self.customers.push({
                        value: response.data[i].idcliente,
                        label: response.data[i].first_name +  ' ' + response.data[i].first_last_name,
                    });                        
                }

            })
            .catch(function(error){
                alert(error)
            })

    },   

    addCustomer: function(){
        this.customers.push({
            value: this.customers.length + 2,
            label: this.new_customer.first_name + ' ' + this.new_customer.first_last_name,
        });
            this.new_customer.first_name = '';
            this.new_customer.first_last_name = '';
            this.new_customer.tipo_documento = '';
            this.new_customer.num_documento = '';
            this.new_customer.sex = '';
            this.new_customer.date_of_birth = '';
            this.new_customer.address = '';

    },


    addItem: function(val){
        var current = this.getSelItem(val.value);
        var quantityItem = this.quantityItem(val.value);
        if (current) {
            if(quantityItem >= current.max_quantity)
            {
                alert('Stock máximo permitido');                
            }
            else{
                this.addPromotion(current);
            }
        }
        else{                
            this.addItemList(val);
        }            

        this.gunFocus();
        this.pay_amount = this.toPay > 0 ? this.toPay : '';
    },

    addItemList: function(val){            
            this.selItems.push({
                line: this.selItems.length + 1,
                inv_item_id: val.value,
                name: val.name,
                quantity: 1,
                price: val.price,
                tax_rate: val.tax_rate,
                tax_id: val.tax_id,                    
                disccount: val.disccount,                    
                max_quantity: val.max_quantity,     
                subinventory_id: val.subinventory_id,
                locator_id: val.locator_id,    
                quantity_start: val.quantity_start,
                quantity_end: val.quantity_end,
            });
    },

    addPromotion: function(current){
         if (current.quantity >= current.quantity_start && current.quantity_start != 0){                
            this.addItemList(this.getItem(current.inv_item_id));
         }
         else{
            current.quantity += 1;   
            if (current.quantity == current.quantity_start) {
                current.price = (current.price / current.quantity_start) * current.quantity_end;
            }                     
         }
    },

    plus: function(index){
        var quantityItem = this.quantityItem(this.selItems[index].inv_item_id);
        console.log(quantityItem + ' >= ' + this.selItems[index].max_quantity)

        if(quantityItem >= this.selItems[index].max_quantity)
        {
            alert('Stock máximo permitido');                
        }
        else{                
            this.addPromotion(this.selItems[index]);
        }
        this.pay_amount = this.toPay;
    },


    subs: function(index){
        this.selItems[index].price = this.getItem(this.selItems[index].inv_item_id).price;
        this.selItems[index].quantity -= this.selItems[index].quantity > 1 ? 1 : 0;
        this.pay_amount = this.toPay;      
    },  
    
    addPay: function(){
        if (this.pay_amount >0) {                
            this.pays.push({
                type_lookup_code: this.pay_type,
                amount: this.pay_amount,
                reference: this.pay_reference,
            });
            this.pay_type = '';
            this.pay_amount = this.toPay > 0 ? this.toPay : '';
            this.pay_reference = '';
        }
    },


    remove: function(index){
        this.selItems.splice(index, 1);
    },   

    getSelItem: function(id){
        var item = '';
        for (var i = 0; i < this.selItems.length; i++) {
            if(this.selItems[i].inv_item_id == id){
                item = this.selItems[i];
            }                
        }
        return item;
    },

    getItem: function(id){
        for (var i = 0; i < this.items.length; i++) {
            if(this.items[i].value == id){
                return this.items[i]
            }                
        }
    },

    quantityItem: function(id){
        var sum = 0;
        for (var i = 0; i < this.selItems.length; i++) {
            if(this.selItems[i].inv_item_id == id){
                sum += this.selItems[i].quantity;
            }                
        }
        return sum;
    },        

    getRate: function(id){
        for (var i = 0; i < this.rates.length; i++) {
            if(this.rates[i].rowid == id){
                return this.rates[i];
            }                
        }            
    },

    removePay: function(index){
        this.pays.splice(index, 1);            
    },


    gunFocus: function(){
        if (this.barcode_reader) {  
            document.getElementsByTagName("input")[5].focus();      
        }
    },

    savePos: function(){
        var self = this;
        axios.post('/ventas/pos', {
                site_id: this.site_id,
                client_id: this.customer.value,
                type_lookup_code: this.type,
                pos_op_amt_id: this.cash_id,
                change: this.change,
                pos_lines: this.selItems,
                pos_pay: this.pays,
            })
            .then(function(response){     
                self.pos_id = response.data.pos_id;               
                alert(response.data.message);
            })
            .catch(function(error){
                alert(error);
            })
    },

    saveCash: function(){
        var self = this;
        axios.post('/ventas/openCash', {        
            start_amount: this.start_amount,
            rate: this.rate,
            comment: this.comment,   
            barcode_reader: this.barcode_reader,             
        })
        .then(function(response){
            self.cashOpened = true;
            self.cash_id = response.data.cash_id;
            alert(response.data.message);
        })
        .catch(function(error){
            alert(error);
        });
    },

    discountAmount: function(index){
        return this.selItems[index].price * this.selItems[index].disccount;
    },

    taxAmount: function(index){
        return (this.selItems[index].price - this.discountAmount(index))  * (this.selItems[index].tax_rate / 100); 
    },

    grossTotal: function(index){
        return (this.selItems[index].price - this.discountAmount(index) + this.taxAmount(index)) * this.selItems[index].quantity;     
    },

    printVoucher: function(){            
        window.print();
        this.reset();          
    },

    reset: function(){
        this.getItems();
        this.getCustomers();
        this.getRates();
        this.selItems = [];

        this.customer = '';
        this.product = '';
        this.pays = [];            
        //search at begin
        this.gunFocus();    
        this.pos_id = '';        
    } ,
  },

  computed:{
    total:function(){
        var total = 0;
        for (var i = 0; i < this.selItems.length; i++) {
            total += this.grossTotal(i);
        }

        return total.toFixed(2);
    },

    payed: function(){
        var total = 0;
        for (var i = 0; i < this.pays.length; i++) {
            total += +this.pays[i].amount;
        }
        return total.toFixed(2);            
    },

    toPay: function(){            
        return (this.total - this.payed).toFixed(2);
    },

    change: function(){
        var total = +this.payed - +this.total;

        return total.toFixed(2) > 0 ? total.toFixed(2) : 0;
    },

    showReference: function(){
        return this.pay_type == 'tdc' || this.pay_type == 'tdd';
    },

    canPay: function(){
        return this.selItems.length;
    },

    canAddPayTaype: function(){
        return this.pay_type  && this.pay_amount;
    },

    totalPayed: function(){
        return +this.payed >= +this.total;
    },

    canOpen: function(){
        return this.start_amount && this.rate;
    }

  }
});