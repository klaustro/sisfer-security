<?php

use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Session;
use sisVentas\Cajero;
use sisVentas\DisccountDetail;
use sisVentas\Item;
use sisVentas\ItemSite;
use sisVentas\Site;
use sisVentas\User;

class DiscountTest extends TestCase
{
    use DatabaseTransactions;

    public function test_discount()
    {
        $user = factory(User::class)->create();

        $cashier = factory(Cajero::class)->create([
            'user_id' => $user->id,
        ]);

        $this->actingAs($user);

        
        $site = factory(Site::class)->create();

        Session::put('site_id', $site->id);

        $items = factory(Item::class, 2)->create();

        $itemSite = factory(ItemSite::class)->create([
            'inv_item_id' => $items[0]->inv_item_id,
            'site_id' => $site->id,
        ]);

        $itemDiscount = factory(DisccountDetail::class)->create([
            'site_id' => $site->id,
            'item_id' => $items[0]->inv_item_id,
            'start_date' => date_format(Carbon::now()->subDay(),"Y-m-d"),
            'end_data' => date_format(Carbon::now()->addDay(),"Y-m-d"),
            'rate_disccount' => 0.1,
        ]);

        //dd($itemSite->item_disccount);
        $this->assertTrue($itemSite->item_disccount == $itemDiscount->rate_disccount);
    }

    public function test_null_discount()
    {
        $user = factory(User::class)->create();

        $cashier = factory(Cajero::class)->create([
            'user_id' => $user->id,
        ]);

        $this->actingAs($user);

        
        $site = factory(Site::class)->create();
        
        Session::put('site_id', $site->id);

        $items = factory(Item::class, 2)->create();

        $itemSite = factory(ItemSite::class)->create([
            'inv_item_id' => $items[0]->inv_item_id,
            'site_id' => $site->id,
        ]);


        $this->assertTrue($itemSite->item_disccount == null);
    }    
}
