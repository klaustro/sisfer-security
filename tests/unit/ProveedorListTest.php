<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use sisVentas\Proveedor;

class ProveedorListTest extends TestCase
{
	use DatabaseTransactions;
    public function test_if_vendor_is_listed()
    {
        $vendor = factory(Proveedor::class, 10)->create();

        $this->get('compras/vendor')
        	->assertResponseOk();
    }
}
