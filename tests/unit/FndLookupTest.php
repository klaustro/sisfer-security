<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use sisVentas\FndLookup;
use sisVentas\FndLookupValue;

class FndLookupTest extends TestCase
{
	use DatabaseTransactions;

	public function test_relationship_with_fndLookupValue()
	{		
		$fndLookup = factory(FndLookup::class)->create([
			'lookup_type' => 'test_type',
		]);
		
		$fndLookupValue = factory(FndLookupValue::class, 5)->create([
			'idlookup' => $fndLookup->idlookup,
		]);

		//dd(FndLookup::where('lookup_type', 'test_type')->first()->fndLookupValue->pluck('description', 'code_value')->toArray());

		//dd(FndLookupValue::orderBy('code_value', 'ASC')->get()->pluck('description', 'code_value')->toArray());


	}
}
