<?php

use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use sisVentas\Entity;
use sisVentas\Moneda;
use sisVentas\Orden;
use sisVentas\DailyRate;

class RateTest extends TestCase
{
    use DatabaseTransactions;

    public function test_rate_usd_to_pen()
    {
        factory(Entity::class)->create([
            'currency_code' => 'PEN',
        ]);
        
        factory(Moneda::class)->create([
            'currency_code' => 'PEN',
        ]);

        factory(Moneda::class)->create([
            'currency_code' => 'USD',
        ]);        
        
        $exchange = factory(DailyRate::class)->create([
            'from_currency' => 'USD',
            'to_currency' => 'PEN',
            'conversion_date' => date('y-m-d'),
        ]);

        factory(Orden::class)->create([
            'currency_code' => 'USD'
        ]);
        //dd($exchange->conversion_rate, Orden::first()->rate_amount);
        $this->assertTrue($exchange->conversion_rate == Orden::first()->rate_amount);
    }

    public function test_rate_no_setted()
    {
        factory(Entity::class)->create([
            'currency_code' => 'PEN',
        ]);
        
        factory(Moneda::class)->create([
            'currency_code' => 'PEN',
        ]);

        factory(Moneda::class)->create([
            'currency_code' => 'USD',
        ]);        
        

        factory(Orden::class)->create([
            'currency_code' => 'USD'
        ]);

        $this->assertTrue(null == Orden::first()->rate_amount);
    }  

    public function test_rate_pen_to_pen()
    {
        factory(Entity::class)->create([
            'currency_code' => 'PEN',
        ]);
        
        factory(Moneda::class)->create([
            'currency_code' => 'PEN',
        ]);

        factory(Moneda::class)->create([
            'currency_code' => 'USD',
        ]);        
        
        $exchange = factory(DailyRate::class)->create([
            'from_currency' => 'USD',
            'to_currency' => 'PEN',
            'conversion_date' => date_format(Carbon::now(),"Y-m-d"),
        ]);

        factory(Orden::class)->create([
            'currency_code' => 'PEN'
        ]);

        $this->assertTrue(1 == Orden::first()->rate_amount);
    }

    function test_api_rate()
    {
        factory(Entity::class)->create([
            'currency_code' => 'PEN',
        ]);
        
        factory(Moneda::class)->create([
            'currency_code' => 'PEN',
        ]);

        factory(Moneda::class)->create([
            'currency_code' => 'USD',
        ]);        
        
        $exchange = factory(TipoCambio::class)->create([
            'from_currency' => 'USD',
            'to_currency' => 'PEN',
            'conversion_date' => date_format(Carbon::now(),"Y-m-d"),
        ]);  
        
        $this->get('exchange/rate')
            ->assertResponseOk()
            ->seeJson([
                'from_currency' => 'USD',
                'to_currency' => 'PEN',
                'conversion_date' => date_format(Carbon::now(),"Y-m-d"),
            ]);
    }

}
