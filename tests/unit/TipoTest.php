<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use sisVentas\FndLookup;
use sisVentas\FndLookupValue;

class TipoTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_type_list()
    {
    		$tipo = factory(FndLookup::class)->create();

    		$tipos = factory(FndLookupValue::class, 3)->create([
    			'idlookup' => $tipo->idlookup,
    		]);

    		//dd($tipo->fndLookupValue);
        
        $this->assertTrue(true);
    }
}
