<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use sisVentas\Caja;
use sisVentas\Cajero;
use sisVentas\FndLookup;
use sisVentas\FndLookupValue;
use sisVentas\Pos;
use sisVentas\PosDetail;
use sisVentas\PosPay;
use sisVentas\Site;
use sisVentas\User;

class CashUnitTest extends TestCase
{
	use DatabaseTransactions; 

    protected $faker;
    protected $user;
    protected $cashier;
    protected $site;
    protected $cash;


    function setUp()
    {
        parent::setUp();
        
        $this->faker = Faker\Factory::create();

        $this->user = factory(User::class)->create();

        $this->cashier = factory(Cajero::class)->create([
            'user_id' => $this->user->id,
        ]);


        $this->site = factory(Site::class)->create();

        $this->cash = factory(Caja::class)->create([
            'site_id' => $this->site->id,
            'idcashier' => $this->cashier->id,
        ]); 

        $class = factory(FndLookup::class)->create(['lookup_type' => 'TIPO_PAGO', 'customization_level' => 'E', 'description' => 'Medios de pago',]);   

        factory(FndLookupValue::class)->create(['idlookup' => $class->idlookup, 'code_value' =>  'che',  'description' =>  'Cheque']);    
        factory(FndLookupValue::class)->create(['idlookup' => $class->idlookup, 'code_value' =>  'efe',  'description' =>  'Efectivo']);  
        factory(FndLookupValue::class)->create(['idlookup' => $class->idlookup, 'code_value' =>  'ndb',  'description' =>  'Nota de débito']);    
        factory(FndLookupValue::class)->create(['idlookup' => $class->idlookup, 'code_value' =>  'tdc',  'description' =>  'Tarjeta de crédito']);    
        factory(FndLookupValue::class)->create(['idlookup' => $class->idlookup, 'code_value' =>  'tdd',  'description' =>  'Tarjeta de Débito']); 
        factory(FndLookupValue::class)->create(['idlookup' => $class->idlookup, 'code_value' =>  'tran',  'description' =>  'Transferencia']);                

    }

    function test_cash_pos()
    {

        $pos = factory(Pos::class)->create([
            'pos_op_amt_id' => $this->cash->idopamt,
        ]);

        $posDetail = factory(PosDetail::class)->create([
            'pos_header_id' => $pos->id,
        ]);

        $posPay = factory(PosPay::class)->create([
            'pos_header_id' => $pos->id,
        ]);

        $this->assertTrue($this->cash->pos->first() == $pos->first());

        $this->assertTrue($this->cash->pos->first()->posDetail->first() == $posDetail->first());

        $this->assertTrue($this->cash->pos->first()->posPay->first() == $posPay->first());
    }

    function test_cash_sells()
    {

        for ($i=0; $i < 5; $i++) { 
            $pos = factory(Pos::class)->create([
                'pos_op_amt_id' => $this->cash->idopamt,
            ]);

            factory(PosDetail::class)->create([
                'pos_header_id' => $pos->id,
            ]);

            factory(PosPay::class)->create([
                'pos_header_id' => $pos->id,
                'type_lookup_code' => $this->faker->randomElement(['efe', 'tdc', 'tdd', 'ndb', 'che','tran']),
            ]);            

            factory(PosPay::class)->create([
                'pos_header_id' => $pos->id,
                'type_lookup_code' => $this->faker->randomElement(['efe', 'tdc', 'tdd', 'ndb', 'che','tran']),
            ]);            

            factory(PosPay::class)->create([
                'pos_header_id' => $pos->id,
                'type_lookup_code' => $this->faker->randomElement(['efe', 'tdc', 'tdd', 'ndb', 'che','tran']),
            ]);
        }

        dd($this->cash->pos()->with('posPay')->get(), $this->cash->sells(), $this->cash->pays_total);

        $this->assertTrue($this->cash->sells() != null);

    }
}
