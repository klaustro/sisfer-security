<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use sisVentas\Cliente;

class ClientTest extends TestCase
{
    use DatabaseTransactions;

    public function test_api_client_index()
    {
        factory(Cliente::class, 20)->create();

        $this->get('ventas/posClientes')
            ->assertResponseOk();
    }
}
