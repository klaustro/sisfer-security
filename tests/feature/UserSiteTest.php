<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Auth;
use sisVentas\Site;
use sisVentas\User;
use sisVentas\UserSite;

class UserSiteTest extends TestCase
{
	use DatabaseTransactions;

	function test_api_list_user()
	{

		$user = factory(User::class)->create();

		factory(User::class, 5)->create();

		$this->get('/userSite/user')
			->assertResponseOk()
			->seeJson([
				'id' => $user->id,
				'email' => $user->email,
			]);
	}

	function test_api_list_sites()
	{

		$sites = factory(Site::class, 5)->create();

		$this->get('/userSite/site')
			->assertResponseOk()
			->seeJson([
				'id' => $sites[0]->id,
				'name' => $sites[0]->name,
				'description' => $sites[0]->description,
			]);
	}	

	function test_api_list_sites_for_user()
	{
		$user = factory(User::class)->create();


		$site = factory(Site::class)->create();

		$this->actingAs($user);
		
		$data = [
			'IDUSUARIO' => $user->id,
			'IDSITE' => $site->id,
			'FEC_INI' => '2017-01-01',
			'FEC_FIN' => '2017-12-31',
			'ULTACTPOR' => Auth::user()->id,
			'CREPOR' => Auth::user()->id,
		];	

		$userSite = UserSite::create($data);

		$this->get('/userSite/' . $user->id)
			->assertResponseOk()
			->seeJson([
				'IDUSUARIO' => $user->id,
				'IDSITE' => $site->id,
				'FEC_INI' => '2017-01-01 00:00:00',
				'FEC_FIN' => '2017-12-31 00:00:00',
				'ULTACTPOR' => Auth::user()->id,
				'CREPOR' => Auth::user()->id,
			]);
	}	

	function test_api_list_sites_for_email()
	{
		$user = factory(User::class)->create();


		$site = factory(Site::class)->create();

		$this->actingAs($user);
		
		$data = [
			'IDUSUARIO' => $user->id,
			'IDSITE' => $site->id,
			'FEC_INI' => '2017-01-01',
			'FEC_FIN' => '2017-12-31',
			'ULTACTPOR' => Auth::user()->id,
			'CREPOR' => Auth::user()->id,
		];	

		$userSite = UserSite::create($data);

		$this->get('/emailSite/' . $user->email)
			->assertResponseOk();
	}	

	function test_api_list_sites_for_wrong_email()
	{
		$user = factory(User::class)->create();


		$site = factory(Site::class)->create();

		$this->actingAs($user);
		
		$data = [
			'IDUSUARIO' => $user->id,
			'IDSITE' => $site->id,
			'FEC_INI' => '2017-01-01',
			'FEC_FIN' => '2017-12-31',
			'ULTACTPOR' => Auth::user()->id,
			'CREPOR' => Auth::user()->id,
		];	

		$userSite = UserSite::create($data);

		$this->get('/emailSite/' . 'sitasd@gmail.com')
			->assertResponseOk();
	}		

	function test_user_site_is_stored()
	{
		$user = factory(User::class)->create();

		$site = factory(Site::class)->create();
		
		$this->actingAs($user);

		$data = [
			'0' => ['IDUSUARIO' => $user->id,
				'IDSITE' => $site->id,
				'FEC_INI' => '2017-01-01',
				'FEC_FIN' => '2017-12-31',
				'ULTACTPOR' => Auth::user()->id,
				'CREPOR' => Auth::user()->id,
			]
		];	

		$this->post('/userSite', $data)
			->assertResponseOk()
			->seeJson(['message' => 'Usuario asociado satisfactoriamente']);

		$this->seeInDatabase('fnd_usersite', [
			'IDUSUARIO' => $user->id,
			'IDSITE' => $site->id,
			'FEC_INI' => '2017-01-01',
			'FEC_FIN' => '2017-12-31',
			'ULTACTPOR' => Auth::user()->id,
			'CREPOR' => Auth::user()->id,
		]);
	}

	function test_user_site_is_updated()
	{
		$user = factory(User::class)->create();

		$site = factory(Site::class)->create();
		
		$this->actingAs($user);


		$data = [
			'IDUSUARIO' => $user->id,
			'IDSITE' => $site->id,
			'FEC_INI' => '2017-01-01',
			'FEC_FIN' => '2017-12-31',
			'ULTACTPOR' => Auth::user()->id,
			'CREPOR' => Auth::user()->id,
		];	

		$userSite = UserSite::create($data);

		$newData = [
			'0' => [
				'IDUSUARIO' => $user->id,
				'IDSITE' => $site->id,
				'FEC_INI' => '2017-01-01',
				'FEC_FIN' => '2017-11-30',
				'ULTACTPOR' => Auth::user()->id,
				'CREPOR' => Auth::user()->id,
			],
		];			

		$this->put('/userSite/' . $userSite->IDUSERSITE, $newData)
			->assertResponseOk()
			->seeJson(['message' => 'Usuario actualizado satisfactoriamente']);

		$this->seeInDatabase('fnd_usersite', [
			'IDUSUARIO' => $user->id,
			'IDSITE' => $site->id,
			'FEC_INI' => '2017-01-01',
			'FEC_FIN' => '2017-11-30',
			'ULTACTPOR' => Auth::user()->id,
		]);		
	}	
}
