<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use sisVentas\Site;
use sisVentas\User;
use sisVentas\UserSite;
use sisVentas\Warehouse;

class WarehouseTest extends TestCase
{
	use DatabaseTransactions;

	protected $user;
	protected $site;
	protected $userSite;

	protected function setUp()
	{
		parent::setUp();

	        $this->user = factory(User::class)->create();

	        $this->site = factory(Site::class)->create();

	        $this->userSite = factory(UserSite::class)->create([
			'IDUSUARIO' => $this->user->id,
			'IDSITE' => $this->site->id,
	        ]);


	}	

    public function test_ii_warehouse_is_paginated()
    {
       	$warehouses = factory(Warehouse::class, 15)->create([
		'site_id' => $this->site->id,
	]);
	
	$last = factory(Warehouse::class)->create([
		'site_id' => $this->site->id,
	]);

	$this->visit('/login')
		->type($this->user->email, 'email')
		->type('secret', 'password')
		->press('Acceder');

	$this->visit('/almacen/warehouse')
		->see('Listado de subinventarios')
		->see($warehouses[0]->name)
		->dontSee($last->name);



    }

	public function test_warehouse_can_be_created()
	{
	    	//Having
	    	$this->actingAs($this->user);

	    	Session::set('site_id', $this->site->id);

		//When 
		$this->visit(route('almacen.warehouse.create'))
			->see('Nuevo subinventario')
			->type('almacen 1', 'name')
			->press('Finalizar');

		//Then
		$this->seeInDatabase('fnd_warehouse',[
			'name' =>'almacen 1',
		]);

		$this->seePageIs('almacen/warehouse')
			->see('almacen 1');

	}    

	public function test_warehouse_data_can_be_showed()
	{
	    	//Having
		$this->actingAs($this->user);
		
		$warehouse = factory(\sisVentas\Warehouse::class)->create([
			'site_id' => $this->site->id,
		]);

		//When
		$this->visit("/almacen/warehouse/{$warehouse->id}");

		//Then
		$this->see($warehouse->name );	
	}	

	public function test_warehouse_can_be_updated()
	{
	    	//Having
	    	$this->actingAs($this->user);

		$warehouse = factory(\sisVentas\Warehouse::class)->create([
			'site_id' => $this->site->id,
		]);

		//When 
		$this->visit("/almacen/warehouse/{$warehouse->id}")
			->type('Almacen 2', 'name')
			->press('Finalizar');

		//Then
		$this->seeInDatabase('fnd_warehouse',[
			'id' => $warehouse->id,
			'name' =>'Almacen 2',
		]);

		$this->seePageIs('/almacen/warehouse')
			->see('Almacen 2');

	}

}
