<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use sisVentas\Item;
use sisVentas\Site;
use sisVentas\User;

class ItemSiteTest extends TestCase
{
		use DatabaseTransactions;
		
    public function test_if_can_items()
    {
    	$user = factory(User::class)->create();

    	$this->actingAs($user);

			$site = factory(Site::class)->create();

			$items = factory(Item::class, 2)->create();


			$data = [
				0 => [
					'inv_item_id' => $items[0]->inv_item_id,
					'site_id' => $site->id,				
					'sku' => '562442547456',
					'list_price_per_unit' => 100,					
				],
				1 => [
					'inv_item_id' => $items[1]->inv_item_id,
					'site_id' => $site->id,				
					'sku' => '254425541',
					'list_price_per_unit' => 200,					
				],				
			];
			
			$this->post('ventas/posItemSite', $data)
				->assertResponseOk()
				->seeJson(['message' => 'Items agregados a la sucursal']);

			$this->seeInDatabase('inv_item_site',[
					'inv_item_id' => $items[0]->inv_item_id,
					'site_id' => $site->id,				
					'sku' => '562442547456',
					'list_price_per_unit' => 100,		
			]);

			$this->seeInDatabase('inv_item_site',[
					'inv_item_id' => $items[1]->inv_item_id,
					'site_id' => $site->id,				
					'sku' => '254425541',
					'list_price_per_unit' => 200,					
			]);			
    }
}
