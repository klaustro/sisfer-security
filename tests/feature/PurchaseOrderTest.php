<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use sisVentas\Item;
use sisVentas\POTerm;
use sisVentas\PurchaseOrder;
use sisVentas\Orden;
use sisVentas\User;

class PurchaseOrderTest extends TestCase
{
	use DatabaseTransactions;
	
	public function test_if_po_detail_is_stored()
	{
		$user = factory(User::class)->create();

		$this->actingAs($user);

		$po = factory(Orden::class)->create();

		$items = factory(Item::class, 3)->create();

		$data = [];

		foreach ($items as $key => $item) {
			array_push($data, [
				'po_header_id' => $po->po_header_id,
				'line_num' => $key,
				'item_id' => $item->inv_item_id,
				'category_id' => $item->idcategoria,
				'item_description' => $item->descripcion,
				'unit_meas_lookup_code' => 'DOC',
				'unit_price' => 300  *  $key,
				'need_by_date' => '2017-09-12"',
				'quantity' => $key * 2,
			]);
		}

	$this->post('compras/poDetail', $data)
		->assertResponseOk()
		->seeJson(['message' => 'Detalle guardado satisfactoriamente']);
	}

	public function test_if_po_detail_is_updated()
	{
		$user = factory(User::class)->create();

		$this->actingAs($user);

		$po = factory(Orden::class)->create();

		$items = factory(Item::class, 3)->create();

		$poDetail = PurchaseOrderDetail::create([
				'po_header_id' => $po->po_header_id,
				'line_num' => 1,
				'item_id' => $items[0]->inv_item_id,
				'category_id' => $items[0]->idcategoria,
				'item_description' => $items[0]->descripcion,
				'unit_meas_lookup_code' => 'DOC',
				'unit_price' => 1500,
				'need_by_date' => '2017-09-12"',
				'quantity' => 5,
		]);

		$poDetail->save();

		$data = [];

		foreach ($items as $key => $item) {
			array_push($data, [
				'po_header_id' => $po->po_header_id,
				'line_num' => $key,
				'item_id' => $item->inv_item_id,
				'category_id' => $item->idcategoria,
				'item_description' => $item->descripcion,
				'unit_meas_lookup_code' => 'DOC',
				'unit_price' => 300  *  $key,
				'need_by_date' => '2017-09-12"',
				'quantity' => $key * 2,
			]);
		}

	$this->post('compras/poDetail', $data)
		->assertResponseOk()
		->seeJson(['message' => 'Detalle guardado satisfactoriamente']);
	}	

	function test_get_detail()
	{
		$user = factory(User::class)->create();

		$this->actingAs($user);

		$po = factory(Orden::class)->create();

		$items = factory(Item::class, 3)->create();

		$data = [];

		foreach ($items as $key => $item) {
			array_push($data, [
				'po_header_id' => $po->po_header_id,
				'line_num' => $key,
				'item_id' => $item->inv_item_id,
				'category_id' => $item->idcategoria,
				'item_description' => $item->descripcion,
				'unit_meas_lookup_code' => 'DOC',
				'unit_price' => 300  *  $key,
				'need_by_date' => '2017-09-12"',
				'quantity' => $key * 2,
			]);
		}

		foreach ($data as $item) {
			$poDetail = PurchaseOrderDetail::create($item);		
			$poDetail->save();
		}

		$this->get('compras/poDetail?id=' . $po->po_header_id)
			->assertResponseOk();
	}

	function test_if_terms_can_be_stored()
	{
		$user = factory(User::class)->create();

		$data = [
        	'terms_id' => '1',
        	'freight_terms_lookup_code' => '1',
        	'ship_via_lookup_code' => '1',
        	'fob_lookup_code' => '1',
        	'note_to_vendor' => '1',
        	'note_to_receiver' => '1',
		];

        $terms = new POTerm();
        $terms->terms_id = $data['terms_id'];
        $terms->freight_terms_lookup_code = $data['freight_terms_lookup_code'];
        $terms->ship_via_lookup_code = $data['ship_via_lookup_code'];
        $terms->fob_lookup_code = $data['fob_lookup_code'];
        $terms->note_to_vendor = $data['note_to_vendor'];
        $terms->note_to_receiver = $data['note_to_receiver'];
        $terms->save();		

		$this->seeInDatabase('po_terms', $data);
	}

	function test_is_oreder_is_approved()
	{
		$user = factory(User::class)->create();

		$this->actingAs($user);

		$po = factory(Orden::class)->create([
			'authorization_status' => 'INCOMPLETE'
		]);		

		$this->put('compras/approveOrder/' . $po->po_header_id)
			->assertResponseOk()
			->seeJson(['message' => 'Orden aprobada']);

	}
}
