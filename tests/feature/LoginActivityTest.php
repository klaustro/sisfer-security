<?php

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use sisVentas\User;

class LoginActivityTest extends TestCase
{
	use DatabaseTransactions;

	public function test_if_login_is_stored()
	{
		//Having
		$user = factory(User::class)->create();

		//When
		$this->visit('/login')
			->type($user->email, 'email')
			->type('secret', 'password')
			->press('Acceder');

		//Then			
		$this->seeInDatabase('fnd_login',[
			'IDUSUARIO' => $user->id,
		]);
	}
}
