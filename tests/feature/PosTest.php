<?php

use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use sisVentas\Cliente;
use sisVentas\Entity;
use sisVentas\FndLookup;
use sisVentas\FndLookupValue;
use sisVentas\Impuesto;
use sisVentas\Item;
use sisVentas\ItemSite;
use sisVentas\Moneda;
use sisVentas\Site;
use sisVentas\TipoCambio;
use sisVentas\User;

class PosTest extends TestCase
{

    use DatabaseTransactions;

    public function test_items_sites_is_listed()
    {
        $site = factory(Site::class)->create();

        $items = factory(Item::class, 20)->create();

        for ($i=0; $i < 6; $i++) { 
	        factory(ItemSite::class)->create([
	        	'item_id' => $items[$i]->inv_item_id,
	        	'site_id' => $site->id,
	        ]);
        }

        $this->get('ventas/posArticles/' . $site->id)
        	->assertResponseOk();
    }

    function test_if_sell_is_stored()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $customer = factory(Cliente::class)->create();

        $fndLookup = factory(FndLookup::class)->create([
            'lookup_type' => 'pos_type',
        ]);
        
        $fndLookupValue = factory(FndLookupValue::class)->create([
            'idlookup' => $fndLookup->idlookup,
            'description' => 'Boleta manual',
            //code_value
        ]);  

        $fndLookupPay = factory(FndLookup::class)->create([
            'lookup_type' => 'pay_type',
        ]);

        $payType = factory(FndLookupValue::class, 2)->create([
            'idlookup' => $fndLookupPay->idlookup,
            //code_value
        ]);       

        $site = factory(Site::class)->create();

        $items = factory(Item::class, 20)->create();

        $tax = factory(Impuesto::class)->create();

        for ($i=0; $i < 6; $i++) { 
            factory(ItemSite::class)->create([
                'inv_item_id' => $items[$i]->inv_item_id,
                'site_id' => $site->id,
            ]);
        }

        $selItems = ItemSite::all();

        $posLines = [
            0 => [
                'inv_item_id' => $items[0]->inv_item_id,
                'price' => $selItems[0]->list_price_per_unit,
                'quantity' => 2,
                'tax_rate' => $tax->tax_rate,
                'tax_id' => $tax->tax_id,
            ],
            1 => [
                'inv_item_id' => $items[1]->inv_item_id,
                'price' => $selItems[1]->list_price_per_unit,
                'quantity' => 1,
                'tax_rate' => $tax->tax_rate,
                'tax_id' => $tax->tax_id,
            ],  
            2 => [
                'inv_item_id' => $items[2]->inv_item_id,
                'price' => $selItems[2]->list_price_per_unit,
                'quantity' => 3,
                'tax_rate' => $tax->tax_rate,
                'tax_id' => $tax->tax_id,
            ],  
        ];

        $posPay = [
            '0' => [
                'type_lookup_code' => $payType[0]->code_value,
                'reference' => '',
                'amount' => 100,                                
            ],
            '1' => [
                'type_lookup_code' => $payType[1]->code_value,
                'reference' => '',
                'amount' => 200,                                
            ],            
        ];

        $pos = [
            'site_id' => $site->id,
            'client_id'=> $customer->idcliente,
            'type_lookup_code' => $fndLookupValue->code_value,
            'pos_lines' => $posLines,
            'pos_pay' => $posPay,
        ];

        $this->post('ventas/pos', $pos)
            ->assertResponseOk()
            ->seeJson(['message' => 'Venta creada exitosamente']);

        $this->seeInDatabase('pos_header', [
            'site_id' => $site->id,
            'client_id'=> $customer->idcliente,
            'type_lookup_code' => $fndLookupValue->code_value,
        ]);

        $this->seeInDatabase('pos_line', [
            'inv_item_id' => $items[0]->inv_item_id,
            'price' => $selItems[0]->list_price_per_unit,
            'quantity' => 2,
            'tax_rate' => $tax->tax_rate,
            'tax_id' => $tax->tax_id,
        ]);

        $this->seeInDatabase('pos_line', [
            'inv_item_id' => $items[1]->inv_item_id,
            'price' => $selItems[1]->list_price_per_unit,
            'quantity' => 1,
            'tax_rate' => $tax->tax_rate,
            'tax_id' => $tax->tax_id,
        ]);
        $this->seeInDatabase('pos_line', [
            'inv_item_id' => $items[2]->inv_item_id,
            'price' => $selItems[2]->list_price_per_unit,
            'quantity' => 3,
            'tax_rate' => $tax->tax_rate,
            'tax_id' => $tax->tax_id,
        ]);
        $this->seeInDatabase('pos_pay', [
            'type_lookup_code' => $payType[0]->code_value,
            'reference' => '',
            'amount' => 100,
        ]);
        $this->seeInDatabase('pos_pay', [
            'type_lookup_code' => $payType[1]->code_value,
            'reference' => '',
            'amount' => 200,
        ]);

    }
}
