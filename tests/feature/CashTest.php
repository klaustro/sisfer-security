<?php

use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use sisVentas\Caja;
use sisVentas\Entity;
use sisVentas\Moneda;
use sisVentas\Site;
use sisVentas\TipoCambio;
use sisVentas\User;

class CashTest extends TestCase
{
    use DatabaseTransactions;

    protected $user;
    protected $site;
    protected $exchange;
    protected $request;

    function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();

        $this->actingAs($this->user);

        factory(Entity::class)->create([
            'currency_code' => 'PEN',
        ]);
        
        $this->site = factory(Site::class)->create();

        factory(Moneda::class)->create([
            'currency_code' => 'PEN',
        ]);

        factory(Moneda::class)->create([
            'currency_code' => 'USD',
        ]);        
        
        $this->exchange = factory(TipoCambio::class)->create([
            'from_currency' => 'USD',
            'to_currency' => 'PEN',
            'conversion_date' => date_format(Carbon::now(),"Y-m-d"),
        ]);          
        $this->request = [
            'start_date' => date_format(Carbon::now(),"Y-m-d"),
            'start_amount' => 100,
            'comment' => 'New Comment',
        ];   

        $this->post('ventas/openCash', $this->request)
            ->assertResponseOk()
            ->seeJson(['message' => 'Caja abierta satisfactoriamente']);  
    }

    function test_cash_is_opened()
    {   
        $this->seeInDatabase('pos_op_amt', [
            'comment' => 'New Comment',
        ]);
    }    

    function test_is_cash_can_be_closed()
    {
        $cash = Caja::first();

        $this->put('ventas/closeCash/' . $cash->idopamt)
            ->assertResponseOk()
            ->seeJson(['message' => 'Caja cerrada satisfactoriamente']);

        $this->dontSeeInDatabase('pos_op_amt', [
            'end_date' => null,
        ]);
    }
}
